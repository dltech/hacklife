#include "regs/f1/rcc_regs.h"
#include "regs/f1/tim_regs.h"
#include "delay.h"


void timDelayInit()
{
    RCC_APB1ENR |= TIM2EN;
    TIM2_CR1  = CKD_CK_INT;
    TIM2_PSC  = 0;
    TIM2_ARR  = 0xFFFF;
    TIM2_CR1 |= CEN;
//    TIM2_EGR |= UG;
}

void timDelayNs(int ns)
{
//    int tout = 0x10001;
    uint32_t tacts = ((uint32_t)ns)/NS_PER_TACT;
    TIM2_EGR |= UG;
//    while((TIM2_CNT < tacts) && (--tout>1));
    while(TIM2_CNT < tacts);
}

void timDelayUs(int us)
{
    for(int i=0 ; i<us ; ++i) {
        timDelayNs(1000);
    }
}

void timDelayMs(int ms)
{
    for(int i=0 ; i<ms ; ++i) {
        timDelayUs(1000);
    }
}

void delay_s(uint16_t s)
{
    uint32_t cnt = ((uint32_t)s)*20;
    while(cnt-- > 0) rough_delay_us(50000);
}

void delay_ms(uint16_t ms)
{
    while(ms-- > 0) rough_delay_us(1000);
}

void rough_delay_us(uint16_t us)
{
    // podbiral na glazok
    volatile uint32_t cnt = (uint32_t)us*(uint32_t)5;
    while(cnt-- > 0);
}
