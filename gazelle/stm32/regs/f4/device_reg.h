#ifndef H_DEVICE_REG
#define H_DEVICE_REG
/*
 * Part of Belkin STM32 HAL, Device electronic signature of
 * STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Flash size register */
#define FLASH_SIZE  MMIO32(0x1FFF7A22)
// F_ID(15:0): value indicates the Flash memory size of the device in Kbytes.

/* Unique device ID register */
#define UID_REG1    MMIO32(0x1FFF7A10)
// UID(31:0): X and Y coordinates on the wafer
#define UID_REG2    MMIO32(0x1FFF7A14)
// UID(63:40): LOT_NUM[23:0] Lot number (ASCII encoded).
// UID(39:32): WAF_NUM[7:0] Wafer number (ASCII encoded).
#define UID_REG3    MMIO32(0x1FFF7A18)
// UID(95:64): LOT_NUM[55:24] Lot number (ASCII encoded).

#endif
