#ifndef H_GPIO_REG
#define H_GPIO_REG
/*
 * Part of Belkin STM32 HAL, System configuration controller (SYSCFG)
 * registers of STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* SYSCFG memory remap register */
#define SYSCFG_MEMRMP   MMIO32(SYSCFG_BASE + 0x00)
// Memory mapping selection
#define MEM_MODE_MAIN   0x0
#define MEM_MODE_SYSTEM 0x1
#define MEM_MODE_FSMC   0x2
#define MEM_MODE_SRAM   0x3

/* SYSCFG peripheral mode configuration register */
#define SYSCFG_PMC      MMIO32(SYSCFG_BASE + 0x04)
// Ethernet PHY interface selection
#define MII_RMII_SEL    0x800000

/* SYSCFG external interrupt configuration register 1 */
#define SYSCFG_EXTICR1  MMIO32(SYSCFG_BASE + 0x08)
// EXTI x configuration
#define EXTIx_PA(x) 0x0000
#define EXTIx_PB(x) (0x1<<(x*4))
#define EXTIx_PC(x) (0x2<<(x*4))
#define EXTIx_PD(x) (0x3<<(x*4))
#define EXTIx_PE(x) (0x4<<(x*4))
#define EXTIx_PF(x) (0x5<<(x*4))
#define EXTIx_PG(x) (0x6<<(x*4))
#define EXTIx_PH(x) (0x7<<(x*4))
#define EXTIx_PI(x) (0x8<<(x*4))
#define EXTI0_PA    0x0000
#define EXTI0_PB    0x0001
#define EXTI0_PC    0x0002
#define EXTI0_PD    0x0003
#define EXTI0_PE    0x0004
#define EXTI0_PF    0x0005
#define EXTI0_PG    0x0006
#define EXTI0_PH    0x0007
#define EXTI0_PI    0x0008
#define EXTI1_PA    0x0000
#define EXTI1_PB    0x0010
#define EXTI1_PC    0x0020
#define EXTI1_PD    0x0030
#define EXTI1_PE    0x0040
#define EXTI1_PF    0x0050
#define EXTI1_PG    0x0060
#define EXTI1_PH    0x0070
#define EXTI1_PI    0x0080
#define EXTI2_PA    0x0000
#define EXTI2_PB    0x0100
#define EXTI2_PC    0x0200
#define EXTI2_PD    0x0300
#define EXTI2_PE    0x0400
#define EXTI2_PF    0x0500
#define EXTI2_PG    0x0600
#define EXTI2_PH    0x0700
#define EXTI2_PI    0x0800
#define EXTI3_PA    0x0000
#define EXTI3_PB    0x1000
#define EXTI3_PC    0x2000
#define EXTI3_PD    0x3000
#define EXTI3_PE    0x4000
#define EXTI3_PF    0x5000
#define EXTI3_PG    0x6000
#define EXTI3_PH    0x7000
#define EXTI3_PI    0x8000

/* SYSCFG external interrupt configuration register 2 */
#define SYSCFG_EXTICR2  MMIO32(SYSCFG_BASE + 0x0c)
// EXTI x configuration
#define EXTIx_PA(x) 0x0000
#define EXTIx_PB(x) (0x1<<((x-4)*4))
#define EXTIx_PC(x) (0x2<<((x-4)*4))
#define EXTIx_PD(x) (0x3<<((x-4)*4))
#define EXTIx_PE(x) (0x4<<((x-4)*4))
#define EXTIx_PF(x) (0x5<<((x-4)*4))
#define EXTIx_PG(x) (0x6<<((x-4)*4))
#define EXTIx_PH(x) (0x7<<((x-4)*4))
#define EXTIx_PI(x) (0x8<<((x-4)*4))
#define EXTI4_PA    0x0000
#define EXTI4_PB    0x0001
#define EXTI4_PC    0x0002
#define EXTI4_PD    0x0003
#define EXTI4_PE    0x0004
#define EXTI4_PF    0x0005
#define EXTI4_PG    0x0006
#define EXTI4_PH    0x0007
#define EXTI4_PI    0x0008
#define EXTI5_PA    0x0000
#define EXTI5_PB    0x0010
#define EXTI5_PC    0x0020
#define EXTI5_PD    0x0030
#define EXTI5_PE    0x0040
#define EXTI5_PF    0x0050
#define EXTI5_PG    0x0060
#define EXTI5_PH    0x0070
#define EXTI5_PI    0x0080
#define EXTI6_PA    0x0000
#define EXTI6_PB    0x0100
#define EXTI6_PC    0x0200
#define EXTI6_PD    0x0300
#define EXTI6_PE    0x0400
#define EXTI6_PF    0x0500
#define EXTI6_PG    0x0600
#define EXTI6_PH    0x0700
#define EXTI6_PI    0x0800
#define EXTI7_PA    0x0000
#define EXTI7_PB    0x1000
#define EXTI7_PC    0x2000
#define EXTI7_PD    0x3000
#define EXTI7_PE    0x4000
#define EXTI7_PF    0x5000
#define EXTI7_PG    0x6000
#define EXTI7_PH    0x7000
#define EXTI7_PI    0x8000

/* SYSCFG external interrupt configuration register 3 */
#define SYSCFG_EXTICR3  MMIO32(SYSCFG_BASE + 0x10)
// EXTI x configuration
#define EXTIx_PA(x) 0x0000
#define EXTIx_PB(x) (0x1<<((x-8)*4))
#define EXTIx_PC(x) (0x2<<((x-8)*4))
#define EXTIx_PD(x) (0x3<<((x-8)*4))
#define EXTIx_PE(x) (0x4<<((x-8)*4))
#define EXTIx_PF(x) (0x5<<((x-8)*4))
#define EXTIx_PG(x) (0x6<<((x-8)*4))
#define EXTIx_PH(x) (0x7<<((x-8)*4))
#define EXTIx_PI(x) (0x8<<((x-8)x*4))
#define EXTI8_PA    0x0000
#define EXTI8_PB    0x0001
#define EXTI8_PC    0x0002
#define EXTI8_PD    0x0003
#define EXTI8_PE    0x0004
#define EXTI8_PF    0x0005
#define EXTI8_PG    0x0006
#define EXTI8_PH    0x0007
#define EXTI8_PI    0x0008
#define EXTI9_PA    0x0000
#define EXTI9_PB    0x0010
#define EXTI9_PC    0x0020
#define EXTI9_PD    0x0030
#define EXTI9_PE    0x0040
#define EXTI9_PF    0x0050
#define EXTI9_PG    0x0060
#define EXTI9_PH    0x0070
#define EXTI9_PI    0x0080
#define EXTI10_PA   0x0000
#define EXTI10_PB   0x0100
#define EXTI10_PC   0x0200
#define EXTI10_PD   0x0300
#define EXTI10_PE   0x0400
#define EXTI10_PF   0x0500
#define EXTI10_PG   0x0600
#define EXTI10_PH   0x0700
#define EXTI10_PI   0x0800
#define EXTI11_PA   0x0000
#define EXTI11_PB   0x1000
#define EXTI11_PC   0x2000
#define EXTI11_PD   0x3000
#define EXTI11_PE   0x4000
#define EXTI11_PF   0x5000
#define EXTI11_PG   0x6000
#define EXTI11_PH   0x7000
#define EXTI11_PI   0x8000

/* SYSCFG external interrupt configuration register 4 */
#define SYSCFG_EXTICR4  MMIO32(SYSCFG_BASE + 0x14)
// EXTI x configuration
#define EXTIx_PA(x) 0x0000
#define EXTIx_PB(x) (0x1<<((x-12)*4))
#define EXTIx_PC(x) (0x2<<((x-12)*4))
#define EXTIx_PD(x) (0x3<<((x-12)*4))
#define EXTIx_PE(x) (0x4<<((x-12)*4))
#define EXTIx_PF(x) (0x5<<((x-12)*4))
#define EXTIx_PG(x) (0x6<<((x-12)*4))
#define EXTIx_PH(x) (0x7<<((x-12)*4))
#define EXTIx_PI(x) (0x8<<((x-12)*4))
#define EXTI12_PA   0x0000
#define EXTI12_PB   0x0001
#define EXTI12_PC   0x0002
#define EXTI12_PD   0x0003
#define EXTI12_PE   0x0004
#define EXTI12_PF   0x0005
#define EXTI12_PG   0x0006
#define EXTI12_PH   0x0007
#define EXTI12_PI   0x0008
#define EXTI13_PA   0x0000
#define EXTI13_PB   0x0010
#define EXTI13_PC   0x0020
#define EXTI13_PD   0x0030
#define EXTI13_PE   0x0040
#define EXTI13_PF   0x0050
#define EXTI13_PG   0x0060
#define EXTI13_PH   0x0070
#define EXTI13_PI   0x0080
#define EXTI14_PA   0x0000
#define EXTI14_PB   0x0100
#define EXTI14_PC   0x0200
#define EXTI14_PD   0x0300
#define EXTI14_PE   0x0400
#define EXTI14_PF   0x0500
#define EXTI14_PG   0x0600
#define EXTI14_PH   0x0700
#define EXTI14_PI   0x0800
#define EXTI15_PA   0x0000
#define EXTI15_PB   0x1000
#define EXTI15_PC   0x2000
#define EXTI15_PD   0x3000
#define EXTI15_PE   0x4000
#define EXTI15_PF   0x5000
#define EXTI15_PG   0x6000
#define EXTI15_PH   0x7000
#define EXTI15_PI   0x8000

/* Compensation cell control register */
#define SYSCFG_CMPCR    MMIO32(SYSCFG_BASE + 0x20)
// Compensation cell ready flag
#define READY   0x100
// Compensation cell power-down
#define CMP_PD  0x001

#endif
