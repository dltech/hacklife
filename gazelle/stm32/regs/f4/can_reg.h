#ifndef H_CAN_REG
#define H_CAN_REG
/*
 * Part of Belkin STM32 HAL, Controller area network (bxCAN)
 * interface registers of STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* CAN master control register */
#define CAN1_MCR MMIO32(CAN1_BASE + 0x000)
#define CAN2_MCR MMIO32(CAN2_BASE + 0x000)
// Debug freeze 1: CAN reception/transmission frozen during debug.
#define DBF         0x10000
// bxCAN software master reset 1: Force a master reset
#define CAN_RESET   0x08000
// Time triggered communication mode
#define TTCM        0x00080
// Automatic bus-off management, CAN hard on leaving the Bus-Off st.
#define ABOM        0x00040
// Automatic wakeup mode, on message reception during Sleep mode.
#define AWUM        0x00020
// No automatic retransmission, 1: A message will be tx only once
#define NART        0x00010
// Receive FIFO locked mode 1: Receive FIFO locked against overrun.
#define RFLM        0x00008
// Transmit FIFO priority 1: Prior driven by the req order (chronol)
#define TXFP        0x00004
// Sleep mode request
#define SLEEP       0x00002
// Initialization request, clear this bit to switch into normal mode
#define INRQ        0x00001

/* CAN master status register */
#define CAN1_MSR MMIO32(CAN1_BASE + 0x004)
#define CAN2_MSR MMIO32(CAN2_BASE + 0x004)
// CAN Rx signal, Monitors the actual value of the CAN_RX Pin.
#define RX      0x800
// Last sample point, The value of RX on the last sample point.
#define SAMP    0x400
// Receive mode, The CAN hardware is currently receiver.
#define RXM     0x200
// Transmit mode, The CAN hardware is currently transmitter.
#define TXM     0x100
// Sleep acknowledge interrupt
#define SLAKI   0x010
// Wakeup interrupt
#define WKUI    0x008
// Error interrupt
#define ERRI    0x004
// Sleep acknowledge
#define SLAK    0x002
// Initialization acknowledge
#define INAK    0x001

/* CAN transmit status register */
#define CAN1_TSR MMIO32(CAN1_BASE + 0x008)
#define CAN2_TSR MMIO32(CAN2_BASE + 0x008)
// Lowest priority flag for mailbox 2
#define LOW2    0x80000000
// Lowest priority flag for mailbox 1
#define LOW1    0x40000000
// Lowest priority flag for mailbox 0
#define LOW0    0x20000000
// Transmit mailbox 2 empty
#define TME2    0x10000000
// Transmit mailbox 1 empty
#define TME1    0x08000000
// Transmit mailbox 0 empty
#define TME0    0x04000000
// Mailbox code
#define CODE_MSK 0x03000000
#define CODE_SFT 24
#define CODE_GET(r) ((r>>CODE_SFT)&0x3)
// Abort request for mailbox 2
#define ABRQ2   0x00800000
// Transmission error of mailbox 2
#define TERR2   0x00080000
// Arbitration lost for mailbox 2
#define ALST2   0x00040000
// Transmission OK of mailbox 2
#define TXOK2   0x00020000
// Request completed mailbox2
#define RQCP2   0x00010000
// Abort request for mailbox 1
#define ABRQ1   0x00008000
// Transmission error of mailbox 1
#define TERR1   0x00000800
// Arbitration lost for mailbox 1
#define ALST1   0x00000400
// Transmission OK of mailbox 1
#define TXOK1   0x00000200
// Request completed mailbox1
#define RQCP1   0x00000100
// Abort request for mailbox 0
#define ABRQ0   0x00000080
// Transmission error of mailbox 0
#define TERR0   0x00000008
// Arbitration lost for mailbox 0
#define ALST0   0x00000004
// Transmission OK of mailbox 0
#define TXOK0   0x00000002
// Request completed mailbox0
#define RQCP0   0x00000001

/* CAN receive FIFO 0 register */
#define CAN1_RF0R MMIO32(CAN1_BASE + 0x00c)
#define CAN2_RF0R MMIO32(CAN2_BASE + 0x00c)
// Release FIFO 0 output mailbox
#define RFOM0       0x20
// FIFO 0 overrun
#define FOVR0       0x10
// FIFO 0 full, when three messages are stored in the FIFO.
#define FULL0       0x08
// FIFO 0 message pending, how many messages are pending
#define FMP0_MSK    0x03
#define FMP0_GET(r) (r&FMP0_MSK)

/* CAN receive FIFO 1 register */
#define CAN1_RF1R MMIO32(CAN1_BASE + 0x010)
#define CAN2_RF1R MMIO32(CAN2_BASE + 0x010)
// Release FIFO 1 output mailbox
#define RFOM1       0x20
// FIFO 1 overrun
#define FOVR1       0x10
// FIFO 1 full, when three messages are stored in the FIFO.
#define FULL1       0x08
// FIFO 1 message pending, how many messages are pending
#define FMP1_MSK    0x03
#define FMP1_GET(r) (r&FMP1_MSK)

/* CAN interrupt enable register */
#define CAN1_IER MMIO32(CAN1_BASE + 0x014)
#define CAN2_IER MMIO32(CAN2_BASE + 0x014)
// Sleep interrupt enable
#define SLKIE   0x20000
// Wakeup interrupt enable
#define WKUIE   0x10000
// Error interrupt enable
#define ERRIE   0x08000
// Last error code interrupt enable
#define LECIE   0x00800
// Bus-off interrupt enable
#define BOFIE   0x00400
// Error passive interrupt enable
#define EPVIE   0x00200
// Error warning interrupt enable
#define EWGIE   0x00100
// FIFO overrun interrupt enable
#define FOVIE1  0x00040
// FIFO full interrupt enable
#define FFIE1   0x00020
// FIFO message pending interrupt enable
#define FMPIE1  0x00010
// FIFO overrun interrupt enable
#define FOVIE0  0x00008
// FIFO full interrupt enable
#define FFIE0   0x00004
// FIFO message pending interrupt enable
#define FMPIE0  0x00002
// Transmit mailbox empty interrupt enable
#define TMEIE   0x00001

/* CAN error status register */
#define CAN1_ESR MMIO32(CAN1_BASE + 0x018)
#define CAN2_ESR MMIO32(CAN2_BASE + 0x018)
// Receive error counter
#define REC_SFT                 23
#define REC_MSK                 0xff000000
#define REC_GET(r)              ((r>>REC_SFT)&0xff)
// Least significant byte of the 9-bit transmit error counter
#define TEC_SFT                 16
#define TEC_MSK                 0x00ff0000
#define TEC_GET(r)              ((r>>TEC_SFT)&0xff)
// Last error code
#define LEC_NO_ERR              0x00000000
#define LEC_STUFF_ERR           0x00000010
#define LEC_FORM_ERR            0x00000020
#define LEC_ACK_ERR             0x00000030
#define LEC_BIT_RX_ERR          0x00000040
#define LEC_BIT_DOMINANT_ERR    0x00000050
#define LEC_CRC_ERR             0x00000060
#define LEC_SET                 0x00000070
// Bus-off flag
#define BOFF                    0x00000004
// Error passive flag
#define EPVF                    0x00000002
// Error warning flag
#define EWGF                    0x00000001

/* CAN bit timing register */
#define CAN1_BTR MMIO32(CAN1_BASE + 0x01c)
#define CAN2_BTR MMIO32(CAN2_BASE + 0x01c)
// Silent mode (debug)
#define SILM        0x80000000
// Loop back mode (debug)
#define LBKM        0x40000000
// Resynchronization jump width tRJW = tq x (SJW[1:0] + 1)
#define SJW_MSK     0x03000000
#define SJW_SFT     24
#define SJW_GET(r)  ((r>>SJW_SFT)&0x3)
#define SJW_SET(n)  ((n<<SJW_SFT)&SJW_MSK)
// Time segment 2 tBS2 = tq x (TS2[2:0] + 1)
#define TS2_MSK     0x00700000
#define TS2_SFT     20
#define TS2_GET(r)  ((r>>TS2_SFT)&0x7)
#define TS2_SET(n)  ((n<<TS2_SFT)&TS2_MSK)
// Time segment 1 tBS1 = tq x (TS1[3:0] + 1)
#define TS1_MSK     0x000f0000
#define TS1_SFT     16
#define TS1_GET(r)  ((r>>TS1_SFT)&0xf)
#define TS1_SET(n)  ((n<<TS1_SFT)&TS1_MSK)
// Baud rate prescaler tq = (BRP[9:0]+1) x tPCLK
#define BRP_MSK     0x000003ff
#define BRP_SET(r)  (r&BRP_MSK)
#define BRP_GET(n)  (n&BRP_MSK)

/* CAN TX mailbox identifier register */
#define CAN1_TI0R MMIO32(CAN1_BASE + 0x180)
#define CAN2_TI0R MMIO32(CAN2_BASE + 0x180)
#define CAN1_TI1R MMIO32(CAN1_BASE + 0x190)
#define CAN2_TI1R MMIO32(CAN2_BASE + 0x190)
#define CAN1_TI2R MMIO32(CAN1_BASE + 0x1a0)
#define CAN2_TI2R MMIO32(CAN2_BASE + 0x1a0)
// Standard identifier
#define STID_MSK    0xffe00000
#define STID_SFT    21
#define STID_GET(r) ((r>>STID_SFT)&0x7ff)
#define STID_SET(n) ((n<<STID_SFT)&STID_MSK)
// Extended identifier
#define EXID_MSK    0xfffffff8
#define EXID_SFT    3
#define EXID_GET(r) ((r>>EXID_SFT)&0x1fffffff)
#define EXID_SET(n) ((n<<EXID_SFT)&EXID_MSK)
// Identifier extension 1: Extended identifier.
#define IDE         0x00000004
// Remote transmission request 1: Remote frame
#define RTR         0x00000002
// Transmit mailbox request
#define TXRQ        0x00000001

/* CAN mailbox data length control and time stamp register */
#define CAN1_TDT0R MMIO32(CAN1_BASE + 0x184)
#define CAN2_TDT0R MMIO32(CAN2_BASE + 0x184)
#define CAN1_TDT1R MMIO32(CAN1_BASE + 0x194)
#define CAN2_TDT1R MMIO32(CAN2_BASE + 0x194)
#define CAN1_TDT2R MMIO32(CAN1_BASE + 0x1a4)
#define CAN2_TDT2R MMIO32(CAN2_BASE + 0x1a4)
// Message time stamp
#define TIME_MSK    0xffff0000
#define TIME_SFT    16
#define TIME_GET(r) ((r>>TIME_SFT)&0xffff)
#define TIME_SET(n) ((n<<TIME_SFT)&TIME_MSK)
// Transmit global time 1: Time stamp TIME[15:0] value is sent
#define TGT         0x00000100
// Data length code
#define DLC_MSK     0x0000000f
#define DLC_GET(r)  (r&DLC_MSK)
#define DLC_SET(n)  (n&DLC_MSK)

/* CAN mailbox data low register */
#define CAN1_TDL0R MMIO32(CAN1_BASE + 0x188)
#define CAN2_TDL0R MMIO32(CAN2_BASE + 0x188)
#define CAN1_TDL1R MMIO32(CAN1_BASE + 0x198)
#define CAN2_TDL1R MMIO32(CAN2_BASE + 0x198)
#define CAN1_TDL2R MMIO32(CAN1_BASE + 0x1a8)
#define CAN2_TDL2R MMIO32(CAN2_BASE + 0x1a8)
// Data Byte 3
#define DATA3_MSK       0xff000000
#define DATA3_SFT       24
#define DATA3_SET(n)    ((n<<DATA3_SFT)&DATA3_MSK)
#define DATA3_GET(r)    ((r>>DATA3_SFT)&0xff)
// Data Byte 2
#define DATA2_MSK       0x00ff0000
#define DATA2_SFT       16
#define DATA2_SET(n)    ((n<<DATA2_SFT)&DATA2_MSK)
#define DATA2_GET(r)    ((r>>DATA2_SFT)&0xff)
// Data Byte 1
#define DATA1_MSK       0x0000ff00
#define DATA1_SFT       8
#define DATA1_SET(n)    ((n<<DATA1_SFT)&DATA1_MSK)
#define DATA1_GET(r)    ((r>>DATA1_SFT)&0xff)
// Data Byte 0
#define DATA0_MSK       0x000000ff
#define DATA0_SFT       0
#define DATA0_SET(n)    ((n<<DATA0_SFT)&DATA0_MSK)
#define DATA0_GET(r)    ((r>>DATA0_SFT)&0xff)

/* CAN mailbox data high register */
#define CAN1_TDH0R MMIO32(CAN1_BASE + 0x0)
#define CAN2_TDH0R MMIO32(CAN2_BASE + 0x0)
#define CAN1_TDH1R MMIO32(CAN1_BASE + 0x19c)
#define CAN2_TDH1R MMIO32(CAN2_BASE + 0x19c)
#define CAN1_TDH2R MMIO32(CAN1_BASE + 0x1ac)
#define CAN2_TDH2R MMIO32(CAN2_BASE + 0x1ac)
// Data Byte 7
#define DATA7_MSK       0xff000000
#define DATA7_SFT       24
#define DATA7_SET(n)    ((n<<DATA7_SFT)&DATA7_MSK)
#define DATA7_GET(r)    ((r>>DATA7_SFT)&0xff)
// Data Byte 6
#define DATA6_MSK       0x00ff0000
#define DATA6_SFT       16
#define DATA6_SET(n)    ((n<<DATA6_SFT)&DATA6_MSK)
#define DATA6_GET(r)    ((r>>DATA6_SFT)&0xff)
// Data Byte 5
#define DATA5_MSK       0x0000ff00
#define DATA5_SFT       8
#define DATA5_SET(n)    ((n<<DATA5_SFT)&DATA5_MSK)
#define DATA5_GET(r)    ((r>>DATA5_SFT)&0xff)
// Data Byte 4
#define DATA4_MSK       0x000000ff
#define DATA4_SFT       0
#define DATA4_SET(n)    ((n<<DATA4_SFT)&DATA4_MSK)
#define DATA4_GET(r)    ((r>>DATA4_SFT)&0xff)

/* CAN receive FIFO mailbox identifier register */
#define CAN1_RI1R MMIO32(CAN1_BASE + 0x1c0)
#define CAN2_RI1R MMIO32(CAN2_BASE + 0x1c0)
#define CAN1_RI0R MMIO32(CAN1_BASE + 0x1b0)
#define CAN2_RI0R MMIO32(CAN2_BASE + 0x1b0)
// Standard identifier
// Extended identifier
// Identifier extension 1: Extended identifier.
// Remote transmission request 1: Remote frame

/* CAN receive FIFO mailbox data length control and time stamp reg */
#define CAN1_RDT1R MMIO32(CAN1_BASE + 0x1c4)
#define CAN2_RDT1R MMIO32(CAN2_BASE + 0x1c4)
#define CAN1_RDT0R MMIO32(CAN1_BASE + 0x1b4)
#define CAN2_RDT0R MMIO32(CAN2_BASE + 0x1b4)
// Message time stamp
// Filter match index
#define FMI_MSK     0x0000ff00
#define FMI_SFT     8
#define FMI_GET(r)  ((r>>FMI_SFT)&0xff)
// Data length code

/* CAN receive FIFO mailbox data low register */
#define CAN1_RDL1R MMIO32(CAN1_BASE + 0x1c8)
#define CAN2_RDL1R MMIO32(CAN2_BASE + 0x1c8)
#define CAN1_RDL0R MMIO32(CAN1_BASE + 0x1b8)
#define CAN2_RDL0R MMIO32(CAN2_BASE + 0x1b8)
// Data Byte 3
// Data Byte 2
// Data Byte 1
// Data Byte 0

/* CAN receive FIFO mailbox data high register */
#define CAN1_RDH1R MMIO32(CAN1_BASE + 0x1cc)
#define CAN2_RDH1R MMIO32(CAN2_BASE + 0x1cc)
#define CAN1_RDH0R MMIO32(CAN1_BASE + 0x1bc)
#define CAN2_RDH0R MMIO32(CAN2_BASE + 0x1bc)
// Data Byte 7
// Data Byte 6
// Data Byte 5
// Data Byte 4

/* CAN filter master register */
#define CAN1_FMR MMIO32(CAN1_BASE + 0x200)
#define CAN2_FMR MMIO32(CAN2_BASE + 0x200)
// CAN2 start bank, start bank for the CAN2 interface
#define CAN2SB_MSK      0x3f00
#define CAN2SB_SFT      8
#define CAN2SB_SET(n)   ((n<<CAN2SB_SFT)&CAN2SB_MSK)
#define CAN2SB_GET(r)   ((r>>CAN2SB_SFT)&0x7f)
// Filter init mode 0: Active filters mode.
#define FINIT           0x0001

/* CAN filter mode register */
#define CAN1_FM1R MMIO32(CAN1_BASE + 0x204)
#define CAN2_FM1R MMIO32(CAN2_BASE + 0x204)
// 1: Two 32-bit registers of filter bank x are in Identifier List mode.
#define FBM0    0x00000001
#define FBM1    0x00000002
#define FBM2    0x00000004
#define FBM3    0x00000008
#define FBM4    0x00000010
#define FBM5    0x00000020
#define FBM6    0x00000040
#define FBM7    0x00000080
#define FBM8    0x00000100
#define FBM9    0x00000200
#define FBM10   0x00000400
#define FBM11   0x00000800
#define FBM12   0x00001000
#define FBM13   0x00002000
#define FBM14   0x00004000
#define FBM15   0x00008000
#define FBM16   0x00010000
#define FBM17   0x00020000
#define FBM18   0x00040000
#define FBM19   0x00080000
#define FBM20   0x00100000
#define FBM21   0x00200000
#define FBM22   0x00400000
#define FBM23   0x00800000
#define FBM24   0x01000000
#define FBM25   0x02000000
#define FBM26   0x04000000
#define FBM27   0x08000000

/* CAN filter scale register */
#define CAN1_FS1R MMIO32(CAN1_BASE + 0x20c)
#define CAN2_FS1R MMIO32(CAN2_BASE + 0x20c)
// Filter scale configuration 1: Single 32-bit scale configuration
#define FSC0    0x00000001
#define FSC1    0x00000002
#define FSC2    0x00000004
#define FSC3    0x00000008
#define FSC4    0x00000010
#define FSC5    0x00000020
#define FSC6    0x00000040
#define FSC7    0x00000080
#define FSC8    0x00000100
#define FSC9    0x00000200
#define FSC10   0x00000400
#define FSC11   0x00000800
#define FSC12   0x00001000
#define FSC13   0x00002000
#define FSC14   0x00004000
#define FSC15   0x00008000
#define FSC16   0x00010000
#define FSC17   0x00020000
#define FSC18   0x00040000
#define FSC19   0x00080000
#define FSC20   0x00100000
#define FSC21   0x00200000
#define FSC22   0x00400000
#define FSC23   0x00800000
#define FSC24   0x01000000
#define FSC25   0x02000000
#define FSC26   0x04000000
#define FSC27   0x08000000

/* CAN filter FIFO assignment register */
#define CAN1_FFA1R MMIO32(CAN1_BASE + 0x214)
#define CAN2_FFA1R MMIO32(CAN2_BASE + 0x214)
// Filter FIFO assignment for filter x
#define FFA0    0x00000001
#define FFA1    0x00000002
#define FFA2    0x00000004
#define FFA3    0x00000008
#define FFA4    0x00000010
#define FFA5    0x00000020
#define FFA6    0x00000040
#define FFA7    0x00000080
#define FFA8    0x00000100
#define FFA9    0x00000200
#define FFA10   0x00000400
#define FFA11   0x00000800
#define FFA12   0x00001000
#define FFA13   0x00002000
#define FFA14   0x00004000
#define FFA15   0x00008000
#define FFA16   0x00010000
#define FFA17   0x00020000
#define FFA18   0x00040000
#define FFA19   0x00080000
#define FFA20   0x00100000
#define FFA21   0x00200000
#define FFA22   0x00400000
#define FFA23   0x00800000
#define FFA24   0x01000000
#define FFA25   0x02000000
#define FFA26   0x04000000
#define FFA27   0x08000000

/* CAN filter activation register */
#define CAN1_FA1R MMIO32(CAN1_BASE + 0x21c)
#define CAN2_FA1R MMIO32(CAN2_BASE + 0x21c)
// The software sets this bit to activate Filter x.
#define FACT0   0x00000001
#define FACT1   0x00000002
#define FACT2   0x00000004
#define FACT3   0x00000008
#define FACT4   0x00000010
#define FACT5   0x00000020
#define FACT6   0x00000040
#define FACT7   0x00000080
#define FACT8   0x00000100
#define FACT9   0x00000200
#define FACT10  0x00000400
#define FACT11  0x00000800
#define FACT12  0x00001000
#define FACT13  0x00002000
#define FACT14  0x00004000
#define FACT15  0x00008000
#define FACT16  0x00010000
#define FACT17  0x00020000
#define FACT18  0x00040000
#define FACT19  0x00080000
#define FACT20  0x00100000
#define FACT21  0x00200000
#define FACT22  0x00400000
#define FACT23  0x00800000
#define FACT24  0x01000000
#define FACT25  0x02000000
#define FACT26  0x04000000
#define FACT27  0x08000000

/* Filter bank i register x */
#define CAN1_F0R1 MMIO32(CAN1_BASE + 0x240)
#define CAN2_F0R1 MMIO32(CAN2_BASE + 0x240)
#define CAN1_F0R2 MMIO32(CAN1_BASE + 0x244)
#define CAN2_F0R2 MMIO32(CAN2_BASE + 0x244)

#define CAN1_F1R1 MMIO32(CAN1_BASE + 0x248)
#define CAN2_F1R1 MMIO32(CAN2_BASE + 0x248)
#define CAN1_F1R2 MMIO32(CAN1_BASE + 0x24c)
#define CAN2_F1R2 MMIO32(CAN2_BASE + 0x24c)

#define CAN1_F2R1 MMIO32(CAN1_BASE + 0x250)
#define CAN2_F2R1 MMIO32(CAN2_BASE + 0x250)
#define CAN1_F2R2 MMIO32(CAN1_BASE + 0x254)
#define CAN2_F2R2 MMIO32(CAN2_BASE + 0x254)

#define CAN1_F3R1 MMIO32(CAN1_BASE + 0x258)
#define CAN2_F3R1 MMIO32(CAN2_BASE + 0x258)
#define CAN1_F3R2 MMIO32(CAN1_BASE + 0x25c)
#define CAN2_F3R2 MMIO32(CAN2_BASE + 0x25c)

#define CAN1_F4R1 MMIO32(CAN1_BASE + 0x260)
#define CAN2_F4R1 MMIO32(CAN2_BASE + 0x260)
#define CAN1_F4R2 MMIO32(CAN1_BASE + 0x264)
#define CAN2_F4R2 MMIO32(CAN2_BASE + 0x264)

#define CAN1_F5R1 MMIO32(CAN1_BASE + 0x268)
#define CAN2_F5R1 MMIO32(CAN2_BASE + 0x268)
#define CAN1_F5R2 MMIO32(CAN1_BASE + 0x26c)
#define CAN2_F5R2 MMIO32(CAN2_BASE + 0x26c)

#define CAN1_F6R1 MMIO32(CAN1_BASE + 0x270)
#define CAN2_F6R1 MMIO32(CAN2_BASE + 0x270)
#define CAN1_F6R2 MMIO32(CAN1_BASE + 0x274)
#define CAN2_F6R2 MMIO32(CAN2_BASE + 0x274)

#define CAN1_F7R1 MMIO32(CAN1_BASE + 0x278)
#define CAN2_F7R1 MMIO32(CAN2_BASE + 0x278)
#define CAN1_F7R2 MMIO32(CAN1_BASE + 0x27c)
#define CAN2_F7R2 MMIO32(CAN2_BASE + 0x27c)

#define CAN1_F8R1 MMIO32(CAN1_BASE + 0x280)
#define CAN2_F8R1 MMIO32(CAN2_BASE + 0x280)
#define CAN1_F8R2 MMIO32(CAN1_BASE + 0x284)
#define CAN2_F8R2 MMIO32(CAN2_BASE + 0x284)

#define CAN1_F9R1 MMIO32(CAN1_BASE + 0x288)
#define CAN2_F9R1 MMIO32(CAN2_BASE + 0x288)
#define CAN1_F9R2 MMIO32(CAN1_BASE + 0x28c)
#define CAN2_F9R2 MMIO32(CAN2_BASE + 0x28c)

#define CAN1_F10R1 MMIO32(CAN1_BASE + 0x290)
#define CAN2_F10R1 MMIO32(CAN2_BASE + 0x290)
#define CAN1_F10R2 MMIO32(CAN1_BASE + 0x294)
#define CAN2_F10R2 MMIO32(CAN2_BASE + 0x294)

#define CAN1_F11R1 MMIO32(CAN1_BASE + 0x298)
#define CAN2_F11R1 MMIO32(CAN2_BASE + 0x298)
#define CAN1_F11R2 MMIO32(CAN1_BASE + 0x29c)
#define CAN2_F11R2 MMIO32(CAN2_BASE + 0x29c)

#define CAN1_F12R1 MMIO32(CAN1_BASE + 0x2a0)
#define CAN2_F12R1 MMIO32(CAN2_BASE + 0x2a0)
#define CAN1_F12R2 MMIO32(CAN1_BASE + 0x2a4)
#define CAN2_F12R2 MMIO32(CAN2_BASE + 0x2a4)

#define CAN1_F13R1 MMIO32(CAN1_BASE + 0x2a8)
#define CAN2_F13R1 MMIO32(CAN2_BASE + 0x2a8)
#define CAN1_F13R2 MMIO32(CAN1_BASE + 0x2ac)
#define CAN2_F13R2 MMIO32(CAN2_BASE + 0x2ac)

#define CAN1_F14R1 MMIO32(CAN1_BASE + 0x2b0)
#define CAN2_F14R1 MMIO32(CAN2_BASE + 0x2b0)
#define CAN1_F14R2 MMIO32(CAN1_BASE + 0x2b4)
#define CAN2_F14R2 MMIO32(CAN2_BASE + 0x2b4)

#define CAN1_F15R1 MMIO32(CAN1_BASE + 0x2b8)
#define CAN2_F15R1 MMIO32(CAN2_BASE + 0x2b8)
#define CAN1_F15R2 MMIO32(CAN1_BASE + 0x2bc)
#define CAN2_F15R2 MMIO32(CAN2_BASE + 0x2bc)

#define CAN1_F16R1 MMIO32(CAN1_BASE + 0x2c0)
#define CAN2_F16R1 MMIO32(CAN2_BASE + 0x2c0)
#define CAN1_F16R2 MMIO32(CAN1_BASE + 0x2c4)
#define CAN2_F16R2 MMIO32(CAN2_BASE + 0x2c4)

#define CAN1_F17R1 MMIO32(CAN1_BASE + 0x2c8)
#define CAN2_F17R1 MMIO32(CAN2_BASE + 0x2c8)
#define CAN1_F17R2 MMIO32(CAN1_BASE + 0x2cc)
#define CAN2_F17R2 MMIO32(CAN2_BASE + 0x2cc)

#define CAN1_F18R1 MMIO32(CAN1_BASE + 0x2d0)
#define CAN2_F18R1 MMIO32(CAN2_BASE + 0x2d0)
#define CAN1_F18R2 MMIO32(CAN1_BASE + 0x2d4)
#define CAN2_F18R2 MMIO32(CAN2_BASE + 0x2d4)

#define CAN1_F19R1 MMIO32(CAN1_BASE + 0x2d8)
#define CAN2_F19R1 MMIO32(CAN2_BASE + 0x2d8)
#define CAN1_F19R2 MMIO32(CAN1_BASE + 0x2dc)
#define CAN2_F19R2 MMIO32(CAN2_BASE + 0x2dc)

#define CAN1_F20R1 MMIO32(CAN1_BASE + 0x2e0)
#define CAN2_F20R1 MMIO32(CAN2_BASE + 0x2e0)
#define CAN1_F20R2 MMIO32(CAN1_BASE + 0x2e4)
#define CAN2_F20R2 MMIO32(CAN2_BASE + 0x2e4)

#define CAN1_F21R1 MMIO32(CAN1_BASE + 0x2e8)
#define CAN2_F21R1 MMIO32(CAN2_BASE + 0x2e8)
#define CAN1_F21R2 MMIO32(CAN1_BASE + 0x2ec)
#define CAN2_F21R2 MMIO32(CAN2_BASE + 0x2ec)

#define CAN1_F22R1 MMIO32(CAN1_BASE + 0x2f0)
#define CAN2_F22R1 MMIO32(CAN2_BASE + 0x2f0)
#define CAN1_F22R2 MMIO32(CAN1_BASE + 0x2f4)
#define CAN2_F22R2 MMIO32(CAN2_BASE + 0x2f4)

#define CAN1_F23R1 MMIO32(CAN1_BASE + 0x2f8)
#define CAN2_F23R1 MMIO32(CAN2_BASE + 0x2f8)
#define CAN1_F23R2 MMIO32(CAN1_BASE + 0x2fc)
#define CAN2_F23R2 MMIO32(CAN2_BASE + 0x2fc)

#define CAN1_F24R1 MMIO32(CAN1_BASE + 0x300)
#define CAN2_F24R1 MMIO32(CAN2_BASE + 0x300)
#define CAN1_F24R2 MMIO32(CAN1_BASE + 0x304)
#define CAN2_F24R2 MMIO32(CAN2_BASE + 0x304)

#define CAN1_F25R1 MMIO32(CAN1_BASE + 0x308)
#define CAN2_F25R1 MMIO32(CAN2_BASE + 0x308)
#define CAN1_F25R2 MMIO32(CAN1_BASE + 0x30c)
#define CAN2_F25R2 MMIO32(CAN2_BASE + 0x30c)

#define CAN1_F26R1 MMIO32(CAN1_BASE + 0x310)
#define CAN2_F26R1 MMIO32(CAN2_BASE + 0x310)
#define CAN1_F26R2 MMIO32(CAN1_BASE + 0x314)
#define CAN2_F26R2 MMIO32(CAN2_BASE + 0x314)

#define CAN1_F27R1 MMIO32(CAN1_BASE + 0x318)
#define CAN2_F27R1 MMIO32(CAN2_BASE + 0x318)
#define CAN1_F27R2 MMIO32(CAN1_BASE + 0x31c)
#define CAN2_F27R2 MMIO32(CAN2_BASE + 0x31c)
// FB[31:0]: Filter bits 0: Dominant bit is exp 1: Recessive bit is exp

#endif
