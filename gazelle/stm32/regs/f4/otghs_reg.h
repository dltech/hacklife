#ifndef H_OTGHS_REG
#define H_OTGHS_REG
/*
 * Part of Belkin STM32 HAL, USB on-the-go high-speed (OTG_HS)
 * register definitions of STM32F4xx MCUs.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/****** OTG_HS global registers *******/
/* OTG_HS control and status register */
#define OTG_HS_GOTGCTL      MMIO32(USB_OTG_HS_BASE + 0x000)
// B-session valid
#define BSVLD   0x80000
// A-session valid
#define ASVLD   0x40000
// Long/short debounce time
#define DBCT    0x20000
// Connector ID status
#define CIDSTS  0x10000
// Device HNP enabled
#define DHNPEN  0x00800
// host set HNP enable
#define HSHNPEN 0x00400
// HNP request
#define HNPRQ   0x00200
// Host negotiation success
#define HNGSCS  0x00100
// Session request
#define SRQ     0x00002
// Session request success
#define SRQSCS  0x00001

/* OTG_HS interrupt register */
#define OTG_HS_GOTGINT      MMIO32(USB_OTG_HS_BASE + 0x004)
// Debounce done
#define DBCDNE  0x80000
// A-device timeout change
#define ADTOCHG 0x40000
// Host negotiation detected
#define HNGDET  0x20000
// Host negotiation success status change
#define HNSSCHG 0x00200
// Session request success status change
#define SRSSCHG 0x00100
// Session end detected
#define SEDET   0x00004

/* OTG_HS AHB configuration register */
#define OTG_HS_GAHBCFG      MMIO32(USB_OTG_HS_BASE + 0x008)
// Periodic TxFIFO empty level
#define PTXFELVL        0x100
// TxFIFO empty level
#define TXFELVL         0x080
// Burst length/type
#define HBSTLEN_SINGLE  0x000
#define HBSTLEN_INCR    0x002
#define HBSTLEN_INCR4   0x006
#define HBSTLEN_INCR8   0x00a
#define HBSTLEN_INCR16  0x00e
// Global interrupt mask
#define GINTMSK         0x001

/* OTG_HS USB configuration register */
#define OTG_HS_GUSBCFG      MMIO32(USB_OTG_HS_BASE + 0x00c)
// Corrupt Tx packet
#define CTXPKT          0x80000000
// Force device mode
#define FDMOD           0x40000000
// Force host mode
#define FHMOD           0x20000000
// ULPI interface protect disable
#define ULPIIPD         0x02000000
// Indicator pass through
#define PTCI            0x01000000
// Indicator complement
#define PCCI            0x00800000
// TermSel DLine pulsing selection
#define TSDPS           0x00400000
// ULPI external VBUS indicator
#define ULPIEVBUSI      0x00200000
// ULPI External VBUS Drive
#define ULPIEVBUSD      0x00100000
// ULPI Clock SuspendM
#define ULPICSM         0x00080000
// ULPI Auto-resume
#define ULPIAR          0x00040000
// ULPI FS/LS select
#define ULPIFSLS        0x00020000
// PHY Low-power clock select
#define PHYLPCS         0x00008000
// USB turnaround time
#define TRDT14_15       0x00003c00
#define TRDT15_16       0x00003800
#define TRDT16_17       0x00003400
#define TRDT17_18       0x00003000
#define TRDT18_20       0x00002c00
#define TRDT20_22       0x00002800
#define TRDT22_24       0x00002400
#define TRDT24_27       0x00002000
#define TRDT27_32       0x00001c00
#define TRDT32_GT       0x00001800
// HNP-capable
#define HNPCAP          0x00000200
// SRP-capable
#define SRPCAP          0x00000100
// Full speed serial transceiver select
#define PHYSEL          0x00000040
// FS timeout calibration
#define TOCAL_MSK       0x00000007
#define TOCAL_GET   (OTG_HS_GUSBCFG&TOCAL_MSK)
#define TOCAL_SET(x)    (x&TOCAL_MSK)

/* OTG_HS reset register */
#define OTG_HS_GRSTCTL      MMIO32(USB_OTG_HS_BASE + 0x010)
// AHB master idle
#define AHBIDL      0x80000000
// TxFIFO number
#define TXFNUM0     0x00000000
#define TXFNUM1     0x00000040
#define TXFNUM2     0x00000080
#define TXFNUM3     0x000000c0
#define TXFNUM4     0x00000100
#define TXFNUM15    0x00000140
#define TXFNUM_ALL  0x00000400
// TxFIFO flush
#define TXFFLSH     0x00000020
// RxFIFO flush
#define RXFFLSH     0x00000010
// Host frame counter reset
#define FCRST       0x00000004
// HCLK soft reset
#define HSRST       0x00000002
// Core soft reset
#define CSRST       0x00000001

/* OTG_HS core interrupt register */
#define OTG_HS_GINTSTS      MMIO32(USB_OTG_HS_BASE + 0x014)
// Resume/remote wakeup detected interrupt
#define WKUPINT         0x80000000
// Session request/new session detected interrupt
#define SRQINT          0x40000000
// Disconnect detected interrupt
#define DISCINT         0x20000000
// Connector ID status change
#define CIDSCHG         0x10000000
// Periodic TxFIFO empty
#define PTXFE           0x04000000
// Host channels interrupt
#define HCINT           0x02000000
// Host port interrupt
#define HPRTINT         0x01000000
// Data fetch suspended
#define DATAFSUSP       0x00400000
// Incomplete periodic transfer
#define IPXFR           0x00200000
// Incomplete isochronous OUT transfer
#define INCOMPISOOUT    0x00200000
// Incomplete isochronous IN transfer
#define IISOIXFR        0x00100000
// OUT endpoint interrupt
#define OEPINT          0x00080000
// IN endpoint interrupt
#define IEPINT          0x00040000
// End of periodic frame interrupt
#define EOPF            0x00008000
// Isochronous OUT packet dropped interrupt
#define ISOODRP         0x00004000
// Enumeration done
#define ENUMDNE         0x00002000
// USB reset
#define USBRST          0x00001000
// USB suspend
#define USBSUSP         0x00000800
// Early suspend
#define ESUSP           0x00000400
// Global OUT NAK effective
#define GONAKEFF        0x00000080
// Global IN non-periodic NAK effective
#define GINAKEFF        0x00000040
// Non-periodic TxFIFO empty
#define NPTXFE          0x00000020
// RxFIFO non-empty
#define RXFLVL          0x00000010
// Start of frame
#define SOF             0x00000008
// OTG interrupt
#define OTGINT          0x00000004
// Mode mismatch interrupt
#define MMIS            0x00000002
// Current mode of operation
#define CMOD            0x00000001

/* OTG_HS interrupt mask register */
#define OTG_HS_GINTMSK      MMIO32(USB_OTG_HS_BASE + 0x018)
// Resume/remote wakeup detected interrupt mask
#define WUIM        0x80000000
// Session request/new session detected interrupt mask
#define SRQIM       0x40000000
// Disconnect detected interrupt mask
#define DISCINT     0x20000000
// Connector ID status change mask
#define CIDSCHGM    0x10000000
// Periodic TxFIFO empty mask
#define PTXFEM      0x04000000
// Host channels interrupt mask
#define HCIM        0x02000000
// Host port interrupt mask
#define PRTIM       0x01000000
// Incomplete periodic transfer mask
#define IPXFRM      0x00200000
// Incomplete isochronous OUT transfer mask
#define IISOOXFRM   0x00200000
// Incomplete isochronous IN transfer mask
#define IISOIXFRM   0x00100000
// OUT endpoints interrupt mask
#define OEPINT      0x00080000
// IN endpoints interrupt mask
#define IEPINT      0x00040000
// End of periodic frame interrupt mask
#define EOPFM       0x00008000
// Isochronous OUT packet dropped interrupt mask
#define ISOODRPM    0x00004000
// Enumeration done mask
#define ENUMDNEM    0x00002000
// USB reset mask
#define USBRST      0x00001000
// USB suspend mask
#define USBSUSPM    0x00000800
// Early suspend mask
#define ESUSPM      0x00000400
// Global OUT NAK effective mask
#define GONAKEFFM   0x00000080
// Global non-periodic IN NAK effective mask
#define GINAKEFFM   0x00000040
// Non-periodic TxFIFO empty mask
#define NPTXFEM     0x00000020
// Receive FIFO non-empty mask
#define RXFLVLM     0x00000010
// Start of frame mask
#define SOFM        0x00000008
// OTG interrupt mask
#define OTGINT      0x00000004
// Mode mismatch interrupt mask
#define MMISM       0x00000002

/* OTG_HS Receive status debug read */
#define OTG_HS_GRXSTSR      MMIO32(USB_OTG_HS_BASE + 0x01c)
/* OTG status read and pop registers */
#define OTG_HS_GRXSTSP      MMIO32(USB_OTG_HS_BASE + 0x020)
// Host mode
// Packet status
#define PKTSTS_IN_DATA_RECEIVED 0x40000
#define PKTSTS_IN_TRX_COMPLETED 0x60000
#define PKTSTS_DATA_TOGGLE_ERR  0xa0000
#define PKTSTS_CHANNEL_HALTED   0xe0000
// Data PID
#define DPID_DATA0              0x00000
#define DPID_DATA1              0x10000
#define DPID_DATA2              0x08000
#define DPID_MDATA              0x18000
// Byte count
#define BCNT_MSK                0x07ff0
#define BCNT_SFT                4
#define BCNT_GET(r)             ((r>>BCNT_SFT)&0x7ff)
#define BCNT_SET(x)             ((x<<BCNT_SFT)&BCNT_MSK)
// Channel number
#define CHNUM_MSK               0x0000f
#define CHNUM_GET(r)            (r&CHNUM_MSK)
#define CHNUM_SET(x)            (x&CHNUM_MSK)
// Device mode
// Frame number
#define FRMNUM_MSK              0x1e00000
#define FRMNUM_SFT              21
#define FRMNUM_GET(r)           ((r>>FRMNUM_SFT)&0x7ff)
// Packet status
#define PKTSTS_GLOBAL_OUT_NAK   0x0200000
#define PKTSTS_OUT_DATA_PACK_RX 0x0400000
#define PKTSTS_OUT_TRX_COMPLETE 0x0600000
#define PKTSTS_SETUP_TRX_COMPL  0x0800000
#define PKTSTS_SETUP_DATA_RX    0x0c00000
// Endpoint number
#define EPNUM_MSK               0x000000f
#define EPNUM_GET(r)            (r&EPNUM_MSK)
#define EPNUM_SET(x)            (x&EPNUM_MSK)

/* OTG_HS Receive FIFO size register */
#define OTG_HS_GRXFSIZ      MMIO32(USB_OTG_HS_BASE + 0x024)
// RXFD[15:0]: OTG_FS Receive FIFO size register
#define RXFD_MSK    0xffff

/* OTG_HS Host non-periodic transmit FIFO size register */
#define OTG_HS_HNPTXFSIZ    MMIO32(USB_OTG_HS_BASE + 0x028)
// Host mode
// Non-periodic TxFIFO depth
#define NPTXFD_MSK      0xffff0000
#define NPTXFD_SFT      16
#define NPTXFD_GET      ((OTG_FS_HNPTXFSIZ>>NPTXFD_SFT)&0xffff)
#define NPTXFD_SET(x)   ((x<<NPTXFD_SFT)&NPTXFD_MSK)
// Non-periodic transmit RAM start address
#define NPTXFSA_MSK     0x0000ffff
#define NPTXFSA_GET     (OTG_FS_HNPTXFSIZ&NPTXFSA_MSK)
#define NPTXFSA_SET(x)  (x&NPTXFSA_MSK)
// Peripherial mode
// Non-periodic TxFIFO depth
#define TX0FD_MSK       0xffff0000
#define TX0FD_SFT       16
#define TX0FD_GET       ((OTG_FS_HNPTXFSIZ>>NPTXFD_SFT)&0xffff)
#define TX0FD_SET(x)    ((x<<NPTXFD_SFT)&NPTXFD_MSK)
// Non-periodic transmit RAM start address
#define TX0FSA_MSK      0x0000ffff
#define TX0FSA_GET      (OTG_FS_HNPTXFSIZ&NPTXFSA_MSK)
#define TX0FSA_SET(x)   (x&NPTXFSA_MSK)

// /* Endpoint 0 Transmit FIFO size */
// #define OTG_FS_DIEPTXF0     MMIO32(USB_OTG_FS_BASE + 0x028)

/* OTG_HS non-periodic transmit FIFO/queue status register */
#define OTG_HS_HNPTXSTS     MMIO32(USB_OTG_HS_BASE + 0x02c)
// Top of the non-periodic transmit request queue
#define NPTXQTOP_NEP_MSK        0x78000000
#define NPTXQTOP_NEP_SFT        27
#define NPTXQTOP_NEP_GET        ((OTG_FS_HNPTXSTS>>NPTXQTOP_NEP_SFT)&0xf)
#define NPTXQTOP_IN_OUT_TOKEN   0x00000000
#define NPTXQTOP_ZERO_LEN_PACK  0x02000000
#define NPTXQTOP_PING_TOKEN     0x04000000
#define NPTXQTOP_CH_HALT        0x06000000
#define NPTXQTOP_TERMINATE      0x01000000
// Non-periodic transmit request queue space available
#define NPTQXSAV_MSK            0x00ff0000
#define NPTQXSAV_SFT            16
#define NPTXFSAV_GET            ((OTG_FS_HNPTXSTS>>NPTQXSAV_SFT)&0xff)
// Non-periodic TxFIFO space available
#define NPTXFSAV_MSK            0x0000ffff
#define NPTXFSAV_FULL           0x00000000
#define NPTXFSAV_GET            (OTG_FS_HNPTXSTS&NPTXFSAV_MSK)

//#define OTG_HS_GI2CCTL      MMIO32(USB_OTG_HS_BASE + 0x030)

/* OTG_HS general core configuration register */
#define OTG_HS_GCCFG        MMIO32(USB_OTG_HS_BASE + 0x038)
// VBUS sensing disable option
#define NOVBUSSENS  0x200000
// SOF output enable
#define SOFOUTEN    0x100000
// Enable the V BUS sensing “B” device
#define VBUSBSEN    0x080000
// Enable the V BUS sensing “A” device
#define VBUSASEN    0x040000
// Enable I2C bus connection for the external I2C PHY interface.
#define I2CPADEN    0x020000
// Power down
#define PWRDWN      0x010000

/* OTG_HS core ID register */
#define OTG_HS_CID          MMIO32(USB_OTG_HS_BASE + 0x03c)
// PRODUCT_ID[31:0]: Product ID field PRODUCT_ID

/* OTG_HS Host periodic transmit FIFO size register */
#define OTG_HS_HPTXFSIZ     MMIO32(USB_OTG_HS_BASE + 0x100)
// Host periodic TxFIFO depth
#define PTXFD_MSK   0xffff0000
#define PTXFD_SFT   16
#define PTXFD_GET   ((OTG_FS_HPTXFSIZ>>PTXFD_SFT)&0xffff)
// Host periodic TxFIFO start address
#define PTXSA_MSK   0x0000ffff
#define PTXSA_GET   (OTG_FS_HPTXFSIZ&PTXSA_MSK)

/* OTG_HS device IN endpoint transmit FIFO size register */
#define OTG_HS_DIEPTXF1     MMIO32(USB_OTG_HS_BASE + 0x104)
#define OTG_HS_DIEPTXF2     MMIO32(USB_OTG_HS_BASE + 0x108)
#define OTG_HS_DIEPTXF3     MMIO32(USB_OTG_HS_BASE + 0x10c)
#define OTG_HS_DIEPTXF4     MMIO32(USB_OTG_HS_BASE + 0x110)
#define OTG_HS_DIEPTXF5     MMIO32(USB_OTG_HS_BASE + 0x114)
// IN endpoint TxFIFO depth
#define INEPTXFD_MSK    0xffff0000
#define INEPTXFD_SFT    16
#define INEPTXFD_GET(r) ((r>>INEPTXFD_SFT)&0xffff)
// IN endpoint FIFOx transmit RAM start address
#define INEPTXSA_MSK   0x0000ffff
#define INEPTXSA_GET(r) (r&INEPTXSA_MSK)

/******** Host-mode registers *********/
/* OTG_HS Host configuration register */
#define OTG_HS_HCFG         MMIO32(USB_OTG_HS_BASE + 0x400)
// FS- and LS-only support
#define FSLSS          0x4
// FS/LS PHY clock select
#define FSLSPCS_RUN48M 0x1
#define FSLSPCS_SEL48M 0x1
#define FSLSPCS_SEL6M  0x2

/* OTG_HS Host frame interval register */
#define OTG_HS_HFIR         MMIO32(USB_OTG_HS_BASE + 0x404)
// Frame interval FRIVL FRIVL[15:0]

/* OTG_HS Host frame number/frame time remaining register */
#define OTG_HS_HFNUM        MMIO32(USB_OTG_HS_BASE + 0x408)
// Frame time remaining
#define FTREM_MSK   0xffff0000
#define FTREM_SFT   16
#define FTREM_GET   ((OTG_FS_HFNUM>>FTREM_SFT)&0xffff)
// Frame number
#define FRNUM_MSK   0x0000ffff
#define FRNUM_GET   (OTG_FS_HFNUM&FRNUM_MSK)

/* OTG_HS_Host periodic transmit FIFO/queue status register */
#define OTG_HS_HPTXSTS      MMIO32(USB_OTG_HS_BASE + 0x410)
// Top of the periodic transmit request queue
#define PTXQTOP_ODD_EVEN        0x80000000
#define PTXQTOP_NEP_MSK         0x78000000
#define PTXQTOP_NEP_SFT         27
#define PTXQTOP_NEP_GET         ((OTG_FS_HPTXSTS>>PTXQTOP_NEP_SFT)&0xf)
#define PTXQTOP_IN_OUT_TYPE     0x00000000
#define PTXQTOP_ZERO_LEN_PACK   0x02000000
#define PTXQTOP_DIS_CH_CMD      0x06000000
#define PTXQTOP_TERMINATE       0x01000000
// Periodic transmit request queue space available
#define PTXQSAV_MSK             0x000f0000
#define PTXQSAV_FULL            0x00000000
#define PTXQSAV_SFT             16
#define PTXQSAV_GET             ((OTG_FS_HPTXSTS>>PTXQSAV_SFT)&0xf)
// Periodic transmit data FIFO space available
#define PTXQSAVL_MSK            0x0000ffff
#define PTXQSAVL_FULL           0x00000000
#define PTXQSAVL_GET            (OTG_FS_HPTXSTS&PTXQSAVL_MSK)

/* OTG_HS Host all channels interrupt register */
#define OTG_HS_HAINT        MMIO32(USB_OTG_HS_BASE + 0x414)
// Channel interrupts
#define HAINT0   0x0001
#define HAINT1   0x0002
#define HAINT2   0x0004
#define HAINT3   0x0008
#define HAINT4   0x0010
#define HAINT5   0x0020
#define HAINT6   0x0040
#define HAINT7   0x0080
#define HAINT8   0x0100
#define HAINT9   0x0200
#define HAINT10  0x0400
#define HAINT11  0x0800
#define HAINT12  0x1000
#define HAINT13  0x2000
#define HAINT14  0x4000
#define HAINT15  0x8000

/* OTG_HS Host all channels interrupt mask register */
#define OTG_HS_HAINTMSK     MMIO32(USB_OTG_HS_BASE + 0x418)
// Channel interrupt mask
#define HAINTM0  0x0001
#define HAINTM1  0x0002
#define HAINTM2  0x0004
#define HAINTM3  0x0008
#define HAINTM4  0x0010
#define HAINTM5  0x0020
#define HAINTM6  0x0040
#define HAINTM7  0x0080
#define HAINTM8  0x0100
#define HAINTM9  0x0200
#define HAINTM10 0x0400
#define HAINTM11 0x0800
#define HAINTM12 0x1000
#define HAINTM13 0x2000
#define HAINTM14 0x4000
#define HAINTM15 0x8000

/* OTG_HS Host port control and status register */
#define OTG_HS_HPRT         MMIO32(USB_OTG_HS_BASE + 0x440)
// Port speed
#define PSPD_FULL           0x20000
#define PSPD_LOW            0x40000
// Port test control
#define PTCTL_DISABLED      0x00000
#define PTCTL_TESTJ         0x02000
#define PTCTL_TESTK         0x04000
#define PTCTL_SE0_NAK       0x06000
#define PTCTL_PACKET_MODE   0x08000
#define PTCTL_FORCE_EN      0x0a000
// Port power
#define PPWR                0x01000
// Port line status
#define PLSTS_DP            0x00800
#define PLSTS_DM            0x00400
// Port reset
#define PRST                0x00100
// Port suspend
#define PSUSP               0x00080
// Port resume
#define PRES                0x00040
// Port overcurrent change
#define POCCHNG             0x00020
// Port overcurrent active
#define POCA                0x00010
// Port enable/disable change
#define PENCHNG             0x00008
// Port enable
#define PENA                0x00004
// Port connect detected
#define PCDET               0x00002
// Port connect status
#define PCSTS               0x00001

/* OTG_HS Host channel-0 characteristics register */
#define OTG_HS_HCCHAR0      MMIO32(USB_OTG_HS_BASE + 0x500)
#define OTG_HS_HCCHAR1      MMIO32(USB_OTG_HS_BASE + 0x520)
#define OTG_HS_HCCHAR2      MMIO32(USB_OTG_HS_BASE + 0x540)
#define OTG_HS_HCCHAR3      MMIO32(USB_OTG_HS_BASE + 0x560)
#define OTG_HS_HCCHAR4      MMIO32(USB_OTG_HS_BASE + 0x580)
#define OTG_HS_HCCHAR5      MMIO32(USB_OTG_HS_BASE + 0x5a0)
#define OTG_HS_HCCHAR6      MMIO32(USB_OTG_HS_BASE + 0x5c0)
#define OTG_HS_HCCHAR7      MMIO32(USB_OTG_HS_BASE + 0x5e0)
#define OTG_HS_HCCHAR8      MMIO32(USB_OTG_HS_BASE + 0x600)
#define OTG_HS_HCCHAR9      MMIO32(USB_OTG_HS_BASE + 0x620)
#define OTG_HS_HCCHAR10     MMIO32(USB_OTG_HS_BASE + 0x640)
#define OTG_HS_HCCHAR11     MMIO32(USB_OTG_HS_BASE + 0x660)
// Channel enable
#define CHENA                   0x80000000
// Channel disable
#define CHDIS                   0x40000000
// Odd frame
#define ODDFRM                  0x20000000
// Device address
#define HDAD_MSK                0x1fc00000
#define HDAD_OFFS               22
// Multicount
#define MCNT1                   0x00100000
#define MCNT2                   0x00200000
#define MCNT3                   0x00300000
// Endpoint type
#define EPTYP_HOST_CONTROL      0x00000000
#define EPTYP_HOST_ISOCHRONOUS  0x00040000
#define EPTYP_HOST_BULK         0x00080000
#define EPTYP_HOST_INTERRUPT    0x000c0000
// Low-speed device
#define LSDEV                   0x00020000
// Endpoint direction
#define EPDIR                   0x00008000
// Endpoint number
#define EPNUM_MSK               0x00007800
#define EPNUM_SFT               11
#define EPNUM_GET(r)            ((r>>EPNUM_SFT)&0xf)
#define EPNUM_SET(x)            ((x<<EPNUM_SFT)&EPNUM_MSK)
// Maximum packet size
#define MPSIZ_MSK               0x000007ff
#define MPSIZ_GET(r)            (r&MPSIZ_MSK)
#define MPSIZ_SET(x)            (x&MPSIZ_MSK)

/* OTG_HS host channel-x split control register */
#define OTG_HS_HCSPLT0      MMIO32(USB_OTG_HS_BASE + 0x504)
#define OTG_HS_HCSPLT1      MMIO32(USB_OTG_HS_BASE + 0x524)
#define OTG_HS_HCSPLT2      MMIO32(USB_OTG_HS_BASE + 0x544)
#define OTG_HS_HCSPLT3      MMIO32(USB_OTG_HS_BASE + 0x564)
#define OTG_HS_HCSPLT4      MMIO32(USB_OTG_HS_BASE + 0x584)
#define OTG_HS_HCSPLT6      MMIO32(USB_OTG_HS_BASE + 0x5a4)
#define OTG_HS_HCSPLT7      MMIO32(USB_OTG_HS_BASE + 0x5c4)
#define OTG_HS_HCSPLT8      MMIO32(USB_OTG_HS_BASE + 0x5e4)
#define OTG_HS_HCSPLT9      MMIO32(USB_OTG_HS_BASE + 0x604)
#define OTG_HS_HCSPLT10     MMIO32(USB_OTG_HS_BASE + 0x624)
#define OTG_HS_HCSPLT11     MMIO32(USB_OTG_HS_BASE + 0x644)
// Split enable
#define SPLITEN         0x80000000
// Do complete split
#define COMPLSPLT       0x00010000
// Transaction position
#define XACTPOS_MSK     0x0000c000
#define XACTPOS_MID     0x00000000
#define XACTPOS_END     0x00004000
#define XACTPOS_BEGIN   0x00008000
#define XACTPOS_ALL     0x0000c000
// Hub address
#define HUBADDR_MSK     0x00003f80
#define HUBADDR_SFT     7
#define HUBADDR_GET(r)  ((r>>HUBADDR_SFT)&HUBADDR_MSK)
#define HUBADDR_SET(x)  ((x<<HUBADDR_SFT)&0x7f)
// Port address
#define PRTADDR_MSK     0x0000007f
#define PRTADDR_GET(r)  (r&PRTADDR_MSK)
#define PRTADDR_SET(x)  (x&PRTADDR_MSK)

/* OTG_HS Host channel-x interrupt register */
#define OTG_HS_HCINT0       MMIO32(USB_OTG_HS_BASE + 0x508)
#define OTG_HS_HCINT1       MMIO32(USB_OTG_HS_BASE + 0x528)
#define OTG_HS_HCINT2       MMIO32(USB_OTG_HS_BASE + 0x548)
#define OTG_HS_HCINT3       MMIO32(USB_OTG_HS_BASE + 0x568)
#define OTG_HS_HCINT4       MMIO32(USB_OTG_HS_BASE + 0x588)
#define OTG_HS_HCINT5       MMIO32(USB_OTG_HS_BASE + 0x5a8)
#define OTG_HS_HCINT6       MMIO32(USB_OTG_HS_BASE + 0x5c8)
#define OTG_HS_HCINT7       MMIO32(USB_OTG_HS_BASE + 0x5e8)
#define OTG_HS_HCINT8       MMIO32(USB_OTG_HS_BASE + 0x608)
#define OTG_HS_HCINT9       MMIO32(USB_OTG_HS_BASE + 0x628)
#define OTG_HS_HCINT10      MMIO32(USB_OTG_HS_BASE + 0x648)
#define OTG_HS_HCINT11      MMIO32(USB_OTG_HS_BASE + 0x668)
// Data toggle error
#define DTERR   0x400
// Frame overrun
#define FRMOR   0x200
// Babble error
#define BBERR   0x100
// Transaction error
#define TXERR   0x080
// Response received interrupt
#define NYET    0x040
// ACK response received/transmitted interrupt
#define ACK     0x020
// NAK response received interrupt
#define NAKH    0x010
// STALL response received interrupt
#define STALLH  0x008
// AHB error
#define AHBERR  0x004
// Channel halted
#define CHH     0x002
// Transfer completed
#define XFRCH   0x001

/* OTG_HS Host channel-x interrupt mask register */
#define OTG_HS_HCINTMSK0    MMIO32(USB_OTG_HS_BASE + 0x50c)
#define OTG_HS_HCINTMSK1    MMIO32(USB_OTG_HS_BASE + 0x52c)
#define OTG_HS_HCINTMSK2    MMIO32(USB_OTG_HS_BASE + 0x54c)
#define OTG_HS_HCINTMSK3    MMIO32(USB_OTG_HS_BASE + 0x56c)
#define OTG_HS_HCINTMSK4    MMIO32(USB_OTG_HS_BASE + 0x58c)
#define OTG_HS_HCINTMSK5    MMIO32(USB_OTG_HS_BASE + 0x5ac)
#define OTG_HS_HCINTMSK6    MMIO32(USB_OTG_HS_BASE + 0x5cc)
#define OTG_HS_HCINTMSK7    MMIO32(USB_OTG_HS_BASE + 0x5ec)
#define OTG_HS_HCINTMSK8    MMIO32(USB_OTG_HS_BASE + 0x60c)
#define OTG_HS_HCINTMSK9    MMIO32(USB_OTG_HS_BASE + 0x62c)
#define OTG_HS_HCINTMSK10   MMIO32(USB_OTG_HS_BASE + 0x64c)
#define OTG_HS_HCINTMSK11   MMIO32(USB_OTG_HS_BASE + 0x66c)
// Data toggle error mask
#define DTERRM  0x400
// Frame overrun mask
#define FRMORM  0x200
// Babble error mask
#define BBERRM  0x100
// Transaction error mask
#define TXERRM  0x080
// response received interrupt mask
#define NYETM   0x040
// ACK response received/transmitted interrupt mask
#define ACKM    0x020
// NAK response received interrupt mask
#define NAKHM   0x010
// STALL response received interrupt mask
#define STALLM  0x008
// AHB error mask
#define AHBERRM 0x004
// Channel halted mask
#define CHHM    0x002
// Transfer completed mask
#define XFRCHM  0x001

/* OTG_HS Host channel-x transfer size register */
#define OTG_HS_HCTSIZ0      MMIO32(USB_OTG_HS_BASE + 0x510)
#define OTG_HS_HCTSIZ1      MMIO32(USB_OTG_HS_BASE + 0x530)
#define OTG_HS_HCTSIZ2      MMIO32(USB_OTG_HS_BASE + 0x550)
#define OTG_HS_HCTSIZ3      MMIO32(USB_OTG_HS_BASE + 0x570)
#define OTG_HS_HCTSIZ4      MMIO32(USB_OTG_HS_BASE + 0x590)
#define OTG_HS_HCTSIZ5      MMIO32(USB_OTG_HS_BASE + 0x5b0)
#define OTG_HS_HCTSIZ6      MMIO32(USB_OTG_HS_BASE + 0x5d0)
#define OTG_HS_HCTSIZ7      MMIO32(USB_OTG_HS_BASE + 0x5f0)
#define OTG_HS_HCTSIZ8      MMIO32(USB_OTG_HS_BASE + 0x610)
#define OTG_HS_HCTSIZ9      MMIO32(USB_OTG_HS_BASE + 0x630)
#define OTG_HS_HCTSIZ10     MMIO32(USB_OTG_HS_BASE + 0x650)
#define OTG_HS_HCTSIZ11     MMIO32(USB_OTG_HS_BASE + 0x670)
// Data PID
#define HDPID_DATA0     0x00000000
#define HDPID_DATA1     0x40000000
#define HDPID_DATA2     0x20000000
#define HDPID_MDATA     0x60000000
// Packet count
#define PKTCNT_MSK      0x1ff80000
#define PKTCNT_SFT      19
#define PKTCNT_GET(r)   ((r>>PKTCNT_SFT)&0x3ff)
#define PKTCNT_SET(x)   ((x<<PKTCNT_SFT)&PKTCNT_MSK)
// Transfer size
#define XFRSIZ_MSK      0x0007ffff
#define XFRSIZ_GET(r)   (r&XFRSIZ_MSK)
#define XFRSIZ_SET(x)   (x&XFRSIZ_MSK)

/* OTG_HS host channel-x DMA address register */
#define OTG_HS_HCDMA0       MMIO32(USB_OTG_HS_BASE + 0x514)
#define OTG_HS_HCDMA1       MMIO32(USB_OTG_HS_BASE + 0x524)
#define OTG_HS_HCDMA2       MMIO32(USB_OTG_HS_BASE + 0x544)
#define OTG_HS_HCDMA3       MMIO32(USB_OTG_HS_BASE + 0x564)
#define OTG_HS_HCDMA4       MMIO32(USB_OTG_HS_BASE + 0x584)
#define OTG_HS_HCDMA5       MMIO32(USB_OTG_HS_BASE + 0x5a4)
#define OTG_HS_HCDMA6       MMIO32(USB_OTG_HS_BASE + 0x5c4)
#define OTG_HS_HCDMA7       MMIO32(USB_OTG_HS_BASE + 0x5e4)
#define OTG_HS_HCDMA8       MMIO32(USB_OTG_HS_BASE + 0x604)
#define OTG_HS_HCDMA9       MMIO32(USB_OTG_HS_BASE + 0x624)
#define OTG_HS_HCDMA10      MMIO32(USB_OTG_HS_BASE + 0x644)
#define OTG_HS_HCDMA11      MMIO32(USB_OTG_HS_BASE + 0x664)
// DMAADDR[31:0]: DMA address

/*********Device-mode registers**********/
/* OTG_HS device configuration register */
#define OTG_HS_DCFG         MMIO32(USB_OTG_HS_BASE + 0x800)
// Periodic scheduling interval
#define PERSCHIVL_MSK       0x03000000
#define PERSCHIVL_25        0x00000000
#define PERSCHIVL_50        0x01000000
#define PERSCHIVL_75        0x02000000
// Periodic frame interval
#define PFIVL_80            0x0000
#define PFIVL_85            0x0800
#define PFIVL_90            0x1000
#define PFIVL_95            0x1800
// Device address
#define DAD_MSK             0x07f0
#define DAD_SFT             4
#define DAD_GET             ((OTG_FS_DCFG>>DAD_SFT)&0x7f)
#define DAD_SFT(x)          ((x<<DAD_SFT)&DAD_MSK)
// Non-zero-length status OUT handshake
#define NZLSOHSK            0x0004
// Device speed
#define DSPD_MSK            0x0003
#define DSPD_HIGH           0x0000
#define DSPD_FULL_EXTPHY    0x0001
#define DSPD_FULL_INTPHY    0x0003

/* OTG_HS device control register */
#define OTG_HS_DCTL         MMIO32(USB_OTG_HS_BASE + 0x804)
// Power-on programming done
#define POPRGDNE            0x800
// Clear global OUT NAK
#define CGONAK              0x400
// Set global OUT NAK
#define SGONAK              0x200
// Clear global IN NAK
#define CGINAK              0x100
// Set global IN NAK
#define SGINAK              0x080
// Test control
#define TCTL_MSK            0x070
#define TCTL_DIS            0x000
#define TCTL_TESTJ          0x010
#define TCTL_TESTK          0x020
#define TCTL_SE0_NAK        0x030
#define TCTL_TEST_PACKET    0x040
#define TCTL_TEST_FORCE_EN  0x050
// Global OUT NAK status
#define GONSTS              0x008
// Global IN NAK status
#define GINSTS              0x004
// Soft disconnect
#define SDIS                0x002
// Remote wakeup signaling
#define RWUSIG              0x001

/* OTG_HS device status register */
#define OTG_HS_DSTS         MMIO32(USB_OTG_HS_BASE + 0x808)
// Frame number of the received SOF
#define FNSOF_MSK       0x007fff00
#define FNSOF_SFT       8
#define FNSOF_GET       ((OTG_FS_DSTS>>FNSOF_SFT)&0x7fff)
#define FNSOF_SET(x)    ((x<<FNSOF_SFT)&FNSOF_MSK)
// Erratic error
#define EERR            0x000008
// Enumerated speed
#define ENUMSPD_FULL    0x000006
// Suspend status
#define SUSPSTS         0x000001

/* OTG_HS device IN endpoint common interrupt mask register */
#define OTG_HS_DIEPMSK      MMIO32(USB_OTG_HS_BASE + 0x810)
// NAK interrupt mask
#define NAKM        0x2000
// FIFO underrun mask
#define TXFURM      0x0100
// IN endpoint NAK effective mask
#define INEPNEM     0x0040
// IN token received with EP mismatch mask
#define INEPNMM     0x0020
// IN token received when TxFIFO empty mask
#define ITTXFEMSK   0x0010
// Timeout condition mask (Non-isochronous endpoints)
#define TOM         0x0008
// AHB error mask
#define AHBERRM     0x0004
// Endpoint disabled interrupt mask
#define EPDM        0x0002
// Transfer completed interrupt mask
#define XFRCM       0x0001

/* OTG_HS device OUT endpoint common interrupt mask register */
#define OTG_HS_DOEPMSK      MMIO32(USB_OTG_HS_BASE + 0x814)
// NYET interrupt mask
#define NYETMSK     0x4000
// NAK interrupt mask
#define NAKMSK      0x2000
// Babble error interrupt mask
#define BERRM       0x1000
// Out packet error mask
#define OPEM        0x0100
// Back-to-back SETUP packets received mask
#define B2BSTUP     0x0040
// Status phase received for control write mask
#define STSPHSRXM   0x0020
// OUT token received when endpoint disabled mask
#define OTEPDM      0x0010
// SETUP phase done mask
#define STUPM       0x0008
// AHB error mask
#define AHBERRM     0x0004
// Endpoint disabled interrupt mask
#define EPDM        0x0002
// Transfer completed interrupt mask
#define XFRCM       0x0001

/* OTG_HS device all endpoints interrupt register */
#define OTG_HS_DAINT        MMIO32(USB_OTG_HS_BASE + 0x818)
// OUT EP interrupt mask bits
#define OEPINT0  0x00010000
#define OEPINT1  0x00020000
#define OEPINT2  0x00040000
#define OEPINT3  0x00080000
#define OEPINT4  0x00100000
#define OEPINT5  0x00200000
#define OEPINT6  0x00400000
#define OEPINT7  0x00800000
#define OEPINT8  0x01000000
#define OEPINT9  0x02000000
#define OEPINT10 0x04000000
#define OEPINT11 0x08000000
// IN EP interrupt mask bits
#define IEPINT0  0x00000001
#define IEPINT1  0x00000002
#define IEPINT2  0x00000004
#define IEPINT3  0x00000008
#define IEPINT4  0x00000010
#define IEPINT5  0x00000020
#define IEPINT6  0x00000040
#define IEPINT7  0x00000080
#define IEPINT8  0x00000100
#define IEPINT9  0x00000200
#define IEPINT10 0x00000400
#define IEPINT11 0x00000800

/* OTG_HS all endpoints interrupt mask register */
#define OTG_HS_DAINTMSK     MMIO32(USB_OTG_HS_BASE + 0x81c)
// OUT EP interrupt mask bits
#define OEPM0  0x00010000
#define OEPM1  0x00020000
#define OEPM2  0x00040000
#define OEPM3  0x00080000
#define OEPM0  0x00100000
#define OEPM1  0x00200000
#define OEPM2  0x00400000
#define OEPM3  0x00800000
#define OEPM0  0x01000000
#define OEPM1  0x02000000
#define OEPM2  0x04000000
#define OEPM3  0x08000000
// IN EP interrupt mask bits
#define IEPM0  0x00000001
#define IEPM1  0x00000002
#define IEPM2  0x00000004
#define IEPM3  0x00000008
#define IEPM0  0x00000010
#define IEPM1  0x00000020
#define IEPM2  0x00000040
#define IEPM3  0x00000080
#define IEPM0  0x00000100
#define IEPM1  0x00000200
#define IEPM2  0x00000400
#define IEPM3  0x00000800

/* OTG_HS device V BUS discharge time register */
#define OTG_HS_DVBUSDIS     MMIO32(USB_OTG_HS_BASE + 0x828)
// Device V BUS discharge time VBUSDT in ms
#define VBUSDT(n)   (uint16_t)(n/46.875)

/* OTG_HS device V BUS pulsing time register */
#define OTG_HS_DVBUSPULSE   MMIO32(USB_OTG_HS_BASE + 0x82c)
// Device V BUS pulsing time in ms
#define DVBUSP(n)   (uint16_t)(n/46.875)

/* OTG_HS Device threshold control register */
#define OTG_HS_DTHRCTL      MMIO32(USB_OTG_HS_BASE + 0x830)
// Arbiter parking enable
#define ARPEN           0x08000000
// Receive threshold length
#define RXTHRLEN_MSK    0x03fe0000
#define RXTHRLEN_SFT    17
#define RXTHRLEN_GET    ((OTG_HS_DTHRCTL>>)&)
#define RXTHRLEN_SET(x) ((x<<)&)
// Receive threshold enable
#define RXTHREN         0x00010000
// Transmit threshold length
#define TXTHRLEN_MSK    0x000007fc
#define TXTHRLEN_SFT    2
#define TXTHRLEN_GET    ((OTG_HS_DTHRCTL>>)&)
#define TXTHRLEN_SET(x) ((x<<)&)
// ISO IN endpoint threshold enable
#define ISOTHREN        0x00000002
// Nonisochronous IN endpoints threshold enable
#define NONISOTHREN     0x00000001

/* OTG_HS device IN endpoint FIFO empty interrupt mask register */
#define OTG_HS_DIEPEMPMSK   MMIO32(USB_OTG_HS_BASE + 0x834)
// IN EP Tx FIFO empty interrupt mask bits
#define INEPTXFEM0  0x0001
#define INEPTXFEM1  0x0002
#define INEPTXFEM2  0x0004
#define INEPTXFEM3  0x0008
#define INEPTXFEM4  0x0010
#define INEPTXFEM5  0x0020
#define INEPTXFEM6  0x0040
#define INEPTXFEM7  0x0080
#define INEPTXFEM8  0x0100
#define INEPTXFEM9  0x0200
#define INEPTXFEM10 0x0400
#define INEPTXFEM11 0x0800

/* OTG_HS device each endpoint interrupt register */
#define OTG_HS_DEACHINT     MMIO32(USB_OTG_HS_BASE + 0x838)
// OUT endpoint 1 interrupt bit
#define OEP1INT 0x20000
// IN endpoint 1interrupt bit
#define IEP1INT 0x00002

/* OTG_HS device each endpoint interrupt register mask */
#define OTG_HS_DEACHINTMSK  MMIO32(USB_OTG_HS_BASE + 0x83c)
// OUT Endpoint 1 interrupt mask bit
#define OEP1INTM 0x20000
// IN Endpoint 1 interrupt mask bit
#define IEP1INTM 0x00002

/* OTG_HS device each in endpoint-1 interrupt register */
#define OTG_HS_DIEPEACHMSK1 MMIO32(USB_OTG_HS_BASE + 0x844)
// NAK interrupt mask
// BNA interrupt mask
#define BIM     0x200
// FIFO underrun mask
// IN endpoint NAK effective mask
// IN token received with EP mismatch mask
// IN token received when TxFIFO empty mask
// Timeout condition mask (nonisochronous endpoints)
// AHB error mask
// Endpoint disabled interrupt mask
// Transfer completed interrupt mask

/* OTG_HS device each OUT endpoint-1 interrupt register */
#define OTG_HS_DOEPEACHMSK1 MMIO32(USB_OTG_HS_BASE + 0x884)
// NYET interrupt mask
// NAK interrupt mask
// Babble error interrupt mask
// BNA interrupt mask
#define BIMSK   0x200
// OUT packet error mask
#define OPEMSK  0x100
// AHB error mask
// Endpoint disabled interrupt mask
// Transfer completed interrupt mask

/* OTG_HS device control IN endpoint 0 control register */
#define OTG_HS_DIEPCTL0     MMIO32(USB_OTG_HS_BASE + 0x900)
#define OTG_HS_DIEPCTL1     MMIO32(USB_OTG_HS_BASE + 0x920)
#define OTG_HS_DIEPCTL2     MMIO32(USB_OTG_HS_BASE + 0x940)
#define OTG_HS_DIEPCTL3     MMIO32(USB_OTG_HS_BASE + 0x960)
#define OTG_HS_DIEPCTL4     MMIO32(USB_OTG_HS_BASE + 0x980)
#define OTG_HS_DIEPCTL5     MMIO32(USB_OTG_HS_BASE + 0x9a0)
// Endpoint enable
#define EPENA               0x80000000
// Endpoint disable
#define EPDIS               0x40000000
// Set odd frame
#define SODDFRM             0x20000000
// TxFIFO number
#define SD0PID              0x10000000
// STALL handshake
#define SEVNFRM             0x10000000
// Set NAK
#define SNAK                0x0800000
// Clear NAK
#define CNAK                0x0400000
// TxFIFO number
#define TXFNUM_MSK          0x03c00000
#define TXFNUM_SFT          22
#define TXFNUM_GET          ((OTG_FS_DIEPCTL0>>TXFNUM_SFT)&0xf)
#define TXFNUM_SET(x)       ((x<<TXFNUM_SFT)&TXFNUM_MSK)
// STALL handshake
#define STALL               0x00200000
// Endpoint type
#define EPTYP_CONTROL       0x00000000
#define EPTYP_ISOCHRONOUS   0x00040000
#define EPTYP_BULK          0x00080000
#define EPTYP_INTERRUPT     0x000c0000
// NAK status
#define NAKSTS              0x00020000
// Even/odd frame
#define EONUM               0x00010000
// Endpoint data PID
#define DPID                0x00010000
// Maximum packet size MPSIZ in bytes
// USB active endpoint
#define USBAEP              0x00008000
// Maximum packet size MPSIZ in bytes
#define MPSIZ_MSK           0x7ff
#define MPSIZ_GET(r)        (r&MPSIZ_MSK)
#define MPSIZ_SET(x)        (x&MPSIZ_MSK)

/* OTG_HS device control OUT endpoint 0 control register */
#define OTG_HS_DOEPCTL0     MMIO32(USB_OTG_HS_BASE + 0xb00)
// Snoop mode
#define SNPM            0x00100000
// Maximum packet size
#define MPSIZ64B        0x00000000
#define MPSIZ32B        0x00000001
#define MPSIZ16B        0x00000002
#define MPSIZ8B         0x00000003

/* OTG_HS device endpoint-x control register */
#define OTG_HS_DOEPCTL1     MMIO32(USB_OTG_HS_BASE + 0xb20)
#define OTG_HS_DOEPCTL2     MMIO32(USB_OTG_HS_BASE + 0xb40)
#define OTG_HS_DOEPCTL3     MMIO32(USB_OTG_HS_BASE + 0xb60)
#define OTG_HS_DOEPCTL4     MMIO32(USB_OTG_HS_BASE + 0xb80)
#define OTG_HS_DOEPCTL5     MMIO32(USB_OTG_HS_BASE + 0xba0)

/* OTG_HS device endpoint-x interrupt register */
#define OTG_HS_DIEPINT0     MMIO32(USB_OTG_HS_BASE + 0x908)
#define OTG_HS_DIEPINT1     MMIO32(USB_OTG_HS_BASE + 0x928)
#define OTG_HS_DIEPINT2     MMIO32(USB_OTG_HS_BASE + 0x948)
#define OTG_HS_DIEPINT3     MMIO32(USB_OTG_HS_BASE + 0x968)
#define OTG_HS_DIEPINT4     MMIO32(USB_OTG_HS_BASE + 0x988)
#define OTG_HS_DIEPINT5     MMIO32(USB_OTG_HS_BASE + 0x9a8)
// NAK input
#define NAK_IN      0x2000
// Packet dropped status
#define PKTDRPSTS   0x0400
// Transmit FIFO empty
#define TXFE        0x0080
// IN endpoint NAK effective
#define INEPNE      0x0040
// IN token received with EP mismatch
#define INEPNM      0x0020
// IN token received when TxFIFO is empty
#define ITTXFE      0x0010
// Timeout condition
#define TOC         0x0008
// AHB error
#define AHBERR      0x0004
// Endpoint disabled interrupt
#define EPDISD_IN   0x0002
// Transfer completed interrupt
#define XFRC_IN     0x0001

/* OTG_HS device endpoint-x interrupt register */
#define OTG_HS_DOEPINT0     MMIO32(USB_OTG_HS_BASE + 0xb08)
#define OTG_HS_DOEPINT1     MMIO32(USB_OTG_HS_BASE + 0xb28)
#define OTG_HS_DOEPINT2     MMIO32(USB_OTG_HS_BASE + 0xb48)
#define OTG_HS_DOEPINT3     MMIO32(USB_OTG_HS_BASE + 0xb68)
#define OTG_HS_DOEPINT4     MMIO32(USB_OTG_HS_BASE + 0xb88)
#define OTG_HS_DOEPINT5     MMIO32(USB_OTG_HS_BASE + 0xba8)
// NYET interrupt
#define NYET        0x4000
// NAK input
#define NAK         0x2000
// Babble error interrupt
#define BERR        0x1000
// OUT packet error
#define OUTPKTERR   0x0100
// Back-to-back SETUP packets received
#define B2BSTUP     0x0040
// OUT token received when endpoint disabled
#define OTEPDIS     0x0010
// SETUP phase done
#define STUP        0x0008
// AHB error
#define AHBERR      0x0004
// Endpoint disabled interrupt
#define EPDISD      0x0002
// Transfer completed interrupt
#define XFRC        0x0001

/* OTG_HS device IN endpoint 0 transfer size register */
#define OTG_FS_DIEPTSIZ0    MMIO32(USB_OTG_FS_BASE + 0x910)
// Packet count
#define PKTCNT_MSK      0x180000
#define PKTCNT_SFT      19
#define PKTCNT_GET      ((OTG_FS_DIEPTSIZ0>>PKTCNT_SFT)&0x3)
#define PKTCNT_SET(x)   ((x<<PKTCNT_SFT)&PKTCNT_MSK)
// Transfer size
#define XFRSIZ_MSK      0x00007f
#define XFRSIZ_GET      (OTG_FS_DIEPTSIZ0&XFRSIZ_MSK)
#define XFRSIZ_SET(x)   (x&XFRSIZ_MSK)

/* OTG_HS device endpoint-x transfer size register */
#define OTG_HS_DIEPTSIZ1    MMIO32(USB_OTG_HS_BASE + 0x930)
#define OTG_HS_DIEPTSIZ2    MMIO32(USB_OTG_HS_BASE + 0x950)
#define OTG_HS_DIEPTSIZ3    MMIO32(USB_OTG_HS_BASE + 0x970)
#define OTG_HS_DIEPTSIZ3    MMIO32(USB_OTG_HS_BASE + 0x990)
#define OTG_HS_DIEPTSIZ3    MMIO32(USB_OTG_HS_BASE + 0x9b0)
// Packet count
#define PKTCNT_IN_MSK       0x1ff80000
#define PKTCNT_IN_SFT       19
#define PKTCNT_IN_GET(r)    ((r>>PKTCNT_IN_SFT)&0x3ff)
#define PKTCNT_IN_SET(x)    ((x<<PKTCNT_IN_SFT)&PKTCNT_IN_MSK)
// Transfer size
#define XFRSIZ_IN_MSK       0x0007ffff
#define XFRSIZ_IN_GET(r)    (r&XFRSIZ_IN_MSK)
#define XFRSIZ_IN_SET(x)    (x&XFRSIZ_IN_MSK)

/* OTG_HS device OUT endpoint 0 transfer size register */
#define OTG_HS_DOEPTSIZ0    MMIO32(USB_OTG_HS_BASE + 0xb10)
// SETUP packet count
#define STUPCNT1        0x20000000
#define STUPCNT2        0x40000000
#define STUPCNT3        0x60000000
// Packet count
#define PKTCNT          0x00080000
// Transfer size
#define XFRSIZ_MSK      0x0000007f
#define XFRSIZ_GET      (OTG_FS_DIEPTSIZ0&XFRSIZ_MSK)
#define XFRSIZ_SET(x)   (x&XFRSIZ_MSK)

/* OTG_HS device OUT endpoint-x transfer size register */
#define OTG_HS_DOEPTSIZ1    MMIO32(USB_OTG_HS_BASE + 0xb30)
#define OTG_HS_DOEPTSIZ2    MMIO32(USB_OTG_HS_BASE + 0xb50)
#define OTG_HS_DOEPTSIZ3    MMIO32(USB_OTG_HS_BASE + 0xb70)
#define OTG_HS_DOEPTSIZ4    MMIO32(USB_OTG_HS_BASE + 0xb70)
#define OTG_HS_DOEPTSIZ5    MMIO32(USB_OTG_HS_BASE + 0xb70)
// Received data PID
#define RXDPID_DATA0     0x00000000
#define RXDPID_DATA2     0x20000000
#define RXDPID_DATA1     0x40000000
#define RXDPID_MDATA     0x60000000
// Packet count
#define PKTCNT_OUT_MSK      0x1ff80000
#define PKTCNT_OUT_SFT      19
#define PKTCNT_OUT_GET(r)   ((r>>PKTCNT_OUT_SFT)&0x3ff)
#define PKTCNT_OUT_SET(x)   ((x<<PKTCNT_OUT_SFT)&PKTCNT_OUT_MSK)
// Transfer size
#define XFRSIZ_OUT_MSK      0x0007ffff
#define XFRSIZ_OUT_GET(r)   (r&XFRSIZ_OUT_MSK)
#define XFRSIZ_OUT_SET(x)   (x&XFRSIZ_OUT_MSK)

/* OTG_HS device IN endpoint transmit FIFO status register */
#define TG_HS_DTXFSTS0      MMIO32(USB_OTG_HS_BASE + 0x918)
#define TG_HS_DTXFSTS1      MMIO32(USB_OTG_HS_BASE + 0x938)
#define TG_HS_DTXFSTS2      MMIO32(USB_OTG_HS_BASE + 0x958)
#define TG_HS_DTXFSTS3      MMIO32(USB_OTG_HS_BASE + 0x978)
// INEPTFSAV[15:0]: IN endpoint TxFIFO space available INEPTFSAV in 32bit words

/* OTG_HS device endpoint-x DMA address register */
#define OTG_HS_DIEPDMA1     MMIO32(USB_OTG_HS_BASE + 0x934)
#define OTG_HS_DIEPDMA2     MMIO32(USB_OTG_HS_BASE + 0x954)
#define OTG_HS_DIEPDMA3     MMIO32(USB_OTG_HS_BASE + 0x974)
#define OTG_HS_DIEPDMA4     MMIO32(USB_OTG_HS_BASE + 0x994)
#define OTG_HS_DIEPDMA5     MMIO32(USB_OTG_HS_BASE + 0x9b4)
/* OTG_HS device endpoint-x DMA address register */
#define OTG_HS_DOEPDMA1     MMIO32(USB_OTG_HS_BASE + 0xb34)
#define OTG_HS_DOEPDMA2     MMIO32(USB_OTG_HS_BASE + 0xb54)
#define OTG_HS_DOEPDMA3     MMIO32(USB_OTG_HS_BASE + 0xb74)
#define OTG_HS_DOEPDMA4     MMIO32(USB_OTG_HS_BASE + 0xb94)
#define OTG_HS_DOEPDMA5     MMIO32(USB_OTG_HS_BASE + 0xbb4)
// DMAADDR[31:0]: DMA address

//#define OTG_HS_DIEPDMAB1    MMIO32(USB_OTG_HS_BASE + 0x93c)
//#define OTG_HS_DIEPDMAB1    MMIO32(USB_OTG_HS_BASE + 0x95c)
//#define OTG_HS_DIEPDMAB1    MMIO32(USB_OTG_HS_BASE + 0x97c)
//#define OTG_HS_DOEPDMAB1    MMIO32(USB_OTG_HS_BASE + 0xb3c)
//#define OTG_HS_DOEPDMAB2    MMIO32(USB_OTG_HS_BASE + 0xb5c)
//#define OTG_HS_DOEPDMAB3    MMIO32(USB_OTG_HS_BASE + 0xb7c)

/**************************************************/
/* OTG_HS power and clock gating control register */
#define OTG_HS_PCGCCTL      MMIO32(USB_OTG_HS_BASE + 0xe00)
// PHY Suspended
#define PHYSUSP  0x10
// Gate HCLK
#define GATEHCLK 0x02
// Stop PHY clock
#define STPPCLK  0x01

#endif
