#ifndef H_RTC_REG
#define H_RTC_REG
/*
 * Part of Belkin STM32 HAL, Real-time clock (RTC) registers of
 * STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* RTC time register */
#define RTC_TR          MMIO32(RTC_BASE + 0x00)
// AM/PM notation
#define PM          0x400000
// Hour tens in BCD format
#define HT_MSK      0x300000
#define HT_SFT      20
#define HT_GET      ((RTC_TR>>HT_SFT)&0x3)
#define HT_SET(x)   ((x<<HT_SFT)&HT_MSK)
// Hour units in BCD format
#define HU_MSK      0x0f0000
#define HU_SFT      16
#define HU_GET      ((RTC_TR>>HU_SFT)&0xf)
#define HU_SET(x)   ((x<<HU_SFT)&HU_MSK)
// Minute tens in BCD format
#define MNT_MSK     0x007000
#define MNT_SFT     12
#define MNT_GET     ((RTC_TR>>MNT_SFT)&0x7)
#define MNT_SET(x)  ((x<<MNT_SFT)&MNT_MSK)
// Minute units in BCD format
#define MNU_MSK     0x000f00
#define MNU_SFT     8
#define MNU_GET     ((RTC_TR>>MNU_SFT)&0xf)
#define MNU_SET(x)  ((x<<MNU_SFT)&MNU_MSK)
// Second tens in BCD format
#define ST_MSK      0x000070
#define ST_SFT      4
#define ST_GET      ((RTC_TR>>ST_SFT)&0x7)
#define ST_SET(x)   ((x<<ST_SFT)&ST_MSK)
// Second units in BCD format
#define SU_MSK      0x00000f
#define SU_GET      (RTC_TR&SU_MSK)
#define SU_SET(x)   (x&SU_MSK)

/* RTC date register */
#define RTC_DR          MMIO32(RTC_BASE + 0x04)
// Year tens in BCD format
#define YT_MSK          0xf00000
#define YT_SFT          20
#define YT_GET          ((RTC_DR>>YT_SFT)&0xf)
#define YT_SET(x)       ((x<<YT_SFT)&YT_MSK)
// Year units in BCD format
#define YU_MSK          0x0f0000
#define YU_SFT          16
#define YU_GET          ((RTC_DR>>YU_SFT)&0xf)
#define YU_SET(x)       ((x<<YU_SFT)&YU_MSK)
// Week day units
#define WDU_MSK         0x00e000
#define WDU_SFT         13
#define WDU_GET         ((RTC_DR>>WDU_SFT)&0x7)
#define WDU_SET(x)      ((x<<WDU_SFT)&WDU_MSK)
#define WDU_MONDAY      0x002000
#define WDU_TUESDAY     0x004000
#define WDU_WENSDAY     0x006000
#define WDU_THURSDAY    0x008000
#define WDU_FRIDAY      0x00a000
#define WDU_SATURDAY    0x00c000
#define WDU_SUNDAY      0x00e000
// Month tens in BCD format
#define MT_MSK          0x001000
// Month units in BCD format
#define MU_MSK          0x000f00
#define MU_SFT          8
#define MU_GET          ((RTC_DR>>MU_SFT)&0xf)
#define MU_SET(x)       ((x<<MU_SFT)&MU_MSK)
// Date tens in BCD format
#define DT_MSK          0x000030
#define DT_SFT          4
#define DT_GET          ((RTC_DR>>DT_SFT)&0x3)
#define DT_SET(x)       ((x<<DT_SFT)&DT_MSK)
// Date units in BCD format
#define DU_MSK          0x00000f
#define DU_GET          (RTC_DR&DU_MSK)
#define DU_SET(x)       (x&DU_MSK)

/* RTC control register */
#define RTC_CR          MMIO32(RTC_BASE + 0x08)
// Calibration output enable
#define COE                     0x800000
// Output selection
#define OSEL_DIS                0x000000
#define OSEL_ALARMA             0x200000
#define OSEL_ALARMB             0x400000
#define OSEL_WKUP               0x600000
// Output polarity
#define POL                     0x100000
// Calibration output selection
#define COSEL                   0x080000
// Backup
#define BKP                     0x040000
// Subtract 1 hour (winter time change)
#define SUB1H                   0x020000
// Add 1 hour (summer time change)
#define ADD1H                   0x010000
// Timestamp interrupt enable
#define TSIE                    0x008000
// Wake-up timer interrupt enable
#define WUTIE                   0x004000
// Alarm B interrupt enable
#define ALRBIE                  0x002000
// Alarm A interrupt enable
#define ALRAIE                  0x001000
// Time stamp enable
#define TSE                     0x000800
// Wake-up timer enable
#define WUTE                    0x000400
// Alarm B enable
#define ALRBE                   0x000200
// Alarm A enable
#define ALRAE                   0x000100
// Coarse digital calibration enable
#define DCE                     0x000080
// Hour format
#define FMT                     0x000040
// Bypass the shadow registers
#define BYPSHAD                 0x000020
// Reference clock detection enable (50 or 60 Hz)
#define REFCKON                 0x000010
// Timestamp event active edge
#define TSEDGE                  0x000008
// Wake-up clock selection
#define WUCKSEL_RTC_DIV16       0x000000
#define WUCKSEL_RTC_DIV8        0x000001
#define WUCKSEL_RTC_DIV4        0x000002
#define WUCKSEL_RTC_DIV2        0x000003
#define WUCKSEL_CK_SPRE         0x000004
#define WUCKSEL_CK_SPRE_2P16    0x000006

/* RTC initialization and status register */
#define RTC_ISR         MMIO32(RTC_BASE + 0x0c)
// Recalibration pending Flag
#define RECALPF 0x10000
// TAMPER2 detection flag
#define TAMP2F  0x04000
// Tamper detection flag
#define TAMP1F  0x02000
// Timestamp overflow flag
#define TSOVF   0x01000
// Timestamp flag
#define TSF     0x00800
// Wake-up timer flag
#define WUTF    0x00400
// Alarm B flag
#define ALRBF   0x00200
// Alarm A flag
#define ALRAF   0x00100
// Initialization mode
#define INIT    0x00080
// Initialization flag
#define INITF   0x00040
// Registers synchronization flag
#define RSF     0x00020
// Initialization status flag
#define INITS   0x00010
// Shift operation pending
#define SHPF    0x00008
// Wake-up timer write flag
#define WUTWF   0x00004
// Alarm B write flag
#define ALRBWF  0x00002
// Alarm A write flag
#define ALRAWF  0x00001

/* RTC prescaler register */
#define RTC_PRER        MMIO32(RTC_BASE + 0x10)
// Asynchronous prescaler factor
#define PREDIV_A_MSK    0x7f0000
#define PREDIV_A_SFT    16
#define PREDIV_A_GET    ((RTC_PRER>>PREDIV_A_SFT)&0x7f)
#define PREDIV_A_SFT(x) ((x<<PREDIV_A_SFT)&PREDIV_A_MSK)
// Synchronous prescaler factor
#define PREDIV_S_MSK    0x007fff
#define PREDIV_S_GET    (RTC_PRER&PREDIV_S_MSK)
#define PREDIV_S_SFT(x) (x&PREDIV_S_MSK)

/* RTC wake-up timer register */
#define RTC_WUTR        MMIO32(RTC_BASE + 0x14)
// WUT[15:0]: Wake-up auto-reload value bits

/* RTC calibration register */
#define RTC_CALIBR      MMIO32(RTC_BASE + 0x18)
// Digital calibration sign
#define DCS     0x80
// Digital calibration
#define DC0     0x00
#define DC4     0x01
#define DC8     0x02
#define DC12    0x03
#define DC16    0x04
#define DC20    0x05
#define DC24    0x06
#define DC28    0x07
#define DC32    0x08
#define DC36    0x09
#define DC40    0x0a
#define DC44    0x0b
#define DC48    0x0c
#define DC52    0x0d
#define DC56    0x0e
#define DC60    0x0f
#define DC64    0x10
#define DC68    0x11
#define DC72    0x12
#define DC76    0x13
#define DC80    0x14
#define DC84    0x15
#define DC88    0x16
#define DC92    0x17
#define DC96    0x18
#define DC100   0x19
#define DC104   0x1a
#define DC108   0x1b
#define DC112   0x1c
#define DC116   0x1d
#define DC120   0x1e
#define DC126   0x1f
#define DCM0    0x00
#define DCM2    0x01
#define DCM4    0x02
#define DCM6    0x03
#define DCM8    0x04
#define DCM10   0x05
#define DCM12   0x06
#define DCM14   0x07
#define DCM16   0x08
#define DCM18   0x09
#define DCM20   0x0a
#define DCM22   0x0b
#define DCM24   0x0c
#define DCM26   0x0d
#define DCM28   0x0e
#define DCM30   0x0f
#define DCM32   0x10
#define DCM34   0x11
#define DCM36   0x12
#define DCM38   0x13
#define DCM40   0x14
#define DCM42   0x15
#define DCM44   0x16
#define DCM46   0x17
#define DCM48   0x18
#define DCM50   0x19
#define DCM52   0x1a
#define DCM54   0x1b
#define DCM56   0x1c
#define DCM58   0x1d
#define DCM60   0x1e
#define DCM63   0x1f

/* RTC alarm A register */
#define RTC_ALRMAR      MMIO32(RTC_BASE + 0x1c)
//
#define
//
#define
//
#define

/* RTC alarm B register */
#define RTC_ALRMBR      MMIO32(RTC_BASE + 0x20)
//
#define

/* RTC write protection register */
#define RTC_WPR         MMIO32(RTC_BASE + 0x24)
// Write protection key
#define KEY1    0xca
#define KEY2    0x53

/* RTC sub second register */
#define RTC_SSR         MMIO32(RTC_BASE + 0x28)
// SS[15:0] Sub second value

/* RTC shift control register */
#define RTC_SHIFTR      MMIO32(RTC_BASE + 0x2c)
// Add one second
#define ADD1S           0x80000000
// Subtract a fraction of a second
#define SUBFS_MSK       0x00007fff
#define SUBFS_SET(x)    (x&SUBFS_MSK)

/* RTC time stamp time register */
#define RTC_TSTR        MMIO32(RTC_BASE + 0x30)
//
#define
//
#define
//
#define
//
#define

/* RTC time stamp date register */
#define RTC_TSDR        MMIO32(RTC_BASE + 0x34)
//
#define
//
#define
//
#define
//
#define

/* RTC timestamp sub second register */
#define RTC_TSSSR       MMIO32(RTC_BASE + 0x38)
// SS[15:0] Sub second value

/* RTC calibration register */
#define RTC_CALR        MMIO32(RTC_BASE + 0x3c)
// Increase frequency of RTC by 488.5 ppm
#define CALP
// Use an 8-second calibration cycle period
#define CALW8
// Use a 16-second calibration cycle period
#define CALW16
// Calibration minus
#define CALM_MSK

/* RTC tamper and alternate function configuration register */
#define RTC_TAFCR       MMIO32(RTC_BASE + 0x40)
// RTC_ALARM output type
#define ALARMOUTTYPE        0x40000
// TIMESTAMP mapping
#define TSINSEL             0x20000
// TAMPER1 mapping
#define TAMP1INSEL          0x10000
// TAMPER pull-up disable
#define TAMPPUDIS           0x08000
// Tamper precharge duration
#define TAMPPRCH1CYCLE      0x00000
#define TAMPPRCH2CYCLE      0x02000
#define TAMPPRCH4CYCLE      0x04000
#define TAMPPRCH8CYCLE      0x06000
// Tamper filter count
#define TAMPFLT_EDGE        0x00000
#define TAMPFLT_2SAMPL      0x00800
#define TAMPFLT_4SAMPL      0x01000
#define TAMPFLT_8SAMPL      0x01800
// Tamper sampling frequency
#define TAMPFREQ_DIV32768   0x00000
#define TAMPFREQ_DIV16384   0x00100
#define TAMPFREQ_DIV8192    0x00200
#define TAMPFREQ_DIV4096    0x00300
#define TAMPFREQ_DIV2048    0x00400
#define TAMPFREQ_DIV1024    0x00500
#define TAMPFREQ_DIV512     0x00600
#define TAMPFREQ_DIV256     0x00700
// Activate timestamp on tamper detection event
#define TAMPTS              0x00080
// Active level for tamper 2
#define TAMP2TRG            0x00010
// Tamper 2 detection enable
#define TAMP2E              0x00008
// Tamper interrupt enable
#define TAMPIE              0x00004
// Active level for tamper 1
#define TAMP1TRG            0x00002
// Tamper 1 detection enable
#define TAMP1E              0x00001

/* RTC alarm A sub second register */
#define RTC_ALRMASSR    MMIO32(RTC_BASE + 0x44)
/* RTC alarm B sub second register */
#define RTC_ALRMBSSR    MMIO32(RTC_BASE + 0x48)
// Mask the most-significant bits starting at this bit
#define MASKSS0     0x01000000
#define MASKSS1_0   0x02000000
#define MASKSS2_0   0x03000000
#define MASKSS3_0   0x04000000
#define MASKSS4_0   0x05000000
#define MASKSS5_0   0x06000000
#define MASKSS6_0   0x07000000
#define MASKSS7_0   0x08000000
#define MASKSS8_0   0x09000000
#define MASKSS9_0   0x0a000000
#define MASKSS10_0  0x0b000000
#define MASKSS11_0  0x0c000000
#define MASKSS12_0  0x0d000000
#define MASKSS13_0  0x0e000000
#define MASKSS_ALL  0x0f000000
// Sub seconds value
#define SS_MSK      0x00007fff
#define SS_GET      (RTC_ALRMASSR&SS_MSK)
#define SS_SET(x)   (x&SS_MSK)

/* RTC backup registers */
#define RTC_BKP0R       MMIO32(RTC_BASE + 0x50)
#define RTC_BKP1R       MMIO32(RTC_BASE + 0x54)
#define RTC_BKP2R       MMIO32(RTC_BASE + 0x58)
#define RTC_BKP3R       MMIO32(RTC_BASE + 0x5c)
#define RTC_BKP4R       MMIO32(RTC_BASE + 0x60)
#define RTC_BKP5R       MMIO32(RTC_BASE + 0x64)
#define RTC_BKP6R       MMIO32(RTC_BASE + 0x68)
#define RTC_BKP7R       MMIO32(RTC_BASE + 0x6c)
#define RTC_BKP8R       MMIO32(RTC_BASE + 0x70)
#define RTC_BKP9R       MMIO32(RTC_BASE + 0x74)
#define RTC_BKP10R      MMIO32(RTC_BASE + 0x78)
#define RTC_BKP11R      MMIO32(RTC_BASE + 0x7c)
#define RTC_BKP12R      MMIO32(RTC_BASE + 0x80)
#define RTC_BKP13R      MMIO32(RTC_BASE + 0x84)
#define RTC_BKP14R      MMIO32(RTC_BASE + 0x88)
#define RTC_BKP15R      MMIO32(RTC_BASE + 0x8c)
#define RTC_BKP16R      MMIO32(RTC_BASE + 0x90)
#define RTC_BKP17R      MMIO32(RTC_BASE + 0x94)
#define RTC_BKP18R      MMIO32(RTC_BASE + 0x98)
#define RTC_BKP16R      MMIO32(RTC_BASE + 0x9c)
#define RTC_BKPnR(n)    MMIO32(RTC_BASE + 0x50 + (n*4))
// BKP[31:0] The application can write or read data to and from these registers.

#endif
