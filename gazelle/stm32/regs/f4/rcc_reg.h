#ifndef H_RCC_REG
#define H_RCC_REG
/*
 * Part of Belkin STM32 HAL, rcc registers of STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Clock control register */
#define RCC_CR          MMIO32(RCC_BASE + 0x00)
// PLLI2S clock ready flag
#define PLLI2SRDY   0x08000000
// PLLI2S enable
#define PLLI2SON    0x04000000
// PLL clock ready flag
#define PLLRDY      0x02000000
// PLL enable
#define PLLON       0x01000000
// Clock security system enable
#define CSSON       0x00080000
// External high-speed clock bypass
#define HSEBYP      0x00040000
// External high-speed clock ready flag
#define HSERDY      0x00020000
// HSE clock enable
#define HSEON       0x00010000
// Internal high-speed clock calibration
#define HSICAL_MSK  0x0000ff00
#define HSICAL_SFT  8
#define HSICAL_GET  ((RCC_CR>>HSICAL_SFT)&0xff)
// Internal high-speed clock trimming
#define HSITRIM_MSK 0x000000f8
#define HSITRIM_SFT 3
#define HSITRIM_GET ((RCC_CR>>HSITRIM_SFT)&0x1f)
#define HSITRIM_SET(x) ((x<<HSITRIM_SFT)&HSITRIM_MSK)
// Internal high-speed clock ready flag
#define HSIRDY  0x00000002
// Internal high-speed clock enable
#define HSION   0x00000001

/* RCC PLL configuration register */
#define RCC_PLLCFGR     MMIO32(RCC_BASE + 0x04)
// Main PLL (PLL) division factor for USB OTG FS, SDIO and RNG clocks
#define PLLQ2       0x02000000
#define PLLQ3       0x03000000
#define PLLQ4       0x04000000
#define PLLQ5       0x05000000
#define PLLQ6       0x06000000
#define PLLQ7       0x07000000
#define PLLQ8       0x08000000
#define PLLQ9       0x09000000
#define PLLQ10      0x0a000000
#define PLLQ11      0x0b000000
#define PLLQ12      0x0c000000
#define PLLQ13      0x0d000000
#define PLLQ14      0x0e000000
#define PLLQ15      0x0f000000
// Main PLL(PLL) and audio PLL (PLLI2S) entry clock source
#define PLLSRC      0x00400000
// Main PLL (PLL) division factor for main system clock
#define PLLP2       0x00000000
#define PLLP4       0x00010000
#define PLLP6       0x00020000
#define PLLP8       0x00030000
// Main PLL (PLL) multiplication factor for VCO
#define PLLN_MSK    0x00007fc0
#define PLLN_SFT    6
#define PLLN_GET    ((RCC_PLLCFGR>>PLLN_SFT)&0x1ff)
#define PLLN_SET(x) ((x<<PLLN_SFT)&PLLN_MSK)
// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock
#define PLLM_MSK    0x0000003f
#define PLLM_GET    (RCC_PLLCFGR&PLLM_MSK)
#define PLLM_SET(x) (x&PLLM_MSK)

/* RCC clock configuration register */
#define RCC_CFGR        MMIO32(RCC_BASE + 0x08)
// Microcontroller clock output 2
#define MCO2_SYSCLK     0x00000000
#define MCO2_PLLI2S     0x40000000
#define MCO2_HSE        0x80000000
#define MCO2_PLL        0xc0000000
// MCO2 prescaler
#define MCO2PRE_DIV2    0x20000000
#define MCO2PRE_DIV3    0x28000000
#define MCO2PRE_DIV4    0x30000000
#define MCO2PRE_DIV5    0x38000000
// MCO2 prescaler
#define MCO1PRE_DIV2    0x04000000
#define MCO1PRE_DIV3    0x05000000
#define MCO1PRE_DIV4    0x06000000
#define MCO1PRE_DIV5    0x07000000
// I2S clock selection
#define I2SSRC          0x00800000
// Microcontroller clock output 1
#define MCO1_HSI        0x00000000
#define MCO1_LSE        0x00200000
#define MCO1_HSE        0x00400000
#define MCO1_PLL        0x00600000
// HSE division factor for RTC clock
#define RTCPRE_MSK      0x001f0000
#define RTCPRE_SFT      16
#define RTCPRE_SET(d)   ((d<<RTCPRE_SFT)&RTCPRE_MSK)
// APB high-speed prescaler (APB2)
#define PPRE2_DIV2      0x00008000
#define PPRE2_DIV4      0x0000a000
#define PPRE2_DIV8      0x0000c000
#define PPRE2_DIV16     0x0000e000
// APB Low speed prescaler (APB1)
#define PPRE1_DIV2      0x00001000
#define PPRE1_DIV4      0x00001400
#define PPRE1_DIV8      0x00001800
#define PPRE1_DIV16     0x00001c00
// AHB prescaler
#define HPRE_DIV2       0x00000080
#define HPRE_DIV4       0x00000090
#define HPRE_DIV8       0x000000a0
#define HPRE_DIV16      0x000000b0
#define HPRE_DIV64      0x000000c0
#define HPRE_DIV128     0x000000d0
#define HPRE_DIV256     0x000000e0
#define HPRE_DIV512     0x000000f0
// System clock switch status
#define SWS_HSI         0x00000000
#define SWS_HSE         0x00000004
#define SWS_PLL         0x00000008
#define SWS_MSK         0x0000000c
// System clock switch
#define SW_HSI          0x00000000
#define SW_HSE          0x00000001
#define SW_PLL          0x00000002
#define SW_MSK          0x00000003

/* RCC clock interrupt register */
#define RCC_CIR         MMIO32(RCC_BASE + 0x0c)
// Clock security system interrupt clear
#define CSSC        0x00800000
// PLL ready interrupt clear
#define PLLRDYC     0x00100000
// HSE ready interrupt clear
#define HSERDYC     0x00080000
// HSI ready interrupt clear
#define HSIRDYC     0x00040000
// LSE ready interrupt clear
#define LSERDYC     0x00020000
// LSI ready interrupt clear
#define LSIRDYC     0x00010000
// PLLI2S ready interrupt enable
#define PLLI2SRDYIE 0x00002000
// PLL ready interrupt enable
#define PLLRDYIE    0x00001000
// HSE ready interrupt enable
#define HSERDYIE    0x00000800
// HSI ready interrupt enable
#define HSIRDYIE    0x00000400
// LSE ready interrupt enable
#define LSERDYIE    0x00000200
// LSI ready interrupt enable
#define LSIRDYIE    0x00000100
// Clock security system interrupt flag
#define CSSF        0x00000080
// PLLI2S ready interrupt flag
#define PLLI2SRDYF  0x00000010
// PLL ready interrupt flag
#define PLLRDYF     0x00000010
// HSE ready interrupt flag
#define HSERDYF     0x00000008
// HSI ready interrupt flag
#define HSIRDYF     0x00000004
// LSE ready interrupt flag
#define LSERDYF     0x00000002
// LSI ready interrupt flag
#define LSIRDYF     0x00000001

/* RCC AHB1 peripheral reset register */
#define RCC_AHB1RSTR    MMIO32(RCC_BASE + 0x10)
// module reset
#define OTGHSRST    0x20000000
#define ETHMACRST   0x02000000
#define DMA2RST     0x00400000
#define DMA1RST     0x00200000
#define CRCRST      0x00001000
#define GPIOIRST    0x00000100
#define GPIOHRST    0x00000080
#define GPIOGRST    0x00000040
#define GPIOFRST    0x00000020
#define GPIOERST    0x00000010
#define GPIODRST    0x00000008
#define GPIOCRST    0x00000004
#define GPIOBRST    0x00000002
#define GPIOARST    0x00000001

/* RCC AHB2 peripheral reset register */
#define RCC_AHB2RSTR    MMIO32(RCC_BASE + 0x14)
#define OTGFSRST    0x80
#define RNGRST      0x40
#define HASHRST     0x20
#define CRYPRST     0x10
#define DCMIRST     0x01

/* RCC AHB3 peripheral reset register */
#define RCC_AHB3RSTR    MMIO32(RCC_BASE + 0x18)
#define FSMCRST 0x1

/* RCC APB1 peripheral reset register */
#define RCC_APB1RSTR    MMIO32(RCC_BASE + 0x20)
#define DACRST      0x20000000
#define PWRRST      0x10000000
#define CAN2RST     0x04000000
#define CAN1RST     0x02000000
#define I2C3RST     0x00800000
#define I2C2RST     0x00400000
#define I2C1RST     0x00200000
#define UART5RST    0x00100000
#define UART4RST    0x00080000
#define USART3RST   0x00040000
#define USART2RST   0x00020000
#define SPI3RST     0x00008000
#define SPI2RST     0x00004000
#define WWDGRST     0x00000800
#define TIM14RST    0x00000100
#define TIM13RST    0x00000080
#define TIM12RST    0x00000040
#define TIM7RST     0x00000020
#define TIM6RST     0x00000010
#define TIM5RST     0x00000008
#define TIM4RST     0x00000004
#define TIM3RST     0x00000002
#define TIM2RST     0x00000001

/* RCC APB2 peripheral reset register */
#define RCC_APB2RSTR    MMIO32(RCC_BASE + 0x24)
#define TIM11RST    0x40000
#define TIM10RST    0x20000
#define TIM9RST     0x10000
#define SYSCFGRST   0x04000
#define SPI1RST     0x01000
#define SDIORST     0x00800
#define ADCRST      0x00100
#define USART6RST   0x00020
#define USART1RST   0x00010
#define TIM8RST     0x00002
#define TIM1RST     0x00001

/* RCC AHB1 peripheral clock enable register */
#define RCC_AHB1ENR     MMIO32(RCC_BASE + 0x30)
#define OTGHSULPIEN     0x20000000
#define OTGHSEN         0x20000000
#define ETHMACPTPEN     0x10000000
#define ETHMACRXEN      0x08000000
#define ETHMACTXEN      0x04000000
#define ETHMACEN        0x02000000
#define DMA2EN          0x00400000
#define DMA1EN          0x00200000
#define CCMDATARAMEN    0x00100000
#define BKPSRAMEN       0x00040000
#define CRCEN           0x00001000
#define GPIOIEN         0x00000100
#define GPIOHEN         0x00000080
#define GPIOGEN         0x00000040
#define GPIOFEN         0x00000020
#define GPIOEEN         0x00000010
#define GPIODEN         0x00000008
#define GPIOCEN         0x00000004
#define GPIOBEN         0x00000002
#define GPIOAEN         0x00000001

/* RCC AHB2 peripheral clock enable register */
#define RCC_AHB2ENR     MMIO32(RCC_BASE + 0x34)
#define OTGFSEN     0x80
#define RNGEN       0x40
#define HASHEN      0x20
#define CRYPEN      0x10
#define DCMIEN      0x01

/* RCC AHB3 peripheral clock enable register */
#define RCC_AHB3ENR     MMIO32(RCC_BASE + 0x38)
#define FSMCEN  0x1

/* RCC APB1 peripheral clock enable register */
#define RCC_APB1ENR     MMIO32(RCC_BASE + 0x40)
#define DACEN       0x20000000
#define PWREN       0x10000000
#define CAN2EN      0x04000000
#define CAN1EN      0x02000000
#define I2C3EN      0x00800000
#define I2C2EN      0x00400000
#define I2C1EN      0x00200000
#define UART5EN     0x00100000
#define UART4EN     0x00080000
#define USART3EN    0x00040000
#define USART2EN    0x00020000
#define SPI3EN      0x00008000
#define SPI2EN      0x00004000
#define WWDGEN      0x00000800
#define TIM14EN     0x00000100
#define TIM13EN     0x00000080
#define TIM12EN     0x00000040
#define TIM7EN      0x00000020
#define TIM6EN      0x00000010
#define TIM5EN      0x00000008
#define TIM4EN      0x00000004
#define TIM3EN      0x00000002
#define TIM2EN      0x00000001

/* RCC APB2 peripheral clock enable register */
#define RCC_APB2ENR     MMIO32(RCC_BASE + 0x44)
#define TIM11EN     0x40000
#define TIM10EN     0x20000
#define TIM9EN      0x10000
#define SYSCFGEN    0x04000
#define SPI1EN      0x01000
#define SDIOEN      0x00800
#define ADC3EN      0x00400
#define ADC2EN      0x00200
#define ADC1EN      0x00100
#define USART6EN    0x00020
#define USART1EN    0x00010
#define TIM8EN      0x00002
#define TIM1EN      0x00001

/* RCC AHB1 peripheral clock enable in low power mode register */
#define RCC_AHB1LPENR   MMIO32(RCC_BASE + 0x50)
#define OTGHSULPILPEN   0x20000000
#define OTGHSLPEN       0x20000000
#define ETHMACPTPLPEN   0x10000000
#define ETHMACRXLPEN    0x08000000
#define ETHMACTXLPEN    0x04000000
#define ETHMACLPEN      0x02000000
#define DMA2LPEN        0x00400000
#define DMA1LPEN        0x00200000
#define BKPSRAMLPEN     0x00040000
#define SRAM1LPEN       0x00020000
#define SRAM2LPEN       0x00010000
#define FLITFLPEN       0x00008000
#define CRCLPEN         0x00001000
#define GPIOILPEN       0x00000100
#define GPIOHLPEN       0x00000080
#define GPIOGLPEN       0x00000040
#define GPIOFLPEN       0x00000020
#define GPIOELPEN       0x00000010
#define GPIODLPEN       0x00000008
#define GPIOCLPEN       0x00000004
#define GPIOBLPEN       0x00000002
#define GPIOALPEN       0x00000001

/* RCC AHB2 peripheral clock enable in low power mode register */
#define RCC_AHB2LPENR   MMIO32(RCC_BASE + 0x54)
#define OTGFSLPEN   0x80
#define RNGLPEN     0x40
#define HASHLPEN    0x20
#define CRYPLPEN    0x10
#define DCMILPEN    0x01
#define

/* RCC AHB3 peripheral clock enable in low power mode register */
#define RCC_AHB3LPENR   MMIO32(RCC_BASE + 0x58)
#define FSMCLPEN    0x1

/* RCC APB1 peripheral clock enable in low power mode register */
#define RCC_APB1LPENR   MMIO32(RCC_BASE + 0x60)
#define DACLPEN     0x20000000
#define PWRLPEN     0x10000000
#define CAN2LPEN    0x04000000
#define CAN1LPEN    0x02000000
#define I2C3LPEN    0x00800000
#define I2C2LPEN    0x00400000
#define I2C1LPEN    0x00200000
#define UART5LPEN   0x00100000
#define UART4LPEN   0x00080000
#define USART3LPEN  0x00040000
#define USART2LPEN  0x00020000
#define SPI3LPEN    0x00008000
#define SPI2LPEN    0x00004000
#define WWDGLPEN    0x00000800
#define TIM14LPEN   0x00000100
#define TIM13LPEN   0x00000080
#define TIM12LPEN   0x00000040
#define TIM7LPEN    0x00000020
#define TIM6LPEN    0x00000010
#define TIM5LPEN    0x00000008
#define TIM4LPEN    0x00000004
#define TIM3LPEN    0x00000002
#define TIM2LPEN    0x00000001

/* RCC APB2 peripheral clock enabled in low power mode register */
#define RCC_APB2LPENR   MMIO32(RCC_BASE + 0x64)
#define TIM11LPEN   0x40000
#define TIM10LPEN   0x20000
#define TIM9LPEN    0x10000
#define SYSCFGLPEN  0x04000
#define SPI1LPEN    0x01000
#define SDIOLPEN    0x00800
#define ADC3LPEN    0x00400
#define ADC2LPEN    0x00200
#define ADC1LPEN    0x00100
#define USART6LPEN  0x00020
#define USART1LPEN  0x00010
#define TIM8LPEN    0x00002
#define TIM1LPEN    0x00001

/* RCC Backup domain control register */
#define RCC_BDCR        MMIO32(RCC_BASE + 0x70)
// Backup domain software reset
#define BDRST           0x00010000
// RTC clock enable
#define RTCEN           0x00008000
// RTC clock source selection
#define RTCSEL_NOCLK    0x00000000
#define RTCSEL_LSE      0x00000100
#define RTCSEL_LSI      0x00000200
#define RTCSEL_HSE      0x00000300
// External low-speed oscillator bypass
#define LSEBYP          0x00000004
// External low-speed oscillator ready
#define LSERDY          0x00000002
// External low-speed oscillator enable
#define LSEON           0x00000001

/* RCC clock control & status register */
#define RCC_CSR         MMIO32(RCC_BASE + 0x74)
// Low-power reset flag
#define LPWRRSTF    0x80000000
// Window watchdog reset flag
#define WWDGRSTF    0x40000000
// Independent watchdog reset flag
#define IWDGRSTF    0x20000000
// Software reset flag
#define SFTRSTF     0x10000000
// POR/PDR reset flag
#define PORRSTF     0x08000000
// PIN reset flag
#define PINRSTF     0x04000000
// BOR reset flag
#define BORRSTF     0x02000000
// Remove reset flag
#define RMVF        0x01000000
// Internal low-speed oscillator ready
#define LSIRDY      0x00000002
// Internal low-speed oscillator enable
#define LSION       0x00000001

/* RCC spread spectrum clock generation register */
#define RCC_SSCGR       MMIO32(RCC_BASE + 0x80)
// Spread spectrum modulation enable
#define SSCGEN          0x80000000
// Spread Select
#define SPREADSEL       0x40000000
// Incrementation step
#define INCSTEP_MSK     0x0fffe000
#define INCSTEP_SFT     13
#define INCSTEP_GET     ((RCC_SSCGR>>INCSTEP_SFT)&0x7fff)
#define INCSTEP_SET(x)  ((x<<INCSTEP_SFT)&INCSTEP_MSK)
// Modulation period
#define MODPER_MSK      0x00001fff
#define MODPER_GET      (RCC_SSCGR&MODPER_MSK)
#define MODPER_SET(x)   (x&MODPER_MSK)

/* RCC PLLI2S configuration register */
#define RCC_PLLI2SCFGR  MMIO32(RCC_BASE + 0x84)
// PLLI2S division factor for I2S clocks
#define PLLI2SR_DIV2    0x20000000
#define PLLI2SR_DIV3    0x30000000
#define PLLI2SR_DIV4    0x40000000
#define PLLI2SR_DIV5    0x50000000
#define PLLI2SR_DIV6    0x60000000
#define PLLI2SR_DIV7    0x70000000
// PLLI2S multiplication factor for VCO
#define PLLI2SN_MSK     0x00007fc0
#define PLLI2SN_SFT     6
#define PLLI2SN_GET     ((RCC_PLLI2SCFGR>>PLLI2SN_SFT)&0x1ff)
#define PLLI2SN_SET(x)  ((x<<PLLI2SN_SFT)&PLLI2SN_MSK)

#endif
