#ifndef H_ADC_REG
#define H_ADC_REG
/*
 * Part of Belkin STM32 HAL, Analog-to-digital converter register
 * definitions of STM32F4xx MCUs.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* ADC status register */
#define ADC1_SR     MMIO32(ADC1_BASE + 0x00)
#define ADC2_SR     MMIO32(ADC2_BASE + 0x00)
#define ADC3_SR     MMIO32(ADC3_BASE + 0x00)
// Overrun
#define OVR     0x20
// Regular channel Start flag
#define STRT    0x10
// Injected channel Start flag
#define JSTRT   0x08
// Injected channel end of conversion
#define JEOC    0x04
// End of conversion
#define EOC     0x02
// Analog watchdog flag
#define AWD     0x01

/* ADC control register 1 */
#define ADC1_CR1    MMIO32(ADC1_BASE + 0x04)
#define ADC2_CR1    MMIO32(ADC2_BASE + 0x04)
#define ADC3_CR1    MMIO32(ADC3_BASE + 0x04)
// Overrun interrupt enable
#define OVRIE                       0x04000000
// Resolution
#define RES12BIT                    0x00000000
#define RES10BIT                    0x01000000
#define RES8BIT                     0x02000000
#define RES6BIT                     0x03000000
// Analog watchdog enable on regular channels
#define AWDEN                       0x00800000
// Analog watchdog enable on injected channels
#define JAWDEN                      0x00400000
// Discontinuous mode channel count
#define DISCNUM_1CHANNEL          0x000000
#define DISCNUM_2CHANNEL          0x002000
#define DISCNUM_3CHANNEL          0x004000
#define DISCNUM_4CHANNEL          0x006000
#define DISCNUM_5CHANNEL          0x008000
#define DISCNUM_6CHANNEL          0x00a000
#define DISCNUM_7CHANNEL          0x00c000
#define DISCNUM_8CHANNEL          0x00e000
// Discontinuous mode on injected channels
#define JDISCEN                   0x001000
// Discontinuous mode on regular channels
#define DISCEN                    0x000800
// Automatic Injected Group conversion
#define JAUTO                     0x000400
// Enable the watchdog on a single channel in scan mode
#define AWDSGL                    0x000200
// Scan mode
#define SCAN                      0x000100
// Interrupt enable for injected channels
#define JEOCIE                    0x000080
// Analog watchdog interrupt enable
#define AWDIE                     0x000040
// Interrupt enable for EOC
#define EOCIE                     0x000020
// Analog watchdog channel select bits
#define AWDCH_CHANNEL0            0x000000
#define AWDCH_CHANNEL1            0x000001
#define AWDCH_CHANNEL2            0x000002
#define AWDCH_CHANNEL3            0x000003
#define AWDCH_CHANNEL4            0x000004
#define AWDCH_CHANNEL5            0x000005
#define AWDCH_CHANNEL6            0x000006
#define AWDCH_CHANNEL7            0x000007
#define AWDCH_CHANNEL8            0x000008
#define AWDCH_CHANNEL9            0x000009
#define AWDCH_CHANNEL10           0x00000a
#define AWDCH_CHANNEL11           0x00000b
#define AWDCH_CHANNEL12           0x00000c
#define AWDCH_CHANNEL13           0x00000d
#define AWDCH_CHANNEL14           0x00000e
#define AWDCH_CHANNEL15           0x00000f
#define AWDCH_CHANNEL16           0x000010
#define AWDCH_CHANNEL17           0x000011
#define AWDCH_CHANNEL17           0x000012

/* ADC control register 2 */
#define ADC1_CR2    MMIO32(ADC1_BASE + 0x08)
#define ADC2_CR2    MMIO32(ADC2_BASE + 0x08)
#define ADC3_CR2    MMIO32(ADC3_BASE + 0x08)
// Start conversion of regular channels
#define SWSTART             0x40000000
// External trigger enable for regular channels
#define EXTEN_DISABLED      0x00000000
#define EXTEN_RISING        0x10000000
#define EXTEN_FALLING       0x20000000
#define EXTEN_BOTH          0x30000000
// External event select for regular group
#define EXTSEL_TIM1_CC1     0x00000000
#define EXTSEL_TIM1_CC2     0x01000000
#define EXTSEL_TIM1_CC3     0x02000000
#define EXTSEL_TIM2_CC2     0x03000000
#define EXTSEL_TIM2_CC3     0x04000000
#define EXTSEL_TIM2_CC4     0x05000000
#define EXTSEL_TIM2_TRGO    0x06000000
#define EXTSEL_TIM3_CC1     0x07000000
#define EXTSEL_TIM3_TRGO    0x08000000
#define EXTSEL_TIM4_CC4     0x09000000
#define EXTSEL_TIM5_CC1     0x0a000000
#define EXTSEL_TIM5_CC2     0x0b000000
#define EXTSEL_TIM5_CC3     0x0c000000
#define EXTSEL_TIM8_CC1     0x0d000000
#define EXTSEL_TIM8_TRGO    0x0e000000
#define EXTSEL_EXTI11       0x0f000000
// Start conversion of injected channels
#define JSWSTART            0x00400000
// External trigger enable for injected channels
#define JEXTEN_DISABLED     0x00000000
#define JEXTEN_RISING       0x001000000
#define JEXTEN_FALLING      0x00200000
#define JEXTEN_BOTH         0x00300000
// External event select for injected group
#define JEXTSEL_TIM1_CC1    0x00000000
#define JEXTSEL_TIM1_CC2    0x00010000
#define JEXTSEL_TIM1_CC3    0x00020000
#define JEXTSEL_TIM2_CC2    0x00030000
#define JEXTSEL_TIM2_CC3    0x00040000
#define JEXTSEL_TIM2_CC4    0x00050000
#define JEXTSEL_TIM2_TRGO   0x00060000
#define JEXTSEL_TIM3_CC1    0x00070000
#define JEXTSEL_TIM3_TRGO   0x00080000
#define JEXTSEL_TIM4_CC4    0x00090000
#define JEXTSEL_TIM5_CC1    0x000a0000
#define JEXTSEL_TIM5_CC2    0x000b0000
#define JEXTSEL_TIM5_CC3    0x000c0000
#define JEXTSEL_TIM8_CC1    0x000d0000
#define JEXTSEL_TIM8_TRGO   0x000e0000
#define JEXTSEL_EXTI11      0x000f0000
// Data alignment
#define ALIGN               0x00000800
// End of conversion selection
#define EOCS                0x00000400
// DMA disable selection (for single ADC mode)
#define DDS                 0x00000200
// Direct memory access mode (for single ADC mode)
#define DMA                 0x00000100
// Continuous conversion
#define CONT                0x00000002
// A/D Converter ON / OFF
#define ADON                0x00000001

/* ADC sample time register 1 */
#define ADC1_SMPR1  MMIO32(ADC1_BASE + 0x0c)
#define ADC2_SMPR1  MMIO32(ADC2_BASE + 0x0c)
#define ADC3_SMPR1  MMIO32(ADC3_BASE + 0x0c)
// Channel 18 Sample time selection
#define SMP18_3CYCL     0x0000000
#define SMP18_15CYCL    0x1000000
#define SMP18_28CYCL    0x2000000
#define SMP18_56CYCL    0x3000000
#define SMP18_84CYCL    0x4000000
#define SMP18_112CYCL   0x5000000
#define SMP18_144CYCL   0x6000000
#define SMP18_480CYCL   0x7000000
// Channel 17 Sample time selection
#define SMP17_3CYCL     0x0000000
#define SMP17_15CYCL    0x0200000
#define SMP17_28CYCL    0x0400000
#define SMP17_56CYCL    0x0600000
#define SMP17_84CYCL    0x0800000
#define SMP17_112CYCL   0x0a00000
#define SMP17_144CYCL   0x0c00000
#define SMP17_480CYCL   0x0e00000
// Channel 16 Sample time selection
#define SMP16_3CYCL     0x0000000
#define SMP16_15CYCL    0x0040000
#define SMP16_28CYCL    0x0080000
#define SMP16_56CYCL    0x00c0000
#define SMP16_84CYCL    0x0100000
#define SMP16_112CYCL   0x0140000
#define SMP16_144CYCL   0x0180000
#define SMP16_480CYCL   0x01c0000
// Channel 15 Sample time selection
#define SMP15_3CYCL     0x0000000
#define SMP15_15CYCL    0x0008000
#define SMP15_28CYCL    0x0010000
#define SMP15_56CYCL    0x0018000
#define SMP15_84CYCL    0x0020000
#define SMP15_112CYCL   0x0028000
#define SMP15_144CYCL   0x0030000
#define SMP15_480CYCL   0x0038000
// Channel 14 Sample time selection
#define SMP14_3CYCL     0x0000000
#define SMP14_15CYCL    0x0001000
#define SMP14_28CYCL    0x0002000
#define SMP14_56CYCL    0x0003000
#define SMP14_84CYCL    0x0004000
#define SMP14_112CYCL   0x0005000
#define SMP14_144CYCL   0x0006000
#define SMP14_480CYCL   0x0007000
// Channel 13 Sample time selection
#define SMP13_3CYCL     0x0000000
#define SMP13_15CYCL    0x0000200
#define SMP13_28CYCL    0x0000400
#define SMP13_56CYCL    0x0000600
#define SMP13_84CYCL    0x0000800
#define SMP13_112CYCL   0x0000a00
#define SMP13_144CYCL   0x0000c00
#define SMP13_480CYCL   0x0000e00
// Channel 12 Sample time selection
#define SMP12_3CYCL     0x0000000
#define SMP12_15CYCL    0x0000040
#define SMP12_28CYCL    0x0000080
#define SMP12_56CYCL    0x00000c0
#define SMP12_84CYCL    0x0000100
#define SMP12_112CYCL   0x0000140
#define SMP12_144CYCL   0x0000180
#define SMP12_480CYCL   0x00001c0
// Channel 11 Sample time selection
#define SMP11_3CYCL     0x0000000
#define SMP11_15CYCL    0x0000008
#define SMP11_28CYCL    0x0000010
#define SMP11_56CYCL    0x0000018
#define SMP11_84CYCL    0x0000020
#define SMP11_112CYCL   0x0000028
#define SMP11_144CYCL   0x0000030
#define SMP11_480CYCL   0x0000038
// Channel 10 Sample time selection
#define SMP10_3CYCL     0x0000000
#define SMP10_15CYCL    0x0000001
#define SMP10_28CYCL    0x0000002
#define SMP10_56CYCL    0x0000003
#define SMP10_84CYCL    0x0000004
#define SMP10_112CYCL   0x0000005
#define SMP10_144CYCL   0x0000006
#define SMP10_480CYCL   0x0000007

/* ADC sample time register 2 */
#define ADC1_SMPR2  MMIO32(ADC1_BASE + 0x10)
#define ADC2_SMPR2  MMIO32(ADC2_BASE + 0x10)
#define ADC3_SMPR2  MMIO32(ADC3_BASE + 0x10)
// Channel 9 Sample time selection
#define SMP9_3CYCL      0x00000000
#define SMP9_15CYCL     0x08000000
#define SMP9_28CYCL     0x10000000
#define SMP9_56CYCL     0x18000000
#define SMP9_84CYCL     0x20000000
#define SMP9_112CYCL    0x28000000
#define SMP9_144CYCL    0x30000000
#define SMP9_480CYCL    0x38000000
// Channel 8 Sample time selection
#define SMP8_3CYCL      0x00000000
#define SMP8_15CYCL     0x01000000
#define SMP8_28CYCL     0x02000000
#define SMP8_56CYCL     0x03000000
#define SMP8_84CYCL     0x04000000
#define SMP8_112CYCL    0x05000000
#define SMP8_144CYCL    0x06000000
#define SMP8_480CYCL    0x07000000
// Channel 7 Sample time selection
#define SMP7_3CYCL      0x00000000
#define SMP7_15CYCL     0x00200000
#define SMP7_28CYCL     0x00400000
#define SMP7_56CYCL     0x00600000
#define SMP7_84CYCL     0x00800000
#define SMP7_112CYCL    0x00a00000
#define SMP7_144CYCL    0x00c00000
#define SMP7_480CYCL    0x00e00000
// Channel 6 Sample time selection
#define SMP6_3CYCL      0x00000000
#define SMP6_15CYCL     0x00040000
#define SMP6_28CYCL     0x00080000
#define SMP6_56CYCL     0x000c0000
#define SMP6_84CYCL     0x00100000
#define SMP6_112CYCL    0x00140000
#define SMP6_144CYCL    0x00180000
#define SMP6_480CYCL    0x001c0000
// Channel 5 Sample time selection
#define SMP5_3CYCL      0x00000000
#define SMP5_15CYCL     0x00008000
#define SMP5_28CYCL     0x00010000
#define SMP5_56CYCL     0x00018000
#define SMP5_84CYCL     0x00020000
#define SMP5_112CYCL    0x00028000
#define SMP5_144CYCL    0x00030000
#define SMP5_480CYCL    0x00038000
// Channel 4 Sample time selection
#define SMP4_3CYCL      0x00000000
#define SMP4_15CYCL     0x00001000
#define SMP4_28CYCL     0x00002000
#define SMP4_56CYCL     0x00003000
#define SMP4_84CYCL     0x00004000
#define SMP4_112CYCL    0x00005000
#define SMP4_144CYCL    0x00006000
#define SMP4_480CYCL    0x00007000
// Channel 3 Sample time selection
#define SMP3_3CYCL      0x00000000
#define SMP3_15CYCL     0x00000200
#define SMP3_28CYCL     0x00000400
#define SMP3_56CYCL     0x00000600
#define SMP3_84CYCL     0x00000800
#define SMP3_112CYCL    0x00000a00
#define SMP3_144CYCL    0x00000c00
#define SMP3_480CYCL    0x00000e00
// Channel 2 Sample time selection
#define SMP2_3CYCL      0x00000000
#define SMP2_15CYCL     0x00000040
#define SMP2_28CYCL     0x00000080
#define SMP2_56CYCL     0x000000c0
#define SMP2_84CYCL     0x00000100
#define SMP2_112CYCL    0x00000140
#define SMP2_144CYCL    0x00000180
#define SMP2_480CYCL    0x000001c0
// Channel 1 Sample time selection
#define SMP1_3CYCL      0x00000000
#define SMP1_15CYCL     0x00000008
#define SMP1_28CYCL     0x00000010
#define SMP1_56CYCL     0x00000018
#define SMP1_84CYCL     0x00000020
#define SMP1_112CYCL    0x00000028
#define SMP1_144CYCL    0x00000030
#define SMP1_480CYCL    0x00000038
// Channel 0 Sample time selection
#define SMP0_3CYCL      0x00000000
#define SMP0_15CYCL     0x00000001
#define SMP0_28CYCL     0x00000002
#define SMP0_56CYCL     0x00000003
#define SMP0_84CYCL     0x00000004
#define SMP0_112CYCL    0x00000005
#define SMP0_144CYCL    0x00000006
#define SMP0_480CYCL    0x00000007

/* ADC injected channel data offset registers */
#define ADC1_JOFR1  MMIO32(ADC1_BASE + 0x14)
#define ADC2_JOFR1  MMIO32(ADC2_BASE + 0x14)
#define ADC3_JOFR1  MMIO32(ADC3_BASE + 0x14)
#define ADC1_JOFR2  MMIO32(ADC1_BASE + 0x18)
#define ADC2_JOFR2  MMIO32(ADC2_BASE + 0x18)
#define ADC3_JOFR2  MMIO32(ADC3_BASE + 0x18)
#define ADC1_JOFR3  MMIO32(ADC1_BASE + 0x1c)
#define ADC2_JOFR3  MMIO32(ADC2_BASE + 0x1c)
#define ADC3_JOFR3  MMIO32(ADC3_BASE + 0x1c)
#define ADC1_JOFR4  MMIO32(ADC1_BASE + 0x20)
#define ADC2_JOFR4  MMIO32(ADC2_BASE + 0x20)
#define ADC3_JOFR4  MMIO32(ADC3_BASE + 0x20)
// JOFFSETx[11:0]: Data offset for injected channel x

/* ADC watchdog high threshold register */
#define ADC1_HTR    MMIO32(ADC1_BASE + 0x24)
#define ADC2_HTR    MMIO32(ADC2_BASE + 0x24)
#define ADC3_HTR    MMIO32(ADC3_BASE + 0x24)
// HT[11:0]: Analog watchdog higher threshold

/* ADC watchdog low threshold register */
#define ADC1_LTR    MMIO32(ADC1_BASE + 0x28)
#define ADC2_LTR    MMIO32(ADC2_BASE + 0x28)
#define ADC3_LTR    MMIO32(ADC3_BASE + 0x28)
// LT[11:0]: Analog watchdog lower threshold

/* ADC regular sequence register 1 */
#define ADC1_SQR1   MMIO32(ADC1_BASE + 0x2c)
#define ADC2_SQR1   MMIO32(ADC2_BASE + 0x2c)
#define ADC3_SQR1   MMIO32(ADC3_BASE + 0x2c)
// Regular channel sequence length
#define L_1CONV    0x000000
#define L_2CONV    0x100000
#define L_3CONV    0x200000
#define L_4CONV    0x300000
#define L_5CONV    0x400000
#define L_6CONV    0x500000
#define L_7CONV    0x600000
#define L_8CONV    0x700000
#define L_9CONV    0x800000
#define L_10CONV   0x900000
#define L_11CONV   0xa00000
#define L_12CONV   0xb00000
#define L_13CONV   0xc00000
#define L_14CONV   0xd00000
#define L_15CONV   0xe00000
#define L_16CONV   0xf00000
// channel number (0..17) assigned as the 16th in the sequence to be converted
#define SQ16(n)     ((uint32_t)((n&0x1f)<<15))
// channel number (0..17) assigned as the 15th in the sequence to be converted
#define SQ15(n)     ((uint32_t)((n&0x1f)<<10))
// channel number (0..17) assigned as the 14th in the sequence to be converted
#define SQ14(n)     ((uint32_t)((n&0x1f)<<5))
// channel number (0..17) assigned as the 13th in the sequence to be converted
#define SQ13(n)     ((uint32_t)(n&0x1f))

/* ADC regular sequence register 2 */
#define ADC1_SQR2   MMIO32(ADC1_BASE + 0x30)
#define ADC2_SQR2   MMIO32(ADC2_BASE + 0x30)
#define ADC3_SQR2   MMIO32(ADC3_BASE + 0x30)
// channel number (0..17) assigned as the 12th in the sequence to be converted
#define SQ12(n)     ((uint32_t)((n&0x1f)<<25))
// channel number (0..17) assigned as the 11th in the sequence to be converted
#define SQ11(n)     ((uint32_t)((n&0x1f)<<20))
// channel number (0..17) assigned as the 10th in the sequence to be converted
#define SQ10(n)     ((uint32_t)((n&0x1f)<<15))
// channel number (0..17) assigned as the 9th in the sequence to be converted
#define SQ9(n)      ((uint32_t)((n&0x1f)<<10))
// channel number (0..17) assigned as the 8th in the sequence to be converted
#define SQ8(n)      ((uint32_t)((n&0x1f)<<5))
// channel number (0..17) assigned as the 7th in the sequence to be converted
#define SQ7(n)      ((uint32_t)(n&0x1f))

/* ADC regular sequence register 3 */
#define ADC1_SQR3   MMIO32(ADC1_BASE + 0x34)
#define ADC2_SQR3   MMIO32(ADC2_BASE + 0x34)
#define ADC3_SQR3   MMIO32(ADC3_BASE + 0x34)
// channel number (0..17) assigned as the 6th in the sequence to be converted
#define SQ6(n)      ((uint32_t)((n&0x1f)<<25))
// channel number (0..17) assigned as the 5th in the sequence to be converted
#define SQ5(n)      ((uint32_t)((n&0x1f)<<20))
// channel number (0..17) assigned as the fourth in the sequence to be converted
#define SQ4(n)      ((uint32_t)((n&0x1f)<<15))
// channel number (0..17) assigned as the third in the sequence to be converted
#define SQ3(n)      ((uint32_t)((n&0x1f)<<10))
// channel number (0..17) assigned as the second in the sequence to be converted
#define SQ2(n)      ((uint32_t)((n&0x1f)<<5))
// channel number (0..17) assigned as the first in the sequence to be converted
#define SQ1(n)      ((uint32_t)(n&0x1f))


/* ADC injected sequence register */
#define ADC1_JSQR   MMIO32(ADC1_BASE + 0x38)
#define ADC2_JSQR   MMIO32(ADC2_BASE + 0x38)
#define ADC3_JSQR   MMIO32(ADC3_BASE + 0x38)
// Injected sequence length
#define JL_1CONV    0x000000
#define JL_2CONV    0x100000
#define JL_3CONV    0x200000
#define JL_4CONV    0x300000
// channel number (0..17) assigned as the fourth in the sequence to be converted
#define JSQ4(n)     ((uint32_t)((n&0x1f)<<15))
// channel number (0..17) assigned as the third in the sequence to be converted
#define JSQ3(n)     ((uint32_t)((n&0x1f)<<10))
// channel number (0..17) assigned as the second in the sequence to be converted
#define JSQ2(n)     ((uint32_t)((n&0x1f)<<5))
// channel number (0..17) assigned as the first in the sequence to be converted
#define JSQ1(n)     ((uint32_t)(n&0x1f))

/* ADC injected data registers */
#define ADC1_JDR1   MMIO32(ADC1_BASE + 0x3c)
#define ADC2_JDR1   MMIO32(ADC2_BASE + 0x3c)
#define ADC3_JDR1   MMIO32(ADC3_BASE + 0x3c)
#define ADC1_JDR2   MMIO32(ADC1_BASE + 0x40)
#define ADC2_JDR2   MMIO32(ADC2_BASE + 0x40)
#define ADC3_JDR2   MMIO32(ADC3_BASE + 0x40)
#define ADC1_JDR3   MMIO32(ADC1_BASE + 0x44)
#define ADC2_JDR3   MMIO32(ADC2_BASE + 0x44)
#define ADC3_JDR3   MMIO32(ADC3_BASE + 0x44)
#define ADC1_JDR4   MMIO32(ADC1_BASE + 0x48)
#define ADC2_JDR4   MMIO32(ADC2_BASE + 0x48)
#define ADC3_JDR4   MMIO32(ADC3_BASE + 0x48)
// JDATA[15:0]: Injected data

/* ADC regular data register */
#define ADC1_DR     MMIO32(ADC1_BASE + 0x4c)
#define ADC2_DR     MMIO32(ADC2_BASE + 0x4c)
#define ADC3_DR     MMIO32(ADC3_BASE + 0x4c)
// DATA[15:0]: Regular data
#define DATA_MSK        0xffff

/* ADC Common status register */
#define ADC_CSR     MMIO32(ADC1_BASE + 0x300)
// Overrun flag of ADC
#define OVR3    0x00200000
#define OVR2    0x00002000
#define OVR1    0x00000020
// Regular channel Start flag of ADC
#define STRT3   0x00100000
#define STRT2   0x00001000
#define STRT1   0x00000010
// Injected channel Start flag of ADC
#define JSTRT3  0x00080000
#define JSTRT2  0x00000800
#define JSTRT1  0x00000008
// Injected channel end of conversion of ADC
#define JEOC3   0x00040000
#define JEOC2   0x00000400
#define JEOC1   0x00000004
// End of conversion of ADC
#define EOC3    0x00020000
#define EOC2    0x00000200
#define EOC1    0x00000002
// Analog watchdog flag of ADC
#define AWD3    0x00010000
#define AWD2    0x00000100
#define AWD1    0x00000001

/* ADC common control register */
#define ADC_CCR     MMIO32(ADC1_BASE + 0x304)
// Temperature sensor and VREFINT enable
#define TSVREFE                                 0x00800000
// VBAT enable
#define VBATE                                   0x00400000
// ADC prescaler
#define ADCPRE_DIV2                             0x00000000
#define ADCPRE_DIV4                             0x00010000
#define ADCPRE_DIV6                             0x00020000
#define ADCPRE_DIV8                             0x00030000
// Direct memory access mode for multi ADC mode
#define DMA_DISABLED                            0x00000000
#define DMA_16BIT_1_2_3                         0x00004000
#define DMA_16BIT_21_13_32                      0x00008000
#define DMA_8BIT_21_13_32                       0x0000c000
// DMA disable selection (for multi-ADC mode)
#define DDS                                     0x00002000
// Delay between 2 sampling phases
#define DELAY_5T                                0x00000000
#define DELAY_6T                                0x00000100
#define DELAY_7T                                0x00000200
#define DELAY_8T                                0x00000300
#define DELAY_9T                                0x00000400
#define DELAY_10T                               0x00000500
#define DELAY_11T                               0x00000600
#define DELAY_12T                               0x00000700
#define DELAY_13T                               0x00000800
#define DELAY_14T                               0x00000900
#define DELAY_15T                               0x00000a00
#define DELAY_16T                               0x00000b00
#define DELAY_17T                               0x00000c00
#define DELAY_18T                               0x00000d00
#define DELAY_19T                               0x00000e00
#define DELAY_20T                               0x00000f00
// Multi ADC mode selection
#define MULTI_INDEPENDENT_MODE                  0x00000000
#define MULTI_DUAL_REGULAR_SIMUL_INJ_SIMUL      0x00000001
#define MULTI_DUAL_REGULAR_SIMUL_ALT_SIMUL      0x00000002
#define MULTI_DUAL_INJECTED_SIMULATEOUS         0x00000005
#define MULTI_DUAL_REGULAR_SIMULATEOUS          0x00000006
#define MULTI_DUAL_INTERLEAVED_MODE_ONLY        0x00000007
#define MULTI_DUAL_ALTERNATE_TRIGGER_ONLY       0x00000009
#define MULTI_TRIPLE_REGULAR_SIMUL_INJ_SIMUL    0x00000011
#define MULTI_TRIPLE_REGULAR_SIMUL_ALT_SIMUL    0x00000012
#define MULTI_TRIPLE_INJECTED_SIMULATEOUS       0x00000015
#define MULTI_TRIPLE_REGULAR_SIMULATEOUS        0x00000016
#define MULTI_TRIPLE_INTERLEAVED_MODE_ONLY      0x00000017
#define MULTI_TRIPLE_ALTERNATE_TRIGGER_ONLY     0x00000019

/* ADC common regular data register for dual and triple modes */
#define ADC_CDR     MMIO32(ADC1_BASE + 0x308)
// 2nd data item of a pair of regular conversions
#define DATA2   ((ADC_CDR>>16)&0xffff)
// 1st data item of a pair of regular conversions
#define DATA1   (ADC_CDR&0xffff)

#endif
