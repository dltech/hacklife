#ifndef H_EXTI_REG
#define H_EXTI_REG
/*
 * Part of Belkin STM32 HAL, External interrupt/event controller
 * (EXTI) registers of STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* Interrupt mask register */
#define EXTI_IMR MMIO32(EXTI_BASE + 0x00)
// Interrupt Mask on line x
#define MR(x)   ((uint32_t)(1<<x))
#define MR0     0x000001
#define MR1     0x000002
#define MR2     0x000004
#define MR3     0x000008
#define MR4     0x000010
#define MR5     0x000020
#define MR6     0x000040
#define MR7     0x000080
#define MR8     0x000100
#define MR9     0x000200
#define MR10    0x000400
#define MR11    0x000800
#define MR12    0x001000
#define MR13    0x002000
#define MR14    0x004000
#define MR15    0x008000
#define MR16    0x010000
#define MR17    0x020000
#define MR18    0x040000
#define MR19    0x080000
#define MR20    0x100000
#define MR21    0x200000
#define MR22    0x400000

/* Event mask register */
#define EXTI_EMR MMIO32(EXTI_BASE + 0x04)
// MRx: Event mask on line x

/* Rising trigger selection register */
#define EXTI_RTSR MMIO32(EXTI_BASE + 0x08)
// Rising trigger event configuration bit of line x
#define TR(x)   ((uint32_t)(1<<x))
#define TR0     0x000001
#define TR1     0x000002
#define TR2     0x000004
#define TR3     0x000008
#define TR4     0x000010
#define TR5     0x000020
#define TR6     0x000040
#define TR7     0x000080
#define TR8     0x000100
#define TR9     0x000200
#define TR10    0x000400
#define TR11    0x000800
#define TR12    0x001000
#define TR13    0x002000
#define TR14    0x004000
#define TR15    0x008000
#define TR16    0x010000
#define TR17    0x020000
#define TR18    0x040000
#define TR19    0x080000
#define TR20    0x100000
#define TR21    0x200000
#define TR22    0x400000

/* Falling trigger selection register */
#define EXTI_FTSR MMIO32(EXTI_BASE + 0x0c)
// Falling trigger event configuration bit of line x

/* Software interrupt event register */
#define EXTI_SWIER MMIO32(EXTI_BASE + 0x10)
// Software interrupt on line x
#define SWIER(x)    ((uint32_t)(1<<x))
#define SWIER0      0x000001
#define SWIER1      0x000002
#define SWIER2      0x000004
#define SWIER3      0x000008
#define SWIER4      0x000010
#define SWIER5      0x000020
#define SWIER6      0x000040
#define SWIER7      0x000080
#define SWIER8      0x000100
#define SWIER9      0x000200
#define SWIER10     0x000400
#define SWIER11     0x000800
#define SWIER12     0x001000
#define SWIER13     0x002000
#define SWIER14     0x004000
#define SWIER15     0x008000
#define SWIER16     0x010000
#define SWIER17     0x020000
#define SWIER18     0x040000
#define SWIER19     0x080000
#define SWIER20     0x100000
#define SWIER21     0x200000
#define SWIER22     0x400000

/* Pending register */
#define EXTI_PR MMIO32(EXTI_BASE + 0x14)
// Pending bit 1: selected trigger request occurred
#define PR(x)   ((uint32_t)(1<<x))
#define PR0     0x000001
#define PR1     0x000002
#define PR2     0x000004
#define PR3     0x000008
#define PR4     0x000010
#define PR5     0x000020
#define PR6     0x000040
#define PR7     0x000080
#define PR8     0x000100
#define PR9     0x000200
#define PR10    0x000400
#define PR11    0x000800
#define PR12    0x001000
#define PR13    0x002000
#define PR14    0x004000
#define PR15    0x008000
#define PR16    0x010000
#define PR17    0x020000
#define PR18    0x040000
#define PR19    0x080000
#define PR20    0x100000
#define PR21    0x200000
#define PR22    0x400000

#endif
