#ifndef H_RNG_REG
#define H_RNG_REG
/*
 * Part of Belkin STM32 HAL, Random number generator (RNG)
 * registers of STMF4xx MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* RNG control register */
#define RNG_CR  MMIO32(RNG_BASE + 0x00)
// Interrupt enable
#define RNG_IE  0x8
// Random number generator enable
#define RNGEN   0x4

/* RNG status register */
#define RNG_SR  MMIO32(RNG_BASE + 0x04)
// Seed error interrupt status
#define SEIS    0x40
// Clock error interrupt status
#define CEIS    0x20
// Seed error current status
#define SECS    0x04
// Clock error current status
#define CECS    0x02
// Data ready
#define DRDY    0x01

/* RNG data register */
#define RNG_DR  MMIO32(RNG_BASE + 0x08)
// RNDATA[31:0]: Random data

#endif
