#ifndef H_DMA_REG
#define H_DMA_REG
/*
 * Part of Belkin STM32 HAL, DMA register definitions of STM32F4xx MCUs.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"

/* DMA low interrupt status register */
#define DMA_LISR MMIO32(DMA1_BASE + 0x00)
#define DMA_LISR MMIO32(DMA2_BASE + 0x00)
// Stream x transfer complete interrupt flag
#define TCIF3   0x08000000
#define TCIF2   0x00200000
#define TCIF1   0x00000800
#define TCIF0   0x00000020
// Stream x half transfer interrupt flag
#define HTIF3   0x04000000
#define HTIF2   0x00100000
#define HTIF1   0x00000400
#define HTIF0   0x00000010
// Stream x transfer error interrupt flag
#define TEIF3   0x02000000
#define TEIF2   0x00080000
#define TEIF1   0x00000200
#define TEIF0   0x00000008
// Stream x direct mode error interrupt flag
#define DMEIF3  0x01000000
#define DMEIF2  0x00040000
#define DMEIF1  0x00000100
#define DMEIF0  0x00000004
// Stream x FIFO error interrupt flag
#define FEIF3   0x00400000
#define FEIF2   0x00010000
#define FEIF1   0x00000040
#define FEIF0   0x00000001

/* DMA high interrupt status register */
#define DMA_HISR MMIO32(DMA1_BASE + 0x04)
#define DMA_HISR MMIO32(DMA2_BASE + 0x04)
// Stream x transfer complete interrupt flag
#define TCIF7   0x08000000
#define TCIF6   0x00200000
#define TCIF5   0x00000800
#define TCIF4   0x00000020
// Stream x half transfer interrupt flag
#define HTIF7   0x04000000
#define HTIF6   0x00100000
#define HTIF5   0x00000400
#define HTIF4   0x00000010
// Stream x transfer error interrupt flag
#define TEIF7   0x02000000
#define TEIF6   0x00080000
#define TEIF5   0x00000200
#define TEIF4   0x00000008
// Stream x direct mode error interrupt flag
#define DMEIF7  0x01000000
#define DMEIF6  0x00040000
#define DMEIF5  0x00000100
#define DMEIF4  0x00000004
// Stream x FIFO error interrupt flag
#define FEIF7   0x00400000
#define FEIF6   0x00010000
#define FEIF5   0x00000040
#define FEIF4   0x00000001

/* DMA low interrupt flag clear register */
#define DMA_LIFCR MMIO32(DMA1_BASE + 0x08)
#define DMA_LIFCR MMIO32(DMA2_BASE + 0x08)
// Stream x clear transfer complete interrupt flag
#define CTCIF3  0x08000000
#define CTCIF2  0x00200000
#define CTCIF1  0x00000800
#define CTCIF0  0x00000020
// CHTIFx: Stream x clear half transfer interrupt flag
#define CHTIF3  0x04000000
#define CHTIF2  0x00100000
#define CHTIF1  0x00000400
#define CHTIF0  0x00000010
// CTEIFx: Stream x clear transfer error interrupt flag
#define CTEIF3  0x02000000
#define CTEIF2  0x00080000
#define CTEIF1  0x00000200
#define CTEIF0  0x00000008
// CDMEIFx: Stream x clear direct mode error interrupt flag
#define CDMEIF3 0x01000000
#define CDMEIF2 0x00040000
#define CDMEIF1 0x00000100
#define CDMEIF0 0x00000004
// CFEIFx: Stream x clear FIFO error interrupt flag
#define CFEIF3  0x00400000
#define CFEIF2  0x00010000
#define CFEIF1  0x00000040
#define CFEIF0  0x00000001

/* DMA high interrupt flag clear register */
#define DMA_HIFCR MMIO32(DMA1_BASE + 0x0c)
#define DMA_HIFCR MMIO32(DMA2_BASE + 0x0c)
// Stream x clear transfer complete interrupt flag
#define CTCIF7  0x08000000
#define CTCIF6  0x00200000
#define CTCIF5  0x00000800
#define CTCIF4  0x00000020
// CHTIFx: Stream x clear half transfer interrupt flag
#define CHTIF7  0x04000000
#define CHTIF6  0x00100000
#define CHTIF5  0x00000400
#define CHTIF4  0x00000010
// CTEIFx: Stream x clear transfer error interrupt flag
#define CTEIF7  0x02000000
#define CTEIF6  0x00080000
#define CTEIF5  0x00000200
#define CTEIF4  0x00000008
// CDMEIFx: Stream x clear direct mode error interrupt flag
#define CDMEIF7 0x01000000
#define CDMEIF6 0x00040000
#define CDMEIF5 0x00000100
#define CDMEIF4 0x00000004
// CFEIFx: Stream x clear FIFO error interrupt flag
#define CFEIF7  0x00400000
#define CFEIF6  0x00010000
#define CFEIF5  0x00000040
#define CFEIF4  0x00000001

/* DMA stream x configuration register */
#define DMA1_S0CR MMIO32(DMA1_BASE + 0x10)
#define DMA2_S0CR MMIO32(DMA2_BASE + 0x10)
#define DMA1_S1CR MMIO32(DMA1_BASE + 0x28)
#define DMA2_S1CR MMIO32(DMA2_BASE + 0x28)
#define DMA1_S2CR MMIO32(DMA1_BASE + 0x40)
#define DMA2_S2CR MMIO32(DMA2_BASE + 0x40)
#define DMA1_S3CR MMIO32(DMA1_BASE + 0x58)
#define DMA2_S3CR MMIO32(DMA2_BASE + 0x58)
#define DMA1_S4CR MMIO32(DMA1_BASE + 0x70)
#define DMA2_S4CR MMIO32(DMA2_BASE + 0x70)
#define DMA1_S5CR MMIO32(DMA1_BASE + 0x88)
#define DMA2_S5CR MMIO32(DMA2_BASE + 0x88)
#define DMA1_S6CR MMIO32(DMA1_BASE + 0xa0)
#define DMA2_S6CR MMIO32(DMA2_BASE + 0xa0)
#define DMA1_S7CR MMIO32(DMA1_BASE + 0xb8)
#define DMA2_S7CR MMIO32(DMA2_BASE + 0xb8)
// Channel selection
#define CHSEL0          0x00000000
#define CHSEL1          0x02000000
#define CHSEL2          0x04000000
#define CHSEL3          0x06000000
#define CHSEL4          0x08000000
#define CHSEL5          0x0a000000
#define CHSEL6          0x0c000000
#define CHSEL7          0x0e000000
// Memory burst transfer configuration
#define MBURST_SINGLE   0x00000000
#define MBURST_INCR4    0x00800000
#define MBURST_INCR8    0x01000000
#define MBURST_INCR16   0x01800000
// Peripheral burst transfer configuration
#define PBURST_SINGLE   0x00000000
#define PBURST_INCR4    0x00200000
#define PBURST_INCR8    0x00400000
#define PBURST_INCR16   0x00600000
// Current target (only in double buffer mode)
#define CT              0x00080000
// Double buffer mode
#define DBM             0x00040000
// Priority level
#define PL_LOW          0x00000000
#define PL_MEDIUM       0x00010000
#define PL_HIGH         0x00020000
#define PL_VERY_HIGH    0x00030000
// Peripheral increment offset size
#define PINCOS          0x00008000
// Memory data size
#define MSIZE_8BIT      0x00000000
#define MSIZE_16BIT     0x00002000
#define MSIZE_32BIT     0x00004000
// Peripheral data size
#define PSIZE_8BIT      0x00000000
#define PSIZE_16BIT     0x00000800
#define PSIZE_32BIT     0x00001000
// Memory increment mode
#define MINC            0x00000400
// Peripheral increment mode
#define PINC            0x00000200
// Circular mode
#define CIRC            0x00000100
// Data transfer direction
#define DIR_P2M         0x00000000
#define DIR_M2P         0x00000040
#define DIR_M2M         0x00000080
// Peripheral flow controller
#define PFCTRL          0x00000020
// Transfer complete interrupt enable
#define TCIE            0x00000010
// Half transfer interrupt enable
#define HTIE            0x00000008
// Transfer error interrupt enable
#define TEIE            0x00000004
// Direct mode error interrupt enable
#define DMEIE           0x00000002
// Stream enable / flag stream ready when read low
#define DMA_EN          0x00000001

/* DMA stream x number of data register */
#define DMA1_S0NDTR MMIO32(DMA1_BASE + 0x14)
#define DMA2_S0NDTR MMIO32(DMA2_BASE + 0x14)
#define DMA1_S1NDTR MMIO32(DMA1_BASE + 0x2c)
#define DMA2_S1NDTR MMIO32(DMA2_BASE + 0x2c)
#define DMA1_S2NDTR MMIO32(DMA1_BASE + 0x44)
#define DMA2_S2NDTR MMIO32(DMA2_BASE + 0x44)
#define DMA1_S3NDTR MMIO32(DMA1_BASE + 0x5c)
#define DMA2_S3NDTR MMIO32(DMA2_BASE + 0x5c)
#define DMA1_S4NDTR MMIO32(DMA1_BASE + 0x74)
#define DMA2_S4NDTR MMIO32(DMA2_BASE + 0x74)
#define DMA1_S5NDTR MMIO32(DMA1_BASE + 0x8c)
#define DMA2_S5NDTR MMIO32(DMA2_BASE + 0x8c)
#define DMA1_S6NDTR MMIO32(DMA1_BASE + 0xa4)
#define DMA2_S6NDTR MMIO32(DMA2_BASE + 0xa4)
#define DMA1_S7NDTR MMIO32(DMA1_BASE + 0xbc)
#define DMA2_S7NDTR MMIO32(DMA2_BASE + 0xbc)
// NDT[15:0]: Number of data items to transfer

/* DMA stream x peripheral address register */
#define DMA1_S0PAR MMIO32(DMA1_BASE + 0x18)
#define DMA2_S0PAR MMIO32(DMA2_BASE + 0x18)
#define DMA1_S1PAR MMIO32(DMA1_BASE + 0x30)
#define DMA2_S1PAR MMIO32(DMA2_BASE + 0x30)
#define DMA1_S2PAR MMIO32(DMA1_BASE + 0x48)
#define DMA2_S2PAR MMIO32(DMA2_BASE + 0x48)
#define DMA1_S3PAR MMIO32(DMA1_BASE + 0x60)
#define DMA2_S3PAR MMIO32(DMA2_BASE + 0x60)
#define DMA1_S4PAR MMIO32(DMA1_BASE + 0x78)
#define DMA2_S4PAR MMIO32(DMA2_BASE + 0x78)
#define DMA1_S5PAR MMIO32(DMA1_BASE + 0x90)
#define DMA2_S5PAR MMIO32(DMA2_BASE + 0x90)
#define DMA1_S6PAR MMIO32(DMA1_BASE + 0xa8)
#define DMA2_S6PAR MMIO32(DMA2_BASE + 0xa8)
#define DMA1_S7PAR MMIO32(DMA1_BASE + 0xc0)
#define DMA2_S7PAR MMIO32(DMA2_BASE + 0xc0)
// PAR[31:0]: Peripheral address

/* DMA stream x memory 0 address register */
#define DMA1_S0M0AR MMIO32(DMA1_BASE + 0x1c)
#define DMA2_S0M0AR MMIO32(DMA2_BASE + 0x1c)
#define DMA1_S1M0AR MMIO32(DMA1_BASE + 0x34)
#define DMA2_S1M0AR MMIO32(DMA2_BASE + 0x34)
#define DMA1_S2M0AR MMIO32(DMA1_BASE + 0x4c)
#define DMA2_S2M0AR MMIO32(DMA2_BASE + 0x4c)
#define DMA1_S3M0AR MMIO32(DMA1_BASE + 0x64)
#define DMA2_S3M0AR MMIO32(DMA2_BASE + 0x64)
#define DMA1_S4M0AR MMIO32(DMA1_BASE + 0x7c)
#define DMA2_S4M0AR MMIO32(DMA2_BASE + 0x7c)
#define DMA1_S5M0AR MMIO32(DMA1_BASE + 0x94)
#define DMA2_S5M0AR MMIO32(DMA2_BASE + 0x94)
#define DMA1_S6M0AR MMIO32(DMA1_BASE + 0xac)
#define DMA2_S6M0AR MMIO32(DMA2_BASE + 0xac)
#define DMA1_S7M0AR MMIO32(DMA1_BASE + 0xc4)
#define DMA2_S7M0AR MMIO32(DMA2_BASE + 0xc4)
// M0A[31:0]: Memory 0 address

/* DMA stream x memory 1 address register */
#define DMA1_S0M1AR MMIO32(DMA1_BASE + 0x20)
#define DMA2_S0M1AR MMIO32(DMA2_BASE + 0x20)
#define DMA1_S1M1AR MMIO32(DMA1_BASE + 0x38)
#define DMA2_S1M1AR MMIO32(DMA2_BASE + 0x38)
#define DMA1_S2M1AR MMIO32(DMA1_BASE + 0x50)
#define DMA2_S2M1AR MMIO32(DMA2_BASE + 0x50)
#define DMA1_S3M1AR MMIO32(DMA1_BASE + 0x68)
#define DMA2_S3M1AR MMIO32(DMA2_BASE + 0x68)
#define DMA1_S4M1AR MMIO32(DMA1_BASE + 0x80)
#define DMA2_S4M1AR MMIO32(DMA2_BASE + 0x80)
#define DMA1_S5M1AR MMIO32(DMA1_BASE + 0x98)
#define DMA2_S5M1AR MMIO32(DMA2_BASE + 0x98)
#define DMA1_S6M1AR MMIO32(DMA1_BASE + 0xb0)
#define DMA2_S6M1AR MMIO32(DMA2_BASE + 0xb0)
#define DMA1_S7M1AR MMIO32(DMA1_BASE + 0xc8)
#define DMA2_S7M1AR MMIO32(DMA2_BASE + 0xc8)
// M1A[31:0]: Memory 1 address (used in case of Double buffer mode)

/* DMA stream x FIFO control register */
#define DMA1_S0FCR MMIO32(DMA1_BASE + 0x24)
#define DMA2_S0FCR MMIO32(DMA2_BASE + 0x24)
#define DMA1_S1FCR MMIO32(DMA1_BASE + 0x3c)
#define DMA2_S1FCR MMIO32(DMA2_BASE + 0x3c)
#define DMA1_S2FCR MMIO32(DMA1_BASE + 0x54)
#define DMA2_S2FCR MMIO32(DMA2_BASE + 0x54)
#define DMA1_S3FCR MMIO32(DMA1_BASE + 0x6c)
#define DMA2_S3FCR MMIO32(DMA2_BASE + 0x6c)
#define DMA1_S4FCR MMIO32(DMA1_BASE + 0x84)
#define DMA2_S4FCR MMIO32(DMA2_BASE + 0x84)
#define DMA1_S5FCR MMIO32(DMA1_BASE + 0x9c)
#define DMA2_S5FCR MMIO32(DMA2_BASE + 0x9c)
#define DMA1_S6FCR MMIO32(DMA1_BASE + 0xb4)
#define DMA2_S6FCR MMIO32(DMA2_BASE + 0xb4)
#define DMA1_S7FCR MMIO32(DMA1_BASE + 0xcc)
#define DMA2_S7FCR MMIO32(DMA2_BASE + 0xcc)
// FIFO error interrupt enable
#define FEIE        0x80
// FIFO status
#define FS_0_0P25   0x00
#define FS_0P25_0P5 0x08
#define FS_0P5_0P75 0x10
#define FS_0P25_1   0x18
#define FS_EMPTY    0x20
#define FS_FULL     0x28
// Direct mode disable
#define DMDIS       0x04
// FIFO threshold selection
#define FTH_0P25    0x00
#define FTH_0P5     0x01
#define FTH_0P75    0x02
#define FTH_FULL    0x03

#endif
