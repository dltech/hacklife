#ifndef H_TYPE_DETECT
#define H_TYPE_DETECT
/*
 * Part of Belkin STM32 HAL, type of controller detection file
 * for the whole microcontroller family support.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined(STM32F101) || defined(STM32F102) || defined(STM32F103)
#define XXX_DENSITY
#endif

#if defined(STM32F105) || defined(STM32F107)
#define CONNECTIVITY
#endif

#endif

