#ifndef H_FLASH_REGS
#define H_FLASH_REGS
/*
 * Part of Belkin STM32 HAL, Flash program and erase controller (FPEC)
 * registers of STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* Flash access control register */
#define FLASH_ACR       MMIO32(FLASH_BASE + 0x00)
// Prefetch buffer status
#define PRFTBS      0x20
// Prefetch buffer enable
#define PRFTBE      0x10
// Flash half cycle access enable
#define HLFCYA      0x08
// Latency
#define LATENCY_24M 0x00
#define LATENCY_48M 0x01
#define LATENCY_72M 0x02

/* FPEC key register */
#define FLASH_KEYR      MMIO32(FLASH_BASE + 0x04)
// FKEYR[31:0] These bits represent the keys to unlock the FPEC.
#define KEY1    0x45670123
#define KEY2    0xcdef89ab

/* Flash OPTKEY register */
#define FLASH_OPTKEYR   MMIO32(FLASH_BASE + 0x08)
// OPTKEYR[31:0] These bits represent the keys to unlock the OPTWRE.

/* Flash status register */
#define FLASH_SR        MMIO32(FLASH_BASE + 0x0c)
// End of operation
#define EOP         0x20
// Write protection error
#define WRPRTERR    0x10
// Programming error
#define PGERR       0x04
// Busy
#define BSY         0x01

/* Flash control register */
#define FLASH_CR        MMIO32(FLASH_BASE + 0x10)
// End of operation interrupt enable
#define EOPIE   0x1000
// Error interrupt enable
#define ERRIE   0x0400
// Option bytes write enable
#define OPTWRE  0x0200
// Lock
#define LOCK    0x0080
// Start
#define STRT    0x0040
// Option byte erase
#define OPTER   0x0020
// Option byte programming
#define OPTPG   0x0010
// Mass erase
#define MER     0x0004
// Page erase
#define PER     0x0002
// Programming
#define PG      0x0001

/* Flash address register */
#define FLASH_AR        MMIO32(FLASH_BASE + 0x14)
// FAR[31:0] Flash Address

/* Option byte register */
#define FLASH_OBR       MMIO32(FLASH_BASE + 0x1c)
// Data1
#define DATA1O_MSK      0x03fc0000
#define DATA1O_SFT      18
#define DATA1_GET_O     ((FLASH_OBR>>DATAO1_SFT)&0xff)
// Data0
#define DATA0O_MSK       0x0003fc00
#define DATA0O_SFT       10
#define DATA0_GET_O     ((FLASH_OBR>>DATAO0_SFT)&0xff)
// User option byte
#define NRST_STDBY_O    0x10
#define NRST_STOP_O     0x08
#define WDG_SW_O        0x04
// Read protection
#define RDPRT       0x02
// Option byte error
#define OPTERR      0x01

/* Write protection register */
#define FLASH_WRPR      MMIO32(FLASH_BASE + 0x20)
// WRP[31:0], 1: Write protection not active

/*** Option Flash bytes ***/
#define OPTION0         0x1ffff800
// User option bytes
#define NRST_STDBY  0x00040000
#define NRST_STOP   0x00020000
#define WDG_SW      0x00010000
// Read protection option byte
#define RDP_MSK     0x0000000f
#define RDPRTO      0x00a5

#define OPTION1         0x1ffff804
// Two bytes for user data storage.
#define DATA1_MSK   0x00ff0000
#define DATA1_SFT   16
#define DATA0_MSK   0x000000ff

#define OPTION2         0x1ffff808
// Flash memory write protection option bytes
#define WRP1_MSK    0x00ff0000
#define WRP1_SFT    16
#define WRP0_MSK    0x000000ff

#define OPTION3         0x1ffff80c
#define WRP3_MSK    0x00ff0000
#define WRP3_SFT    16
#define WRP2_MSK    0x000000ff

#endif
