#ifndef H_RTC_REGS
#define H_RTC_REGS
/*
 * Part of Belkin STM32 HAL, Real-time clock (RTC) registers of
 * STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* RTC control register high */
#define RTC_CRH     MMIO32(RTC_BASE + 0x00)
// Overflow interrupt enable
#define OWIE    0x4
// Alarm interrupt enable
#define ALRIE   0x2
// Second interrupt enable
#define SECIE   0x1

/* RTC control register low */
#define RTC_CRL     MMIO32(RTC_BASE + 0x04)
// RTC operation OFF
#define RTOFF   0x20
// Configuration flag 1: Enter configuration mode.
#define CNF     0x10
// Registers synchronized flag
#define RSF     0x08
// Overflow flag 1: 32-bit programmable counter overflow occurred.
#define OWF     0x04
// Alarm flag
#define ALRF    0x02
// Second flag, set when the 32-bit programmable prescaler overflows
#define SECF    0x01

/* RTC prescaler load register high */
#define RTC_PRLH    MMIO32(RTC_BASE + 0x08)
// PRL[19:16]: RTC prescaler reload value
#define PRLH_SFT    16
#define PRLH_MSK    0xf
#define PRLH_CALC(in_freq)   (((in_freq-1) >> PRLH_SFT)&PRLH_MSK)
#define PRLH_32768  0x0

/* RTC prescaler load register low */
#define RTC_PRLL    MMIO32(RTC_BASE + 0x0c)
// PRL[15:0] fTR_CLK = fRTCCLK/(PRL[19:0]+1)
#define PRLL_MSK    0xf
#define PRLL_CALC(in_freq)   ((in_freq-1)&PRLL_MSK)
#define PRLL_32768  0x7fff

/* RTC prescaler divider register high */
#define RTC_DIVH    MMIO32(RTC_BASE + 0x10)
// RTC_DIV[19:16]: RTC clock divider high

/* RTC prescaler divider register low */
#define RTC_DIVL    MMIO32(RTC_BASE + 0x14)
// RTC_DIV[15:0]: RTC clock divider low

/* RTC counter register high */
#define RTC_CNTH    MMIO32(RTC_BASE + 0x18)
// RTC_CNT[31:16]: RTC counter high

/* RTC counter register low */
#define RTC_CNTL    MMIO32(RTC_BASE + 0x1c)
// RTC_CNT[15:0]: RTC counter low

/* RTC alarm register high */
#define RTC_ALRH    MMIO32(RTC_BASE + 0x20)
// RTC_ALR[31:16]: RTC alarm high
#define ALRH_SFT    16
#define ALRH_MSK    0xffff

/* RTC alarm register low */
#define RTC_ALRL MMIO32(RTC_BASE + 0x24)
// RTC_ALR[15:0]: RTC alarm low
#define ALRL_MSK    0xffff

#endif
