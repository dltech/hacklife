#ifndef H_FSMC_REGS
#define H_FSMC_REGS
/*
 * Part of Belkin STM32 HAL, Flexible static memory controller
 * (FSMC) registers of STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* SRAM/NOR-Flash chip-select control registers 1..4 */
#define FSMC_BCR1 MMIO32(FMSC_BASE + 0x000)
#define FSMC_BCR2 MMIO32(FMSC_BASE + 0x008)
#define FSMC_BCR3 MMIO32(FMSC_BASE + 0x010)
#define FSMC_BCR4 MMIO32(FMSC_BASE + 0x018)
// Write burst enable.
#define CBURSTRW    0x80000
// CRAM page size.
#define CPSIZE128   0x10000
#define CPSIZE256   0x20000
#define CPSIZE512   0x30000
#define CPSIZE1024  0x40000
// Wait signal during asynchronous transfers
#define ASYNCWAIT   0x08000
// Extended mode enable.
#define EXTMOD      0x04000
// Wait enable bit.
#define WAITEN      0x02000
// Write enable bit.
#define WREN        0x01000
// Wait timing configuration.
#define WAITCFG     0x00800
// Wrapped burst mode support.
#define WRAPMOD     0x00400
// Wait signal polarity bit.
#define WAITPOL     0x00200
// Burst enable bit.
#define BURSTEN     0x00100
// Flash access enable
#define FACCEN      0x00040
// Memory databus width.
#define MWID8BIT    0x00000
#define MWID16BIT   0x00010
// Memory type.
#define MTYP_SRAM   0x00000
#define MTYP_PSRAM  0x00004
#define MTYP_NOR    0x00008
// Address/data multiplexing enable bit.
#define MUXEN       0x00002
// Memory bank enable bit.
#define MBKEN       0x00001

/* SRAM/NOR-Flash chip-select timing registers 1..4 */
#define FSMC_BTR1 MMIO32(FMSC_BASE + 0x004)
#define FSMC_BTR2 MMIO32(FMSC_BASE + 0x00c)
#define FSMC_BTR3 MMIO32(FMSC_BASE + 0x014)
#define FSMC_BTR4 MMIO32(FMSC_BASE + 0x01c)
// Access mode
#define ACCMOD_A    0x00000000
#define ACCMOD_B    0x10000000
#define ACCMOD_C    0x20000000
#define ACCMOD_D    0x30000000
// Data latency for synchronous NOR Flash memory
#define DATLAT2     0x00000000
#define DATLAT3     0x01000000
#define DATLAT4     0x02000000
#define DATLAT5     0x03000000
#define DATLAT6     0x04000000
#define DATLAT7     0x05000000
#define DATLAT8     0x06000000
#define DATLAT9     0x07000000
#define DATLAT10    0x08000000
#define DATLAT11    0x09000000
#define DATLAT12    0x0a000000
#define DATLAT13    0x0b000000
#define DATLAT14    0x0c000000
#define DATLAT15    0x0d000000
#define DATLAT16    0x0e000000
#define DATLAT17    0x0f000000
// Clock divide ratio (for FSMC_CLK signal)
#define CLKDIV2     0x00100000
#define CLKDIV3     0x00200000
#define CLKDIV4     0x00300000
#define CLKDIV5     0x00400000
#define CLKDIV6     0x00500000
#define CLKDIV7     0x00600000
#define CLKDIV8     0x00700000
#define CLKDIV9     0x00800000
#define CLKDIV10    0x00900000
#define CLKDIV11    0x00a00000
#define CLKDIV12    0x00b00000
#define CLKDIV13    0x00c00000
#define CLKDIV14    0x00d00000
#define CLKDIV15    0x00e00000
#define CLKDIV16    0x00f00000
// Bus turnaround phase duration
#define BUSTURN1    0x00000000
#define BUSTURN2    0x00010000
#define BUSTURN3    0x00020000
#define BUSTURN4    0x00030000
#define BUSTURN5    0x00040000
#define BUSTURN6    0x00050000
#define BUSTURN7    0x00060000
#define BUSTURN8    0x00070000
#define BUSTURN9    0x00080000
#define BUSTURN10   0x00090000
#define BUSTURN11   0x000a0000
#define BUSTURN12   0x000b0000
#define BUSTURN13   0x000c0000
#define BUSTURN14   0x000d0000
#define BUSTURN15   0x000e0000
#define BUSTURN16   0x000f0000
// Data-phase duration
#define DATAST_MSK  0x0000ff00
#define DATAST_SFT  8
#define DATAST_SET(n) ((n<<DATAST_SFT)&DATAST_MSK)
#define DATAST_GET(r) ((r>>DATAST_SFT)&0xff)
// Address-hold phase duration
#define ADDHLD2     0x00000010
#define ADDHLD3     0x00000020
#define ADDHLD4     0x00000030
#define ADDHLD5     0x00000040
#define ADDHLD6     0x00000050
#define ADDHLD7     0x00000060
#define ADDHLD8     0x00000070
#define ADDHLD9     0x00000080
#define ADDHLD10    0x00000090
#define ADDHLD11    0x000000a0
#define ADDHLD12    0x000000b0
#define ADDHLD13    0x000000c0
#define ADDHLD14    0x000000d0
#define ADDHLD15    0x000000e0
#define ADDHLD16    0x000000f0
// Address setup phase duration
#define ADDSET1     0x00000000
#define ADDSET2     0x00000001
#define ADDSET3     0x00000002
#define ADDSET4     0x00000003
#define ADDSET5     0x00000004
#define ADDSET6     0x00000005
#define ADDSET7     0x00000006
#define ADDSET8     0x00000007
#define ADDSET9     0x00000008
#define ADDSET10    0x00000009
#define ADDSET11    0x0000000a
#define ADDSET12    0x0000000b
#define ADDSET13    0x0000000c
#define ADDSET14    0x0000000d
#define ADDSET15    0x0000000e
#define ADDSET16    0x0000000f

/* SRAM/NOR-Flash write timing registers 1..4 */
#define FSMC_BWTR1 MMIO32(FMSC_BASE + 0x104)
#define FSMC_BWTR2 MMIO32(FMSC_BASE + 0x10c)
#define FSMC_BWTR3 MMIO32(FMSC_BASE + 0x114)
#define FSMC_BWTR4 MMIO32(FMSC_BASE + 0x11c)
// Access mode
// Bus turnaround phase duration
// Data-phase duration
// Address-hold phase duration
// Address setup phase duration

/* PC Card/NAND Flash control registers 2..4 */
#define FSMC_PCR2 MMIO32(FMSC_BASE + 0x60)
#define FSMC_PCR3 MMIO32(FMSC_BASE + 0x80)
#define FSMC_PCR4 MMIO32(FMSC_BASE + 0xa0)
// ECC page size.
#define ECCPS256    0x00000
#define ECCPS512    0x20000
#define ECCPS1024   0x40000
#define ECCPS2048   0x60000
#define ECCPS4096   0x80000
#define ECCPS8192   0xa0000
// Sets time from ALE low to RE low in number of AHB clock
#define TAR1        0x00000
#define TAR2        0x02000
#define TAR3        0x04000
#define TAR4        0x06000
#define TAR5        0x08000
#define TAR6        0x0a000
#define TAR7        0x0c000
#define TAR8        0x0e000
#define TAR9        0x10000
#define TAR10       0x12000
#define TAR11       0x14000
#define TAR12       0x16000
#define TAR13       0x18000
#define TAR14       0x1a000
#define TAR15       0x1c000
#define TAR16       0x1e000
// time from CLE low to RE low in number of AHB clock
#define TCLR1       0x00000
#define TCLR2       0x00200
#define TCLR3       0x00400
#define TCLR4       0x00600
#define TCLR5       0x00800
#define TCLR6       0x00a00
#define TCLR7       0x00c00
#define TCLR8       0x00e00
#define TCLR9       0x01000
#define TCLR10      0x01200
#define TCLR11      0x01400
#define TCLR12      0x01600
#define TCLR13      0x01800
#define TCLR14      0x01a00
#define TCLR15      0x01c00
#define TCLR16      0x01e00
// ECC computation logic enable bit
#define ECCEN       0x00040
// Databus width.
#define PWID8BIT    0x00000
#define PWID16BIT   0x00010
// Memory type. 1: NAND Flash (default after reset)
#define PTYP        0x00008
// PC Card/NAND Flash memory bank enable bit.
#define PBKEN       0x00004
// Wait feature enable bit.
#define PWAITEN     0x00002

/* FIFO status and interrupt register 2..4 */
#define FSMC_SR2 MMIO32(FMSC_BASE + 0x64)
#define FSMC_SR3 MMIO32(FMSC_BASE + 0x84)
#define FSMC_SR4 MMIO32(FMSC_BASE 0xa4)
// FIFO empty.
#define FEMPT   0x40
// Interrupt falling edge detection enable bit
#define IFEN    0x20
// Interrupt high-level detection enable bit
#define ILEN    0x10
// Interrupt rising edge detection enable bit
#define IREN    0x08
// Interrupt falling edge status
#define IFS     0x04
// Interrupt high-level status
#define ILS     0x02
// Interrupt rising edge status
#define IRS     0x01

/* Common memory space timing register 2..4 */
#define FSMC_PMEM2 MMIO32(FMSC_BASE 0x68)
#define FSMC_PMEM3 MMIO32(FMSC_BASE + 0x88)
#define FSMC_PMEM4 MMIO32(FMSC_BASE + 0xa8)
// Common memory x databus HiZ time
#define MEMHIZ_MSK      0xff000000
#define MEMHIZ_SFT      24
#define MEMHIZ_GET(r)   ((r>>MEMHIZ_SFT)&0xff)
#define MEMHIZ_SET(n)   ((n<<MEMHIZ_SFT)&MEMHIZ_MSK)
// Common memory x hold time
#define MEMHOLD_MSK     0x00ff0000
#define MEMHOLD_SFT     16
#define MEMHOLD_GET(r)  ((r>>MEMHOLD_SFT)&0xff)
#define MEMHOLD_SET(n)  ((n<<MEMHOLD_SFT)&MEMHOLD_MSK)
// Common memory x wait time
#define MEMWAIT_MSK     0x0000ff00
#define MEMWAIT_SFT     8
#define MEMWAIT_GET(r)  ((r>>MEMWAIT_SFT)&0xff)
#define MEMWAIT_SET(n)  ((n<<MEMWAIT_SFT)&MEMWAIT_MSK)
// Common memory x setup time
#define MEMSET_MSK      0x000000ff
#define MEMSET_GET(r)   (r&MEMSET_MSK)
#define MEMSET_SET(n)   (n&MEMSET_MSK)

/* Attribute memory space timing registers 2..4 */
#define FSMC_PATT2 MMIO32(FMSC_BASE + 0x6c)
#define FSMC_PATT3 MMIO32(FMSC_BASE + 0x8c)
#define FSMC_PATT4 MMIO32(FMSC_BASE + 0xac)
// Attribute memory x databus HiZ time
#define ATTHIZ_MSK      0xff000000
#define ATTHIZ_SFT      24
#define ATTHIZ_GET(r)   ((r>>ATTHIZ_SFT)&0xff)
#define ATTHIZ_SET(n)   ((n<<ATTHIZ_SFT)&ATTHIZ_MSK)
// Attribute memory x hold time
#define ATTHOLD_MSK     0x00ff0000
#define ATTHOLD_SFT     16
#define ATTHOLD_GET(r)  ((r>>ATTHOLD_SFT)&0xff)
#define ATTHOLD_SET(n)  ((n<<ATTHOLD_SFT)&ATTHOLD_MSK)
// Attribute memory x wait time
#define ATTWAIT_MSK     0x0000ff00
#define ATTWAIT_SFT     8
#define ATTWAIT_GET(r)  ((r>>ATTWAIT_SFT)&0xff)
#define ATTWAIT_SET(n)  ((n<<ATTWAIT_SFT)&ATTWAIT_MSK)
// Attribute memory x setup time
#define ATTSET_MSK      0x000000ff
#define ATTSET_GET(r)   (r&ATTSET_MSK)
#define ATTSET_SET(n)   (n&ATTSET_MSK)

/* I/O space timing register 4 */
#define FSMC_PIO4 MMIO32(FMSC_BASE + 0xb0)
// I/O x databus HiZ time
#define IOHIZ_MSK       0xff000000
#define IOHIZ_SFT       24
#define IOHIZ_GET(r)    ((r>>IOHIZ_SFT)&0xff)
#define IOHIZ_SET(n)    ((n<<IOHIZ_SFT)&IOHIZ_MSK)
// I/O x hold time
#define IOHOLD_MSK      0x00ff0000
#define IOHOLD_SFT      16
#define IOHOLD_GET(r)   ((r>>IOHOLD_SFT)&0xff)
#define IOHOLD_SET(n)   ((n<<IOHOLD_SFT)&IOHOLD_MSK)
// I/O x wait time
#define IOWAIT_MSK      0x0000ff00
#define IOWAIT_SFT      8
#define IOWAIT_GET(r)   ((r>>IOWAIT_SFT)&0xff)
#define IOWAIT_SET(n)   ((n<<IOWAIT_SFT)&IOWAIT_MSK)
// I/O x setup time
#define IOSET_MSK       0x000000ff
#define IOSET_GET(r)    (r&IOSET_MSK)
#define IOSET_SET(n)    (n&IOSET_MSK)

/* ECC result registers 2/3 */
#define FSMC_ECCR2 MMIO32(FMSC_BASE + 0x74)
#define FSMC_ECCR3 MMIO32(FMSC_BASE + 0x94)
// error correction code value computed by the ECC

#endif
