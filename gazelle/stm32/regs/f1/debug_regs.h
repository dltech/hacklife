#ifndef H_DEBUG_REGS
#define H_DEBUG_REGS
/*
 * Part of Belkin STM32 HAL, advanced debugging features registers of
 * STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* MCU device ID code */
#define DBGMCU_IDCODE   MMIO32(0xe0042000)
// Revision identifier
#define REV_ID_MSK      0xffff0000
#define REV_ID_SFT      16
#define REV_ID_GET(r)   ((r>>REV_ID_SFT)&REV_ID_MSK)
#define REVA_LOW            0x1000
#define REVA_MEDIUM         0x0000
#define REVB_MEDIUM         0x2000
#define REVZ_MEDIUM         0x2001
#define REVX_MEDIUM         0x2003
#define REVA_HIGH           0x1000
#define REVZ_HIGH           0x1001
#define REVX_HIGH           0x1003
#define REVA_XL             0x1000
#define REVA_CONNECTIVITY   0x1000
#define REVZ_CONNECTIVITY   0x1001
// Device identifier
#define DEV_ID_MSK      0x00000fff
#define DEV_ID_GET(r)   (r&DEV_ID_MSK)
#define LOW_DENSITY     0x412
#define MEDIUM_DENSITY  0x410
#define HIGH_DENSITY    0x414
#define XL_DENSITY      0x430
#define CONNECTIVITY    0x418

/* Debug MCU configuration register */
#define DBGMCU_CR   MMIO32(0xe0042004)
// TIMx counter stopped when core is halted (x=9..14)
#define DBG_TIM11_STOP          0x40000000
#define DBG_TIM10_STOP          0x20000000
#define DBG_TIM9_STOP           0x10000000
#define DBG_TIM14_STOP          0x08000000
#define DBG_TIM13_STOP          0x04000000
#define DBG_TIM12_STOP          0x02000000
// Debug CAN2 stopped when core is halted
#define DBG_CAN2_STOP           0x00200000
// TIMx counter stopped when core is halted (x=8..5)
#define DBG_TIM7_STOP           0x00100000
#define DBG_TIM6_STOP           0x00080000
#define DBG_TIM5_STOP           0x00040000
#define DBG_TIM8_STOP           0x00020000
// SMBUS timeout mode stopped when Core is halted
#define DBG_I2C2_SMBUS_TIMEOUT  0x00010000
// SMBUS timeout mode stopped when Core is halted
#define DBG_I2C1_SMBUS_TIMEOUT  0x00008000
// Debug CAN1 stopped when Core is halted
#define DBG_CAN1_STOP           0x00004000
// TIMx counter stopped when core is halted (x=4..1)
#define DBG_TIM4_STOP           0x00002000
#define DBG_TIM3_STOP           0x00001000
#define DBG_TIM2_STOP           0x00000800
#define DBG_TIM1_STOP           0x00000400
// Debug window watchdog stopped when core is halted
#define DBG_WWDG_STOP           0x00000200
// Debug independent watchdog stopped when core is halted
#define DBG_IWDG_STOP           0x00000100
// Trace pin assignment control
#define TRACE_MODE_ASYNC        0x00000000
#define TRACE_MODE_SYNC_SIZE1   0x00000040
#define TRACE_MODE_SYNC_SIZE2   0x00000080
#define TRACE_MODE_SYNC_SIZE4   0x000000c0
#define TRACE_IOEN              0x00000020
// Debug Standby mode
#define DBG_STANDBY             0x00000004
// Debug Stop mode
#define DBG_STOP                0x00000002
// Debug Sleep mode
#define DBG_SLEEP               0x00000001

#endif
