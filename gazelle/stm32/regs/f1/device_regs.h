#ifndef H_DEVICE_REGS
#define H_DEVICE_REGS
/*
 * Part of Belkin STM32 HAL, Device electronic signature of
 * STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* Flash size register */
#define FLASH_SIZE  MMIO32(0x1FFFF7E0)
// value indicates the Flash memory size of the device in Kbytes.

/* Unique device ID register */
#define UID_REG1    MMIO32(0x1FFFF7E8)
// U_ID(15:0): 15:0 unique ID bits
#define UID_REG2    MMIO32(0x1FFFF7EA)
// U_ID(31:16): 31:16 unique ID bits
#define UID_REG3    MMIO32(0x1FFFF7EC)
// U_ID(63:32): 63:32 unique ID bits
#define UID_REG4    MMIO32(0x1FFFF7F0)
// U_ID(95:64): 95:64 Unique ID bits.

#endif
