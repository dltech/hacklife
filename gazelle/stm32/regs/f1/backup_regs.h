#ifndef H_BKP_REGS
#define H_BKP_REGS
/*
 * Part of Belkin STM32 HAL, Backup registers (BKP) of STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* Backup data register x (x = 1 ..42) */
#define BKP_DR1     MMIO32(BKP_BASE + 0x04)
#define BKP_DR2     MMIO32(BKP_BASE + 0x08)
#define BKP_DR3     MMIO32(BKP_BASE + 0x0c)
#define BKP_DR4     MMIO32(BKP_BASE + 0x10)
#define BKP_DR5     MMIO32(BKP_BASE + 0x14)
#define BKP_DR6     MMIO32(BKP_BASE + 0x18)
#define BKP_DR7     MMIO32(BKP_BASE + 0x1c)
#define BKP_DR8     MMIO32(BKP_BASE + 0x20)
#define BKP_DR9     MMIO32(BKP_BASE + 0x24)
#define BKP_DR10    MMIO32(BKP_BASE + 0x28)
#define BKP_DR11    MMIO32(BKP_BASE + 0x40)
#define BKP_DR12    MMIO32(BKP_BASE + 0x44)
#define BKP_DR13    MMIO32(BKP_BASE + 0x48)
#define BKP_DR14    MMIO32(BKP_BASE + 0x4c)
#define BKP_DR15    MMIO32(BKP_BASE + 0x50)
#define BKP_DR16    MMIO32(BKP_BASE + 0x54)
#define BKP_DR17    MMIO32(BKP_BASE + 0x58)
#define BKP_DR18    MMIO32(BKP_BASE + 0x5c)
#define BKP_DR19    MMIO32(BKP_BASE + 0x60)
#define BKP_DR20    MMIO32(BKP_BASE + 0x64)
#define BKP_DR21    MMIO32(BKP_BASE + 0x68)
#define BKP_DR22    MMIO32(BKP_BASE + 0x6c)
#define BKP_DR23    MMIO32(BKP_BASE + 0x70)
#define BKP_DR24    MMIO32(BKP_BASE + 0x74)
#define BKP_DR25    MMIO32(BKP_BASE + 0x78)
#define BKP_DR26    MMIO32(BKP_BASE + 0x7c)
#define BKP_DR27    MMIO32(BKP_BASE + 0x80)
#define BKP_DR28    MMIO32(BKP_BASE + 0x84)
#define BKP_DR29    MMIO32(BKP_BASE + 0x88)
#define BKP_DR30    MMIO32(BKP_BASE + 0x8c)
#define BKP_DR31    MMIO32(BKP_BASE + 0x90)
#define BKP_DR32    MMIO32(BKP_BASE + 0x94)
#define BKP_DR33    MMIO32(BKP_BASE + 0x98)
#define BKP_DR34    MMIO32(BKP_BASE + 0x9c)
#define BKP_DR35    MMIO32(BKP_BASE + 0xa0)
#define BKP_DR36    MMIO32(BKP_BASE + 0xa4)
#define BKP_DR37    MMIO32(BKP_BASE + 0xa8)
#define BKP_DR38    MMIO32(BKP_BASE + 0xac)
#define BKP_DR39    MMIO32(BKP_BASE + 0xb0)
#define BKP_DR40    MMIO32(BKP_BASE + 0xb4)
#define BKP_DR41    MMIO32(BKP_BASE + 0xb8)
#define BKP_DR42    MMIO32(BKP_BASE + 0xbc)
// D[15:0] Backup data

/* RTC clock calibration register */
#define BKP_RTCCR   MMIO32(BKP_BASE + 0x2c)
// Alarm or second output selection 1: RTC Second pulse output sel
#define ASOS    0x200
// Alarm or second output enable set out on TAMPER pin
#define ASOE    0x100
// Calibration clock output, freq div by 64 on the TAMPER pin.
#define CCO     0x080
// Calibration value, pulses will be ignored every 2^20 clock pulses.
#define CAL_MSK 0x07f

/* Backup control register */
#define BKP_CR      MMIO32(BKP_BASE + 0x30)
// TAMPER pin active level 1: 0 on TAMPER resets backup registers
#define TPAL    0x2
// TAMPER pin enable 1: Tamper alternate I/O function is activated.
#define TPE     0x1

/* Backup control/status register */
#define BKP_CSR     MMIO32(BKP_BASE + 0x34)
// Tamper interrupt flag
#define TIF     0x200
// Tamper event flag
#define TEF     0x100
// TAMPER pin interrupt enable
#define TPIE    0x004
// Clear tamper interrupt
#define CTI     0x002
// Clear tamper event
#define CTE     0x001

#endif
