#ifndef H_IWDG_REGS
#define H_IWDG_REGS
/*
 * Part of Belkin STM32 HAL, Independent watchdog (IWDG) registers
 * of STMF103 MCU.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* Key register */
#define IWDG_KR     MMIO32(IWDG_BASE + 0x00)
// Key value (write only, read 0000h)
#define KEY_SLEEP   0xaaaa
#define KEY_ACCESS  0x5555
#define KEY_START   0xcccc

/* Prescaler register */
#define IWDG_PR     MMIO32(IWDG_BASE + 0x04)
// Prescaler divider
#define PR4     0x0
#define PR8     0x1
#define PR16    0x2
#define PR32    0x3
#define PR64    0x4
#define PR128   0x5
#define PR256   0x6
#define PR256   0x7

/* Reload register */
#define IWDG_RLR    MMIO32(IWDG_BASE + 0x08)
// Watchdog counter reload value
#define RL_MSK  0xfff

/* Status register */
#define IWDG_SR     MMIO32(IWDG_BASE + 0x0c)
// Watchdog counter reload value update
#define RVU 0x2
// Watchdog prescaler value update
#define PVU 0x1

#endif
