/*
 * Part of lowlevellib - a heap of microcontroller periferial code.
 * Here is STM32 GPIO API functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gpio.h"

void gpioToDefault(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_FLOATING(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_FLOATING((i-8));
        }
    }
}

void gpioSetInput(uint32_t port, uint32_t pin)
{
    gpioToDefault(port, pin);
}

void gpioSetAnalogue(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_ANALOG(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_ANALOG((i-8));
        }
    }
}

void gpioSetPushPull(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_PUSH_PULL(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_PUSH_PULL((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT50((i-8));
        }
    }
}

void gpioSetOpenDrain(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_OPEN_DRAIN(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_OPEN_DRAIN((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT50((i-8));
        }
    }
}

void gpioSetAlternativeF(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( ((pin & (1<<i)) != 0) && ((GPIOx_CRL(port) & CNF_MASK(i)) == CNF_PUSH_PULL(i)) ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_AF_PUSH_PULL(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        } else if( ((pin & (1<<i)) != 0) && ((GPIOx_CRL(port) & CNF_MASK(i)) == CNF_OPEN_DRAIN(i)) ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_AF_OPEN_DRAIN(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        } else if((pin & (1<<i)) != 0) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_AF_PUSH_PULL(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( ((pin & (1<<i)) != 0) && ((GPIOx_CRH(port) & CNF_MASK((i-8))) == CNF_PUSH_PULL((i-8))) ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_AF_PUSH_PULL((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT50((i-8));
        } else if( ((pin & (1<<i)) != 0) && ((GPIOx_CRH(port) & CNF_MASK((i-8))) == CNF_OPEN_DRAIN((i-8))) ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_AF_OPEN_DRAIN((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT50((i-8));
        } else if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_AF_PUSH_PULL((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT50((i-8));
        }
    }
}

void gpioSetPullUp(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_PUPD(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_PUPD((i-8));
        }
    }
    GPIOx_BSRR(port) = pin;
}

void gpioSetPullDown(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(i) + CNF_MASK(i));
            GPIOx_CRL(port) |= CNF_PUPD(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~(MODE_MASK((i-8)) + CNF_MASK((i-8)));
            GPIOx_CRH(port) |= CNF_PUPD((i-8));
        }
    }
    GPIOx_BRR(port) = pin;
}

void gpioSetOutput2M(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~MODE_MASK(i);
            GPIOx_CRL(port) |= MODE_OUTPUT2(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~MODE_MASK((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT2((i-8));
        }
    }
}

void gpioSetOutput10M(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~MODE_MASK(i);
            GPIOx_CRL(port) |= MODE_OUTPUT10(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~MODE_MASK((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT10((i-8));
        }
    }
}

void gpioSetOutput50M(uint32_t port, uint32_t pin)
{
    for(int i=0 ; i<8 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRL(port) &= ~MODE_MASK(i);
            GPIOx_CRL(port) |= MODE_OUTPUT50(i);
        }
    }
    for(int i=8 ; i<16 ; ++i) {
        if( (pin & (1<<i)) != 0 ) {
            GPIOx_CRH(port) &= ~MODE_MASK((i-8));
            GPIOx_CRH(port) |= MODE_OUTPUT50((i-8));
        }
    }
}

void gpioSet(uint32_t port, uint32_t pin)
{
    GPIOx_BSRR(port) |= pin;
}

void gpioReset(uint32_t port, uint32_t pin)
{
    GPIOx_BRR(port) |= pin;
}

int gpioIsActive(uint32_t port, uint32_t pin)
{
    if( (GPIOx_IDR(port) & pin) == 0 ) {
        return 0;
    } else {
        return 1;
    }
    return 0;
}

uint32_t getPort(uint32_t port)
{
    return GPIOx_IDR(port);
}


void gpionToDefault(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) | CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_FLOATING(pin);
    } else {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) | CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_FLOATING((pin-8));
    }
}

void gpionSetInput(uint32_t port, int pin)
{
    gpionToDefault(port, pin);
}

void gpionSetAnalogue(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_ANALOG(pin);
    } else {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_ANALOG((pin-8));
    }
}

void gpionSetPushPull(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_PUSH_PULL(pin);
        GPIOx_CRL(port) |= MODE_OUTPUT50(pin);
    } else {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_PUSH_PULL((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT50((pin-8));
    }
}

void gpionSetOpenDrain(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_OPEN_DRAIN(pin);
        GPIOx_CRL(port) |= MODE_OUTPUT50(pin);
    } else {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_OPEN_DRAIN((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT50((pin-8));
    }
}

void gpionSetAlternativeF(uint32_t port, int pin)
{
    if( (pin<8) && ((GPIOx_CRL(port) & CNF_MASK(pin)) == CNF_PUSH_PULL(pin)) ) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_AF_PUSH_PULL(pin);
        GPIOx_CRL(port) |= MODE_OUTPUT50(pin);
    } else if( (pin<8) && ((GPIOx_CRL(port) & CNF_MASK(pin)) == CNF_OPEN_DRAIN(pin)) ) {
            GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
            GPIOx_CRL(port) |= CNF_AF_OPEN_DRAIN(pin);
            GPIOx_CRL(port) |= MODE_OUTPUT50(pin);
    } else if(pin<8) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_AF_PUSH_PULL(pin);
        GPIOx_CRL(port) |= MODE_OUTPUT50(pin);
    }

    if( (pin>=8) && ((GPIOx_CRH(port) & CNF_MASK((pin-8))) == CNF_PUSH_PULL((pin-8))) ) {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_AF_PUSH_PULL((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT50((pin-8));
    } else if( (pin>=8) && ((GPIOx_CRH(port) & CNF_MASK((pin-8))) == CNF_OPEN_DRAIN((pin-8))) ) {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_AF_OPEN_DRAIN((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT50((pin-8));
    } else if(pin >= 8) {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_AF_PUSH_PULL((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT50((pin-8));
    }
}

void gpionSetPullUp(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_PUPD(pin);
    } else {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_PUPD((pin-8));
    }
    GPIOx_BSRR(port) = 1<<pin;
}

void gpionSetPullDown(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~(MODE_MASK(pin) + CNF_MASK(pin));
        GPIOx_CRL(port) |= CNF_PUPD(pin);
    } else {
        GPIOx_CRH(port) &= ~(MODE_MASK((pin-8)) + CNF_MASK((pin-8)));
        GPIOx_CRH(port) |= CNF_PUPD((pin-8));
    }
    GPIOx_BRR(port) = 1<<pin;
}

void gpionSetOutput2M(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~MODE_MASK(pin);
        GPIOx_CRL(port) |= MODE_OUTPUT2(pin);
    } else {
        GPIOx_CRH(port) &= ~MODE_MASK((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT2((pin-8));
    }
}

void gpionSetOutput10M(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~MODE_MASK(pin);
        GPIOx_CRL(port) |= MODE_OUTPUT10(pin);
    } else {
        GPIOx_CRH(port) &= ~MODE_MASK((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT10((pin-8));
    }
}

void gpionSetOutput50M(uint32_t port, int pin)
{
    if(pin<8) {
        GPIOx_CRL(port) &= ~MODE_MASK(pin);
        GPIOx_CRL(port) |= MODE_OUTPUT50(pin);
    } else {
        GPIOx_CRH(port) &= ~MODE_MASK((pin-8));
        GPIOx_CRH(port) |= MODE_OUTPUT50((pin-8));
    }
}

void gpionSet(uint32_t port, int pin)
{
    GPIOx_BSRR(port) |= 1<<pin;
}

void gpionReset(uint32_t port, int pin)
{
    GPIOx_BRR(port) |= 1<<pin;
}

int gpionIsActive(uint32_t port, int pin)
{
    if( (GPIOx_IDR(port) & (1<<pin)) == 0 ) {
        return 0;
    } else {
        return 1;
    }
    return 0;
}
