#ifndef H_DELAY
#define H_DELAY

#include "inttypes.h"
#include "STM32F103_CMSIS/stm32f103.h"

//#define NS_PER_TACT 1000000000/SYSTEM_CLOCK
#define NS_PER_TACT 13


void timDelayInit(void);
void timDelayNs(int ns);
void timDelayUs(int us);
void timDelayMs(int ms);

// cyclic delays
void rough_delay_us(uint16_t us);
void delay_ms(uint16_t ms);
void delay_s(uint16_t s);


#endif
