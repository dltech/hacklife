/*
 * Part of USB universal flaser.
 * Serial Wire Debug Interface for DAP access functions.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "swd.h"
#include "../stm32/rcc.h"
#include "../stm32/gpio.h"

int parity(uint32_t word, int num);
void halfTact(void);
void sendSequence(uint32_t data, int size);
uint32_t getSequence(int size);


void swdInit()
{
    // debug led
    enablePeriphClock(IOPA);
    gpioSetPushPull(GPIOA, GPIO1);
    gpioSetOutput50M(GPIOA, GPIO1);
    // timer init
    enablePeriphClock(SWD_TIM);
    TIMx_CR1(SWD_TIM_BASE)   = CKD_CK_INT | CMS_EDGE;
    TIMx_CR2(SWD_TIM_BASE)   = 0;
    TIMx_SMCR(SWD_TIM_BASE)  = 0;
    TIMx_DIER(SWD_TIM_BASE)  = 0;
    TIMx_SR(SWD_TIM_BASE)    = 0;
    TIMx_CCMR1(SWD_TIM_BASE) = 0;
    TIMx_CCMR2(SWD_TIM_BASE) = 0;
    TIMx_CCER(SWD_TIM_BASE)  = 0;
    TIMx_DCR(SWD_TIM_BASE)   = 0;
    TIMx_PSC(SWD_TIM_BASE)   = SWD_TIM_PSC;
    TIMx_ARR(SWD_TIM_BASE)   = 1;
    // gpio_init
    enablePeriphClock(SWD_CLK);
    gpioSetPushPull(SWD_PORT, SWDIO_PIN | SWCLK_PIN);
    gpioSetOutput50M(SWD_PORT, SWDIO_PIN | SWCLK_PIN);
    // start
    TIMx_CR1(SWD_TIM_BASE) |= CEN;
    TIMx_ARR(SWD_TIM_BASE) |= UG;
}

uint8_t dresp = 0;
uint8_t swdWrite(uint8_t addr, uint8_t apndp, uint32_t wdata)
{
    uint8_t response = 0;
    uint8_t ack = 0;
    int parityy = 0;
    // set accessed register
    response = apndp | A_SET(addr);
    // calculate the parity bit
    if(parity((response>>1),4)) {
        response |= PARITY;
    }
    // adding start and park bits
    response |= START | PARK;
    dresp = response;
    parityy = parity(wdata,32);
    // transaction cycle
    gpionSetPushPull(SWD_PORT, SWDIO_PINN);
    sendSequence(response,8);
    // park and turaround bit
    gpionSetPullUp(SWD_PORT, SWDIO_PINN);
    getSequence(1);
    // receiving response
    ack = (uint8_t)getSequence(3);
    // skip if not ok
    if(ack != ACK_OK) {
        getSequence(1);
        return ack;
    }
    // turnaround bit
    getSequence(1);
    gpionSetPushPull(SWD_PORT, SWDIO_PINN);
    // writing register
    sendSequence(wdata,32);
    // and it's parity bit
    sendSequence((uint32_t)parityy, 1);
    return ack;
}

uint8_t ddack, dres;

uint32_t swdRead(uint8_t addr, uint8_t apndp, uint8_t *ack)
{
    uint8_t response = 0;
    uint32_t rdata;
    // set accessed register
    response = apndp | RNW | A_SET(addr);
    // calculate the parity bit
    if(parity((response>>1),4)) {
        response |= PARITY;
    }
    // adding start and park bits
    response |= START | PARK;
    dres = response;
    // transaction cycle
    gpionSetPushPull(SWD_PORT, SWDIO_PINN);
    sendSequence(response,8);
    // turaround bit
    gpionSetPullUp(SWD_PORT, SWDIO_PINN);
    getSequence(1);
    // receiving response
    *ack = (uint8_t)getSequence(3);
    ddack = *ack;
    // skip if not ok
    if((*ack) != ACK_OK) {
        getSequence(1);
        return (uint32_t)(*ack);
    }
    // reading register
    rdata = getSequence(32);
    // checking parity
    if( getSequence(1) != (uint32_t)parity(rdata,32) ) {
        *ack = 0xa;//ACK_ERR;
        return 0;
    }
    getSequence(1);
    // if equal, return readed data
    return rdata;
}

void swdResetSequence()
{
    gpionSetPushPull(SWD_PORT, SWDIO_PINN);
    sendSequence(0xffffffff,32);
    sendSequence(0xffffffff,32);
}

void swdSwitchSequence()
{
    swdResetSequence();
    sendSequence(TO_SWD, 16);
    swdResetSequence();
    sendSequence(0x0000, 16);
}

void swdFinalize()
{
    gpionSetPushPull(SWD_PORT, SWDIO_PINN);
    gpionSetOutput50M(SWD_PORT, SWDIO_PINN);
    sendSequence(0xffffffff,16);
}

void halfTact()
{
    uint32_t tOut = 1e6;
    TIMx_ARR(SWD_TIM_BASE) |= UG;
    TIMx_SR(SWD_TIM_BASE)    = 0;
    while( ((TIMx_SR(SWD_TIM_BASE) & UIF) == 0) && ((--tOut)>0) );
}

void sendSequence(uint32_t data, int size)
{
    for(int i=0 ; i<size ; ++i) {
        GPIOx_BRR(SWD_PORT) |= SWCLK_PIN;
        if((data&(1<<i)) > 0) {
            GPIOx_BSRR(SWD_PORT) |= SWDIO_PIN;
        } else {
            GPIOx_BRR(SWD_PORT) |= SWDIO_PIN;
        }
        halfTact();
        GPIOx_BSRR(SWD_PORT) |= SWCLK_PIN;
        halfTact();
    }
}

uint32_t getSequence(int size)
{
    uint32_t read = 0;
    for(int i=0 ; i<size ; ++i) {
        GPIOx_BRR(SWD_PORT) |= SWCLK_PIN;
        halfTact();
        if((GPIOx_IDR(SWD_PORT) & SWDIO_PIN) > 0) {
            read |= 1<<i;
        }
        GPIOx_BSRR(SWD_PORT) |= SWCLK_PIN;
        halfTact();
    }
    return read;
}

int parity(uint32_t word, int num)
{
    int ones=0;
    for(int i=0 ; i<num ; ++i)
    {
        if( (word&(1<<i)) > 0 ) {
            ++ones;
        }
    }
    if((ones%2) == 0) {
        return 0;
    }
    return 1;
}

