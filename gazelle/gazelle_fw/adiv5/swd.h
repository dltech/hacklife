#ifndef SWD_H
#define SWD_H
/*
 * Part of USB universal flaser.
 * Serial Wire Debug Interface for DAP access functions.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../stm32/regs/f1/tim_regs.h"
#include "../stm32/STM32F103_CMSIS/stm32f103.h"
#include "../stm32/gpio.h"

#define SWD_FREQ_KHZ    1000

#define SWD_CLK         IOPB
#define SWD_PORT        GPIOB
#define SWDIO_PIN       GPIO8
#define SWCLK_PIN       GPIO9
#define SWDIO_PINN      8
#define SWCLK_PINN      9

#define SWD_TIM_BASE    TIM2_BASE
#define SWD_TIM         TIM2
#define SWD_TIM_PSC     ((SYSTEM_CLOCK/SWD_FREQ_KHZ)/4000)

/* request frame format (LSB-first) */
// start bit (1)
#define START       0x01
// Debug port or Acess port select bit
#define APNDP       0x02
// read/write bit, 0 is write
#define RNW         0x04
// register address
#define A_MSK       0x18
#define A_SFT       3
#define A_SET(x)    ((x<<A_SFT)&A_MSK)
// parity bit
#define PARITY      0x20
// stop bit (0)
#define STOP        0x40
// turnaround (always 1)
#define PARK        0x80

// macro for chooosing the register bank
#define APACC       APNDP
#define DPACC       0x00

// Acknowledge field variants (LSB-first)
#define ACK_OK      0x1
#define ACK_WAIT    0x2
#define ACK_FAULT   0x4
#define ACK_ERR     0x7
#define ACK_MSK     0x7

// jtag to swd sequence
#define TO_SWD      0xe79e
//#define TO_SWD      0x79e7

void swdInit(void);
void swdSwitchSequence(void);
uint8_t swdWrite(uint8_t addr, uint8_t apndp, uint32_t wdata);
uint32_t swdRead(uint8_t addr, uint8_t apndp, uint8_t *ack);
void swdResetSequence(void);
void swdFinalize(void);

#endif
