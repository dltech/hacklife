/*
 * Part of USB universal flaser.
 * ARMv7M memory mapped registers access through the SWD interface.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "dap.h"
//#include "swd.h"
#include "memory_mapped.h"

volatile uint8_t increment = ADDR_INC_OFF;

uint32_t memReadSingle(uint32_t addr)
{
    uint8_t ack = 0;
    uint32_t reg;
    if(increment != ADDR_INC_OFF) {
        writeDap(CSW, DEFAULT_CSW | ADDR_INC_OFF);
        increment = ADDR_INC_OFF;
    }
    writeDap(TAR, addr);
    readDap(DRW, &ack);
    reg = readDap(DRW, &ack);
    if( ack != ACK_OK ) {
        dapReset();
        return ACK_ERR;
    }
    return reg;
}

uint8_t memReadSeries(uint32_t addr, uint32_t *regs, int size)
{
    uint8_t ack = 0;
    if(increment != ADDR_INC_SINGLE) {
        writeDap(CSW, DEFAULT_CSW | ADDR_INC_SINGLE);
        increment = ADDR_INC_SINGLE;
    }
    writeDap(TAR, addr);
    readDap(DRW, &ack);
    for(int i=0 ; i<size ; ++i) {
        regs[i] = readDap(DRW, &ack);
    }
    return ack;
}

uint8_t memWriteSingle(uint32_t addr, uint32_t reg)
{
    uint8_t ack = 0;
    if(increment != ADDR_INC_OFF) {
        writeDap(CSW, DEFAULT_CSW | ADDR_INC_OFF);
        increment = ADDR_INC_OFF;
    }
    writeDap(TAR, addr);
    ack = writeDap(DRW, reg);
    return ack;
}

uint8_t memWriteSeries(uint32_t addr, uint32_t *regs, int size)
{
    uint8_t ack = 0;
    if(increment != ADDR_INC_SINGLE) {
        writeDap(CSW, DEFAULT_CSW | ADDR_INC_SINGLE);
        increment = ADDR_INC_SINGLE;
    }
    writeDap(TAR, addr);
    for(int i=0 ; i<size ; ++i) {
        ack = writeDap(DRW, regs[i]);
    }
    return ack;
}

uint32_t memVerify(uint32_t addr, uint32_t *regs, int size)
{
    uint8_t ack = 0;
    int tOut = 100;
    if(increment != ADDR_INC_SINGLE) {
        writeDap(CSW, DEFAULT_CSW | ADDR_INC_SINGLE);
        increment = ADDR_INC_SINGLE;
    }
    writeDap(CTRL_STAT, DEFAULT_CTRL_STAT | TRNMODE_PUSH_VERIFY);
    if( writeDap(TAR, addr) != ACK_OK ) {
        dapReset();
        return ACK_ERR;
    }
    for(int i=0 ; i<size ; ++i) {
        tOut = 100;
        while(((ack = writeDap(DRW, regs[i])) == ACK_WAIT) && (--tOut>0));
        if(tOut < 2) {
            dapReset();
            return ACK_ERR;
        }
        if( ack == ACK_FAULT ) {
            if( readDap(CTRL_STAT, &ack) & STICKYCMP ) {
                return addr + (uint32_t)(TRNCNT_GET(readDap(CTRL_STAT, &ack)));
            }
        }
    }
    return 0;
}
