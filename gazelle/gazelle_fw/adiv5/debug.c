/*
 * Part of USB universal flaser.
 * Base level ARMv7 debug functions.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dap.h"
#include "memory_mapped.h"
#include "regs/system_control_reg.h"
#include "regs/debug_sys_reg.h"
#include "debug.h"


int resetAndHalt()
{
    uint32_t status = 0;
    int tOut = 1e3;
    memWriteSingle(DHCSR, DBGKEY | C_DEBUGEN);
    memWriteSingle(DEMCR, VC_CORERESET);
    memWriteSingle(AIRCR, VECTKEY | SYSRESETREQ);
    while(((status = memReadSingle(DHCSR) & S_HALT) == 0) && (--tOut>0));
    if( (tOut < 2) || (status == ACK_ERR) ) {
        return -1;
    }
    return 0;
}

int resetAndRun()
{
    uint8_t ack = 0;
    memWriteSingle(DHCSR, DBGKEY);
    ack = memWriteSingle(AIRCR, VECTKEY | SYSRESETREQ);
    if( ack != ACK_OK ) {
        return -1;
    }
    return 0;
}
