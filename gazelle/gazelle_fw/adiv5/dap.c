/*
 * Part of USB universal flaser.
 * Functions for interacting with debug access port (DAP).
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "dap.h"
#include "delay.h"

volatile uint8_t apbanksel = 0;

uint32_t dapBegin()
{
    uint32_t idcode;
    uint8_t ack;
    swdInit();
    swdSwitchSequence();
    idcode = readDap(IDCODE, &ack);
    // write settings of debugger
    writeDap(SELECT, APBANKSEL_SET(0));
    writeDap(CTRL_STAT, DEFAULT_CTRL_STAT);
    writeDap(CSW, DEFAULT_CSW);
    writeDap(WCR, DEFAULT_WCR);
    return idcode;
}

uint32_t dapReset() {
    uint32_t idcode;
    uint8_t ack;
    swdSwitchSequence();
    readDap(IDCODE, &ack);
    writeDap(ABORT, DAPABORT);
    idcode = readDap(IDCODE, &ack);
    writeDap(SELECT, APBANKSEL_SET(0));
    writeDap(CTRL_STAT, DEFAULT_CTRL_STAT);
    writeDap(CSW, DEFAULT_CSW);
    writeDap(WCR, DEFAULT_WCR);
    return idcode;
}

uint8_t writeDap(uint8_t reg, uint32_t data)
{
    int tOut = MEMAP_RETRY_COUNT;
    uint8_t ack = ACK_WAIT;
    switch(reg) {
        case ABORT:
            ack = swdWrite(ABORT_AP, ABORT_APNDP, data);
            break;
        case IDCODE:
            break;
        case CTRL_STAT:
            ack = swdWrite(CTRL_STAT_AP, CTRL_STAT_APNDP, data);
            break;
        case WCR:
            while((ack == ACK_WAIT) && (--tOut > 0)) {
                ack = swdWrite(SELECT_AP, SELECT_APNDP, WCR_SEL | apbanksel);
            }
            if(ack == ACK_OK) {
                swdWrite(WCR_AP, WCR_APNDP, data);
                swdWrite(SELECT_AP, SELECT_APNDP, apbanksel);
            }
            break;
        case SELECT:
            ack = swdWrite(SELECT_AP, SELECT_APNDP, data);
            if(ack == ACK_OK) {
                apbanksel = data & APBANKSEL_MSK;
            }
            break;
        case RESEND:
            break;
        case RDBUFF:
            break;
        case CSW:
            if(apbanksel == APBANKSEL_SET(CSW_BANK)) {
                ack = swdWrite(CSW_AP, CSW_APNDP, data);
            } else if((ack = swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(CSW_BANK))) == ACK_OK) {
                apbanksel = APBANKSEL_SET(CSW_BANK);
                ack = swdWrite(CSW_AP, CSW_APNDP, data);
            }
            break;
        case TAR:
            if(apbanksel == APBANKSEL_SET(TAR_BANK)) {
                ack = swdWrite(TAR_AP, TAR_APNDP, data);
            } else if((ack = swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(TAR_BANK))) == ACK_OK) {
                apbanksel = APBANKSEL_SET(TAR_BANK);
                ack = swdWrite(TAR_AP, TAR_APNDP, data);
            }
            break;
        case DRW:
            if(apbanksel == APBANKSEL_SET(DRW_BANK)) {
                ack = swdWrite(DRW_AP, DRW_APNDP, data);
            } else if((ack = swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(DRW_BANK))) == ACK_OK) {
                apbanksel = APBANKSEL_SET(DRW_BANK);
                ack = swdWrite(DRW_AP, DRW_APNDP, data);
            }
            break;
        case BD0:
            if(apbanksel == APBANKSEL_SET(BD0_BANK)) {
                ack = swdWrite(BD0_AP, BD0_APNDP, data);
            } else if((ack = swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD0_BANK))) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD0_BANK);
                ack = swdWrite(BD0_AP, BD0_APNDP, data);
            }
            break;
        case BD1:
            if(apbanksel == APBANKSEL_SET(BD1_BANK)) {
                ack = swdWrite(BD1_AP, BD1_APNDP, data);
            } else if((ack = swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD1_BANK))) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD1_BANK);
                ack = swdWrite(BD1_AP, BD1_APNDP, data);
            }
            break;
        case BD2:
            if(apbanksel == APBANKSEL_SET(BD2_BANK)) {
                ack = swdWrite(BD2_AP, BD2_APNDP, data);
            } else if((ack = swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD2_BANK))) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD2_BANK);
                ack = swdWrite(BD2_AP, BD2_APNDP, data);
            }
            break;
        case BD3:
            if(apbanksel == APBANKSEL_SET(BD3_BANK)) {
                ack = swdWrite(BD3_AP, BD3_APNDP, data);
            } else if((ack = swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD3_BANK))) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD3_BANK);
                ack = swdWrite(BD3_AP, BD3_APNDP, data);
            }
            break;
        case CFG:
            break;
        case BASE:
            break;
        case IDR:
            break;
    }
    return ack;
}

uint32_t readDap(uint8_t reg, uint8_t *ack)
{
    uint32_t data = 0;
    *ack = ACK_WAIT;
    int tOut = MEMAP_RETRY_COUNT;
    uint8_t apndp = 0;
    uint8_t ap = 0;
    switch(reg) {
        case ABORT:
            return 0;
            break;
        case IDCODE:
            data = swdRead(IDCODE_AP, IDCODE_APNDP, ack);
            break;
        case CTRL_STAT:
            data = swdRead(CTRL_STAT_AP, CTRL_STAT_APNDP, ack);
            break;
        case WCR:
            if(swdWrite(SELECT_AP, SELECT_APNDP, WCR_SEL | apbanksel) == ACK_OK) {
                data = swdRead(WCR_AP, WCR_APNDP, ack);
                swdWrite(SELECT_AP, SELECT_APNDP, apbanksel);
            }
            break;
        case SELECT:
            break;
        case RESEND:
            data = swdRead(RESEND_AP, RESEND_APNDP, ack);
            break;
        case RDBUFF:
            data = swdRead(RDBUFF_AP, RDBUFF_APNDP, ack);
            break;
        case CSW:
            if(apbanksel == APBANKSEL_SET(CSW_BANK)) {
                apndp = CSW_APNDP;
                ap = CSW_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(CSW_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(CSW_BANK);
                apndp = CSW_APNDP;
                ap = CSW_AP;
            }
            break;
        case TAR:
            if(apbanksel == APBANKSEL_SET(TAR_BANK)) {
                apndp = TAR_APNDP;
                ap = TAR_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(TAR_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(TAR_BANK);
                apndp = TAR_APNDP;
                ap = TAR_AP;
            }
            break;
        case DRW:
            if(apbanksel == APBANKSEL_SET(DRW_BANK)) {
                apndp = DRW_APNDP;
                ap = DRW_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(DRW_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(DRW_BANK);
                apndp = DRW_APNDP;
                ap = DRW_AP;
            }
            break;
        case BD0:
            if(apbanksel == APBANKSEL_SET(BD0_BANK)) {
                apndp = BD0_APNDP;
                ap = BD0_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD0_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD0_BANK);
                apndp = BD0_APNDP;
                ap = BD0_AP;
            }
            break;
        case BD1:
            if(apbanksel == APBANKSEL_SET(BD1_BANK)) {
                apndp = BD1_APNDP;
                ap = BD1_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD1_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD1_BANK);
                apndp = BD1_APNDP;
                ap = BD1_AP;
            }
            break;
        case BD2:
            if(apbanksel == APBANKSEL_SET(BD2_BANK)) {
                apndp = BD2_APNDP;
                ap = BD2_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD2_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD2_BANK);
                apndp = BD2_APNDP;
                ap = BD2_AP;
            }
            break;
        case BD3:
            if(apbanksel == APBANKSEL_SET(BD3_BANK)) {
                apndp = BD3_APNDP;
                ap = BD3_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BD3_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BD3_BANK);
                apndp = BD3_APNDP;
                ap = BD3_AP;
            }
            break;
        case CFG:
            if(apbanksel == APBANKSEL_SET(CFG_BANK)) {
                apndp = CFG_APNDP;
                ap = CFG_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(CFG_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(CFG_BANK);
                apndp = CFG_APNDP;
                ap = CFG_AP;
            }
            break;
        case BASE:
            if(apbanksel == APBANKSEL_SET(BASE_BANK)) {
                apndp = BASE_APNDP;
                ap = BASE_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(BASE_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(BASE_BANK);
                apndp = BASE_APNDP;
                ap = BASE_AP;
            }
            break;
        case IDR:
            if(apbanksel == APBANKSEL_SET(IDR_BANK)) {
                apndp = IDR_APNDP;
                ap = IDR_AP;
            } else if(swdWrite(SELECT_AP, SELECT_APNDP, APBANKSEL_SET(IDR_BANK)) == ACK_OK) {
                apbanksel = APBANKSEL_SET(IDR_BANK);
                apndp = IDR_APNDP;
                ap = IDR_AP;
            }
            break;
    }
    // debug port registers read without retry
    if(reg<CSW) {
        return data;
    }
    // access port registers reading will be retried until success
    while((*ack == ACK_WAIT) && (--tOut > 0)) {
        data = swdRead(ap, apndp, ack);
    }
    // data read register reads only once for series access
    if(reg == DRW) {
        return data;
    }
    // yet another read for pushing data into buffer
    data = swdRead(ap, apndp, ack);
    return data;
}
