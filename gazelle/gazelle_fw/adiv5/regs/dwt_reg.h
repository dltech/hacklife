#ifndef DWT_REG_H
#define DWT_REG_H
/*
 * Part of USB universal flaser.
 * The Data Watchpoint and Trace unit registers for SWD ARMv7M debug support
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Control register */
#define DWT_CTRL        0xe0001000
// Number of comparators implemented.
#define NUMCOMP_MSK     0xf0000000
#define NUMCOMP_SFT     28
#define NUMCOMP_GET(r)  ((r>>NUMCOMP_SFT)&0xf)
// Shows whether the impl supports trace sampling and except tracing
#define NOTRCPKT            0x08000000
// implementation includes external match signals
#define NOEXTTRIG           0x04000000
// Shows whether the implementation supports a cycle counter
#define NOCYCCNT            0x02000000
// Shows whether the implementation supports the profiling counters
#define NOPRFCNT            0x01000000
// Enables POSTCNT underflow Event counter packets generation
#define CYCEVTENA           0x00400000
// Enables generation of the Folded-instruction counter overflow event
#define FOLDEVTENA          0x00200000
// Enables generation of the LSU counter overflow event.
#define LSUEVTENA           0x00100000
// Enables generation of the Sleep counter overflow event.
#define SLEEPEVTENA         0x00080000
// Enables generation of the Exception overhead counter overflow event:
#define EXCEVTENA           0x00040000
// Enables generation of the CPI counter overflow event
#define CPIEVTENA           0x00020000
// Enables generation of exception trace
#define EXCTRCENA           0x00010000
// Enables use of POSTCNT counter as a timer for Periodic PC sample pack gen
#define PCSAMPLENA          0x00001000
// Selects the position of the synchronization packet counter tap on the CYCCNT
#define SYNCTAP_DIS         0x00000000
#define SYNCTAP_AT24        0x00000400
#define SYNCTAP_AT26        0x00000800
#define SYNCTAP_AT28        0x00000c00
// Selects the position of the POSTCNT tap on the CYCCNT counter
#define CYCTAP              0x00000200
// Initial value for the POSTCNT counter.
#define POSTINIT_MSK        0x000001e0
#define POSTINIT_SFT        5
#define POSTINIT_SET(n)     ((n<<POSTINIT_SFT)&POSTINIT_MSK)
#define POSTINIT_GET(r)     ((r>>POSTINIT_SFT)&0xf)
// Reload value for the POSTCNT counter.
#define POSTPRESET_MSK      0x0000001e
#define POSTPRESET_SFT      1
#define POSTPRESET_SET(n)   ((n<<POSTPRESET_SFT)&POSTPRESET_MSK)
#define POSTPRESET_GET(r)   ((r>>POSTPRESET_SFT)&0xf)
// Enables CYCCNT
#define CYCCNTENA           0x00000001

/* Cycle Count register */
#define DWT_CYCCNT      0xe0001004
// Incrementing cycle counter value.

/* CPI Count register */
#define DWT_CPICNT      0xe0001008
// Base instruction overhead counter.
#define CPICNT_MSK  0xff

/* Exception Overhead Count register */
#define DWT_EXCCNT      0xe000100c
// The exception overhead counter.
#define EXCCNT_MSK  0xff

/* Sleep Count register */
#define DWT_SLEEPCNT    0xe0001010
// Sleep counter
#define SLEEPCNT_MSK    0xff

/* LSU Count register */
#define DWT_LSUCNT      0xe0001014
// Load-store overhead counter.
#define LSUCNT_MSK  0xff

/* Folded-instruction Count register */
#define DWT_FOLDCNT     0xe0001018
// Folded instruction counter.
#define FOLDCNT_MSK 0xff

/* Program Counter Sample Register */
#define DWT_PCSR        0xe000101c
// Executed Instruction Address sample value.

/* Comparator registers */
#define DWT_COMP(n)     (0xe0001020 + n*16)
// Reference value for comparison.

/* Comparator Mask registers */
#define DWT_MASK(n)     (0xe0001024 + n*16)
// The size of the ignore mask

/* Comparator Function registers */
#define DWT_FUNCTION(n) (0xe0001028 + n*16)
// Comparator match
#define MATCHED                                         0x01000000
// comparator n of a second comparator to use for linked address comparison
#define DATAVADDR1_MSK                                  0x000f0000
#define DATAVADDR1_SFT                                  16
#define DATAVADDR1_GET(r)   ((r>>DATAVADDR1_SFT)&0xf)
#define DATAVADDR1_SET(n)   ((n<<DATAVADDR1_SFT)&DATAVADDR1_MSK)
// comparator number of a comparator to use for linked address comparison
#define DATAVADDR0_MSK                                  0x0000f000
#define DATAVADDR0_SFT                                  12
#define DATAVADDR0_GET(r)   ((r>>DATAVADDR0_SFT)&0xf)
#define DATAVADDR0_SET(n)   ((n<<DATAVADDR0_SFT)&DATAVADDR0_MSK)
// For data value matching, specifies the size of the required data comparison
#define DATAVSIZE_BYTE                                  0x00000000
#define DATAVSIZE_HALHWORD                              0x00000400
#define DATAVSIZE_WORD                                  0x00000800
// whether the implementation supports use of a second linked comparator
#define LNK1ENA                                         0x00000200
// Enables data value comparison, if supported
#define DATAVMATCH                                      0x00000100
// enable cycle count comparison for comparator 0
#define CYCMATCH                                        0x00000080
// enables generation of Data trace address packets, that hold Daddr[15:0]
#define EMITRANGE                                       0x00000020
// Selects action taken on comparator match
#define FUNCTION_DISABLED                               0x0
#define FUNCTION_DATAVMATCH_R_WATCHPOINT                0x5
#define FUNCTION_DATAVMATCH_W_WATCHPOINT                0x6
#define FUNCTION_DATAVMATCH_RW_WATCHPOINT               0x7
#define FUNCTION_DATAVMATCH_R_CMPMATCH                  0x9
#define FUNCTION_DATAVMATCH_W_CMPMATCH                  0xa
#define FUNCTION_DATAVMATCH_RW_CMPMATCH                 0xb
#define FUNCTION_DADDR_RW_PC_TRACE                      0x1
#define FUNCTION_EMITRANGE_DADDR_RW_ADDRESS_TRACE       0x1
#define FUNCTION_DADDR_RW_DATA_TRACE                    0x2
#define FUNCTION_EMITRANGE_DADDR_RW_ADDRESS_DATA_TRACE  0x2
#define FUNCTION_DADDR_RW_PC_DATA_TRACE                 0x3
#define FUNCTION_EMITRANGE_DADDR_RW_ADDRESS_DATA_TRACE  0x3
#define FUNCTION_IADDR_WATCHPOINT                       0x4
#define FUNCTION_DADDR_R_WATCHPOINT                     0x5
#define FUNCTION_DADDR_W_WATCHPOINT                     0x6
#define FUNCTION_DADDR_RW_WATCHPOINT                    0x7
#define FUNCTION_IADDR_CMPMATCH                         0x8
#define FUNCTION_DADDR_R_CMPMATCH                       0x9
#define FUNCTION_DADDR_W_CMPMATCH                       0xa
#define FUNCTION_DADDR_RW_CMPMATCH                      0xb
#define FUNCTION_DADDR_R_DATA_TRACE                     0xc
#define FUNCTION_EMITRANGE_DADDR_R_ADDRESS_TRACE        0xc
#define FUNCTION_DADDR_W_DATA_TRACE                     0xd
#define FUNCTION_EMITRANGE_DADDR_W_ADDRESS_TRACE        0xd
#define FUNCTION_DADDR_R_PC_DATA_TRACE                  0xe
#define FUNCTION_EMITRANGE_DADDR_R_ADDRESS_DATA_TRACE   0xe
#define FUNCTION_DADDR_W_PC_DATA_TRACE                  0xf
#define FUNCTION_EMITRANGE_DADDR_W_ADDRESS_DATA_TRACE   0xf
#define FUNCTION_CYCMATCH_PC_TRACE                      0x1
#define FUNCTION_CYCMATCH_WATCHPOINT                    0x4
#define FUNCTION_CYCMATCH_CMPMATCH                      0x8

#endif
