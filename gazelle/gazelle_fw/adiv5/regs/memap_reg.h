#ifndef MEM_AP_REG_H
#define MEM_AP_REG_H
/*
 * Part of USB universal flaser.
 * Memory Access Port (MEM-AP) registers for SWD ARMv7M debug support
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Control/Status Word (CSW) Register */
#define CSW_AP     0x00
#define CSW_BANK   0x00
#define CSW_APNDP  0x02
// Debug software access enable.
#define DBG_SW_ENABLE   0x80000000
//  0 = core. 1 = debug.
#define MASTER_TYPE     0x20000000
// User and Privilege control - HPROT[1].
#define HPROT1          0x02000000
// reserved, must be 1
#define CSW_RESERVED1   0x01000000
// Secure Privileged Debug Enabled.
#define SPIDEN          0x00800000
// Transfer in progress.
#define TR_IN_PROG      0x00000080
// Device enabled. When transactions can be issued through the MEM-AP.
#define DEVICE_EN       0x00000040
// Address auto-increment and packing mode.
#define ADDR_INC_OFF    0x00000000
#define ADDR_INC_SINGLE 0x00000010
#define ADDR_INC_PACK   0x00000020
// Size of the access to perform.
#define SIZE_MSK        0x00000007
#define SIZE_GET(r)     (r&SIZE_MSK)
#define SIZE_SET(n)     (n&SIZE_MSK)
#define SIZE_8BIT       0x00000000
#define SIZE_16BIT      0x00000001
#define SIZE_32BIT      0x00000002

/* The Transfer Address Register (TAR) */
#define TAR_AP      0x01
#define TAR_BANK    0x00
#define TAR_APNDP   0x02
// Memory address for the current transfer.

/* The Data Read/Write Register (DRW) */
#define DRW_AP      0x03
#define DRW_BANK    0x00
#define DRW_APNDP   0x02
// Data value of the current transfer.

/* Banked Data Registers 0 to 3 (BD0 to BD3) */
#define BD0_AP      0x00
#define BD0_BANK    0x01
#define BD0_APNDP   0x02
#define BD1_AP      0x01
#define BD1_BANK    0x01
#define BD1_APNDP   0x02
#define BD2_AP      0x02
#define BD2_BANK    0x01
#define BD2_APNDP   0x02
#define BD3_AP      0x03
#define BD3_BANK    0x01
#define BD3_APNDP   0x02
// Data value of the current transfer.

/* Configuration Register (CFG) */
#define CFG_AP      0x01
#define CFG_BANK    0x0f
#define CFG_APNDP   0x02
// whether memory accesses by the MEM-AP are big-endian or little-endian:
#define BIG_ENDIAN  0x1

/* Debug Base Address Register (BASE) */
#define BASE_AP     0x02
#define BASE_BANK   0x0f
#define BASE_APNDP  0x02
// Bits [31:12] of the address offset, in the memory-mapped resource
#define BASEADDR_MSK    0xfffff000
#define BASEADDR_GET(r) (r&BASEADDR_MSK)
// Base address register format.
#define FORMAT          0x00000002
// whether a debug entry is present for this MEM-AP
#define ENTRY_PRESENT   0x00000001

/* The Identification Register, IDR */
#define IDR_AP      0x03
#define IDR_BANK    0x0f
#define IDR_APNDP   0x02
// increments by 1 on each major or minor revision of the design
#define REVISION_MSK                0xf0000000
#define REVISION_SFT                28
#define REVISION_GET(r)             ((r>>REVISION_SFT)&0xf)
// JEP-106 codes, used to identify the designer of the AP
#define JEP106_CONTINUATION_MSK     0x0f000000
#define JEP106_CONTINUATION_SFT     24
#define JEP106_CONTINUATION_GET(r)  ((r>JEP106_CONTINUATION_SFT)&0xf)
#define JEP106_IDENTITY_MSK         0x00fe0000
#define JEP106_IDENTITY_SFT         17
#define JEP106_IDENTITY_GET(r)      ((r>>JEP106_IDENTITY_SFT)&0x7f)
// 1: This AP is a Memory Access Port.
#define CLASS                       0x00010000
// This field identifies the AP implementation.
#define AP_ID_VARIANT_MSK           0x000000f0
#define AP_ID_VARIANT_SFT           4
#define AP_ID_VARIANT_GET(r)        ((r>>AP_ID_VARIANT_SFT)&0xf)
#define AP_ID_TYPE_MSK              0x0000000f
#define AP_ID_TYPE_GET(r)           (r&AP_ID_TYPE_MSK)

#endif
