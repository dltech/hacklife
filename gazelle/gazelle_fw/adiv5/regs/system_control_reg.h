#ifndef SYSTEM_CONTROL_REG_H
#define SYSTEM_CONTROL_REG_H
/*
 * Part of USB universal flaser.
 * AIRCR ARMv7M register for reset perform.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Application Interrupt and Reset Control Register */
#define AIRCR   0xe000ed0c
// writes must write 0x05FA to this field, otherwise the write is ignored.
#define VECTKEY             0x05fa0000
#define VECTKEYSTAT         0xfa05
#define VECTKEYSTAT_GET(r)  ((r>>16)&0xffff)
// Indicates the memory system endianness, 1 - Big endian.
#define ENDIANNESS          0x00008000
// Priority grouping, indicates the binary point position.
#define PRIGROUP_MSK        0x00000700
#define PRIGROUP_SFT        8
// System Reset Request
#define SYSRESETREQ         0x00000004
// clears all active state information for fixed and configurable exceptions
#define VECTCLRACTIVE       0x00000002
// a local system reset
#define VECTRESET           0x00000001

#endif
