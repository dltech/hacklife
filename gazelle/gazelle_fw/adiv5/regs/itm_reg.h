#ifndef ITM_REG_H
#define ITM_REG_H
/*
 * Part of USB universal flaser.
 * The Instrumentation Trace Macrocell module registers
 * for SWD ARMv7M debug support
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Stimulus Port register n, 0-255 */
#define ITM_STIM0   0xe0000000
#define ITM_STIM1   0xe0000004
#define ITM_STIM2   0xe0000008
#define ITM_STIM3   0xe000000c
#define ITM_STIM4   0xe0000010
#define ITM_STIM5   0xe0000014
#define ITM_STIM6   0xe0000018
#define ITM_STIM7   0xe000001c
#define ITM_STIM8   0xe0000020
#define ITM_STIM9   0xe0000024
#define ITM_STIM10  0xe0000028
#define ITM_STIM11  0xe000002c
#define ITM_STIM12  0xe0000030
#define ITM_STIM13  0xe0000034
#define ITM_STIM14  0xe0000038
#define ITM_STIM15  0xe000003c
#define ITM_STIM16  0xe0000040
#define ITM_STIM17  0xe0000044
#define ITM_STIM18  0xe0000048
#define ITM_STIM19  0xe000004c
#define ITM_STIM20  0xe0000050
#define ITM_STIM21  0xe0000054
#define ITM_STIM22  0xe0000058
#define ITM_STIM23  0xe000005c
#define ITM_STIM24  0xe0000060
#define ITM_STIM25  0xe0000064
#define ITM_STIM26  0xe0000068
#define ITM_STIM27  0xe000006c
#define ITM_STIM28  0xe0000070
#define ITM_STIM29  0xe0000074
#define ITM_STIM30  0xe0000078
#define ITM_STIM31  0xe000007c
#define ITM_STIM(n) (0xe0000000 + n*4)
// Data write to the stimulus port FIFO, for forwarding as a software ev pack
// Indicates whether the stimulus port FIFO can accept data.
#define FIFOREADY   0x1

/* Trace Enable Register */
#define ITM_TER0    0xe0000e00
#define ITM_TER1    0xe0000e04
#define ITM_TER2    0xe0000e08
#define ITM_TER3    0xe0000e0c
#define ITM_TER4    0xe0000e10
#define ITM_TER5    0xe0000e14
#define ITM_TER6    0xe0000e18
#define ITM_TER7    0xe0000e1c
#define ITM_TER(n)  (0xe0000e00 + n*4)
// an individual enable bit for each ITM_STIM register.
#define STIMENA0    0x00000001
#define STIMENA1    0x00000002
#define STIMENA2    0x00000004
#define STIMENA3    0x00000008
#define STIMENA4    0x00000010
#define STIMENA5    0x00000020
#define STIMENA6    0x00000040
#define STIMENA7    0x00000080
#define STIMENA8    0x00000100
#define STIMENA9    0x00000200
#define STIMENA10   0x00000400
#define STIMENA11   0x00000800
#define STIMENA12   0x00001000
#define STIMENA13   0x00002000
#define STIMENA14   0x00004000
#define STIMENA15   0x00008000
#define STIMENA16   0x00010000
#define STIMENA17   0x00020000
#define STIMENA18   0x00040000
#define STIMENA19   0x00080000
#define STIMENA20   0x00100000
#define STIMENA21   0x00200000
#define STIMENA22   0x00400000
#define STIMENA23   0x00800000
#define STIMENA24   0x01000000
#define STIMENA25   0x02000000
#define STIMENA26   0x04000000
#define STIMENA27   0x08000000
#define STIMENA28   0x10000000
#define STIMENA29   0x20000000
#define STIMENA30   0x40000000
#define STIMENA31   0x80000000

/* ITM Trace Privilege Register */
#define ITM_TPR     0xe0000e40
// Bit mask to enable unprivileged access to ITM stimulus ports.

/* Trace Control Register */
#define ITM_TCR     0xe0000e80
// Indicates whether the ITM is currently processing events
#define BUSY                0x00800000
// Identifier for multi-source trace stream formatting.
#define TRACE_BUS_ID_MSK    0x007f0000
#define TRACE_BUS_ID_SFT    16
#define TRACE_BUS_ID_GET(r) ((r>>TRACE_BUS_ID_SFT)&0x7f)
#define TRACE_BUS_ID_SET(n) ((n<<TRACE_BUS_ID_SFT)&TRACE_BUS_ID_MSK)
// Global timestamp frequency
#define GTSFREQ_DIS         0x00000000
#define GTSFREQ_128CYCLE    0x00000400
#define GTSFREQ_8192CYCLE   0x00000800
#define GTSFREQ_EMPTY       0x00000c00
// Local timestamp prescaler, used with the trace packet reference clock.
#define TSPRESCALE_NO       0x00000000
#define TSPRESCALE_DIV4     0x00000100
#define TSPRESCALE_DIV16    0x00000200
#define TSPRESCALE_DIV64    0x00000300
// Enables asynchronous clocking of the timestamp counter
#define SWOENA              0x00000010
// Enables forwarding of hardware event packet from the DWT unit to the ITM
#define TXENA               0x00000008
// Enables Synchronization packet transmission for a synchronous TPIU
#define SYNCENA             0x00000004
// Enables Local timestamp generation
#define TSENA               0x00000002
// Enables the ITM
#define ITMENA              0x00000001

/* Peripheral Identification registers */
#define PID0        0xe0000fe0
#define PID1        0xe0000fe4
#define PID2        0xe0000fe8
#define PID3        0xe0000fec
#define PID4        0xe0000fd0
#define PID5        0xe0000fd4
#define PID6        0xe0000fd8
#define PID7        0xe0000fdc

/* Component Identification registers */
#define CID0        0xe0000ff0
#define CID1        0xe0000ff4
#define CID2        0xe0000ff8
#define CID3        0xe0000ffc

#endif
