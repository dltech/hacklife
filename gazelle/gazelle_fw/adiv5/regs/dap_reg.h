#ifndef DAP_REG_H
#define DAP_REG_H
/*
 * Part of USB universal flaser.
 * Access port registers for SWD ARMv7M debug support
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* The AP Abort Register, ABORT */
#define ABORT_AP        0x0
#define ABORT_SEL       0x0
#define ABORT_APNDP     0x0
// Write 1 to this bit to clear the STICKYORUN overrun error flag to 0.
#define ORUNERRCLR  0x00000010
// Write 1 to this bit to clear the WDATAERR write data error flag to 0.
#define WDERRCLR    0x00000008
// Write 1 to this bit to clear the STICKYERR sticky error flag to 0.
#define STKERRCLR   0x00000004
// Write 1 to this bit to clear the STICKYCMP sticky compare flag to 0.
#define STKCMPCLR   0x00000002
// Write 1 to this bit to generate a DAP abort.
#define DAPABORT    0x00000001

/* The Identification Code Register, IDCODE */
#define IDCODE_AP       0x0
#define IDCODE_SEL      0x0
#define IDCODE_APNDP    0x0
// Version code.
#define VERSION_MSK     0xf0000000
#define VERSION_SFT     28
#define VERSION_GET(r)  ((r>>VERSION_SFT)&0xf)
// Part Number for the DP.
#define PARTNO_MSK      0x0ffff000
#define PARTNO_SFT      12
#define PARTNO_GET(r)   ((r>>PARTNO_SFT)&0xffff)
#define PARTNO_JTAG_DP  0xba00
#define PARTNO_SW_DP    0xba10
// JEDEC Designer ID, code that identifies the designer of the ADI impl
#define DESIGNER_MSK    0x00000ffe
#define DESIGNER_SFT    1
#define DESIGNER_GET(r) ((r>>DESIGNER_SFT)&0x7ff)
#define DEFIGNER_ARM    0x43b

/* The Control/Status Register, CTRL/STAT */
#define CTRL_STAT_AP    0x1
#define CTRL_STAT_SEL   0x0
#define CTRL_STAT_APNDP 0x0
// System power-up acknowledge.
#define CSYSPWRUPACK            0x80000000
// System power-up request.
#define CSYSPWRUPREQ            0x40000000
// Debug power-up acknowledge.
#define CDBGPWRUPACK            0x20000000
// Debug power-up request.
#define CDBGPWRUPREQ            0x10000000
// Debug reset acknowledge.
#define CDBGRSTACK              0x08000000
// Debug reset request.
#define CDBGRSTREQ              0x04000000
// Transaction counter.
#define TRNCNT_MSK              0x00fff000
#define TRNCNT_SFT              12
#define TRNCNT_GET(r)           ((r>>TRNCNT_SFT)&0xfff)
#define TRNCNT_SET(n)           ((n<<TRNCNT_SFT)&TRNCNT_MSK)
// bytes to be masked in pushed compare and pushed verify operations.
#define MASKLANE_MSK            0x00000f00
#define MASKLANE_SFT            8
#define MASKLANE_GET(r)         ((r>>MASKLANE_SFT)&0xf)
#define MASKLANE_SET(n)         ((n<<MASKLANE_SFT)&MASKLANE_MSK)
// This bit is set to 1 if a Write Data Error occurs.
#define WDATAERR                0x00000080
// 1 if the response to the previous AP or RDBUFF read was OK.
#define READOK                  0x00000040
// This bit is set to 1 if an error is returned by an AP transaction.
#define STICKYERR               0x00000020
// 1 when a match occurs on a pushed compare or verify operation.
#define STICKYCMP               0x00000010
// This field sets the transfer mode for AP operations
#define TRNMODE_NORMAL          0x00000000
#define TRNMODE_PUSH_VERIFY     0x00000004
#define TRNMODE_PUSH_COMPARE    0x00000008
// If overrun detection is en, this bit is set to 1 when an overrun
#define STICKYORUN              0x00000002
// This bit is set to 1 to enable overrun detection.
#define ORUNDETECT              0x00000001

/* The Wire Control Register, WCR */
#define WCR_AP          0x1
#define WCR_SEL         0x1
#define WCR_APNDP       0x0
// Turnaround tri-state period
#define TURNROUND1          0x000
#define TURNROUND2          0x100
#define TURNROUND3          0x200
#define TURNROUND4          0x300
// Selects or indicates the operating mode for the wire conn to the DP
#define WIREMODE_SYNC       0x040
#define WIREMODE_ASYNC      0x000
// prescaler for generating the clock used to oversample the SWD
#define PRESCALER_MSK       0x007
#define PRESCALER_SET(n)    (n&PRESCALER_MSK)
#define PRESCALER_GET(r)    (r&PRESCALER_MSK)

/* The AP Select Register, SELECT */
#define SELECT_AP       0x2
#define SELECT_SEL      0x0
#define SELECT_APNDP    0x0
// Selects the current AP.
#define APSEL_MSK           0xff000000
#define APSEL_SFT           24
#define APSEL_SET(n)        ((n<<APSEL_SFT)&APSEL_MSK)
// Selects the active four-word register bank on the current AP.
#define APBANKSEL_MSK       0x000000f0
#define APBANKSEL_SFT       4
#define APBANKSEL_SET(n)    ((n<<APBANKSEL_SFT)&APBANKSEL_MSK)
// SW-DP Debug Port address bank select
#define CTRLSEL             0x00000001

/* The Read Resend Register, RESEND */
#define RESEND_AP       0x2
#define RESEND_SEL      0x0
#define RESEND_APNDP    0x0
// Reading the RESEND register enables the read data to be recovered

/* The Read Buffer, RDBUFF */
#define RDBUFF_AP       0x3
#define RDBUFF_SEL      0x0
#define RDBUFF_APNDP    0x0
// data from the AP, presented as the result of a previous read

#endif
