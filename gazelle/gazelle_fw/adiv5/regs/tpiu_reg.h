#ifndef TPIU_REG_H
#define TPIU_REG_H
/*
 * Part of USB universal flaser.
 * Trace Port Interface Unit registers for SWD ARMv7M debug support
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Supported Parallel Port Sizes Register */
#define TPIU_SSPSR  0xe0040000
// 1 - Width (N+1) supported.

/* Current Parallel Port Size Register */
#define TPIU_CSPSR  0xe0040004
// 1 - Width (N+1) is the current trace port width.

/* Asynchronous Clock Prescaler Register */
#define TPIU_ACPR   0xe0040010
// SWO output clock = Asynchronous_Reference_Clock/(SWOSCALAR +1)
#define SWOSCALER_MSK   0xffff

/* Selected Pin Protocol Register */
#define TPIU_SPPR   0xe00400f0
// Specified the protocol for trace output from the TPIU.
#define TXMODE_PARALLEL         0x0
#define TXMODE_SWO_MANCHESTER   0x1
#define TXMODE_SWO_NRZ          0x2

/* TPIU Type register */
#define TPIU_TYPE   0xe0040fc8
#define TPIU_DEVID  0xe0040fc8
// Indicates support for SWO using UART/NRZ encoding
#define NRZVALID            0x800
// Indicates support for SWO using Manchester encoding
#define MANCVALID           0x400
// Indicates support for parallel trace port operation
#define PTINVALID           0x200
// minimum implemented size of the TPIU output FIFO for trace data
#define FIFOSZ_MSK          0x1c0
#define FIFOSZ_SFT          6
#define FIFOSZ_GET(r)       ((r>>FIFOSZ_SFT)&0x7)
// Specifies whether TRACECLKIN can be asynchronous to CLK
#define ASYNC_TRACECLKIN    0x020
// Specifies the number of trace inputs
#define TRACE_INPUT1        0x000
#define TRACE_INPUT2        0x001

#endif
