#ifndef FPB_REG_H
#define FPB_REG_H
/*
 * Part of USB universal flaser.
 * Flash Patch and Breakpoint unit registers for SWD ARMv7M debug support
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Flash Patch Control Register */
#define FP_CTRL     0xe0002000
// Flash Patch breakpoint architecture revision
#define REV_MSK         0xf0000000
#define REV_SFT         28
#define REV_GET(r)      ((r>>REV_SFT)&0xf)
#define REV_VERSION1    0x0
#define REV_VERSION2    0x1
// the number of instruction address comparators
#define NUM_CODE_MSB_MSK    0x00007000
#define NUM_CODE_MSB_SFT    12
#define NUM_CODE_LSB_MSK    0x00000f00
#define NUM_CODE_LSB_SFT    8
#define NUM_CODE_GET(r)     ((((r>>NUM_CODE_MSB_SFT)&0x7)<<4)+((r>>NUM_CODE_LSB_SFT)&0xf))
#define NUM_CODE_SET(n)     (((n<<(NUM_CODE_MSB_SFT-4))&NUM_CODE_MSB_MSK)+((n<<NUM_CODE_LSB_SFT)&NUM_CODE_LSB_MSK))
// The number of literal address comparators supported
#define NUM_LIT_MSK         0x00000f00
#define NUM_LIT_SFT         8
#define NUM_LIT_GET(r)      ((r>>NUM_LIT_SFT)&0xf)
#define NUM_LIT_SET(n)      ((n<<NUM_LIT_SFT)&NUM_LIT_MSK)
// On any write to FP_CTRL, this bit must be 1.
#define KEY             0x00000002
// Enable bit for the FPB
#define ENABLE          0x00000001

/* Flash Patch Remap register */
#define FP_REMAP    0xe0002004
// Indicates whether the FPB unit supports Flash Patch remap
#define RMPSPT      0x20000000
// bits[28:5] of the base address in SRAM to which the FPB remaps the address.
#define REMAP_MSK       0x1fffffe0
#define REMAP_SFT       5
#define REMAP_GET(r)    ((r>>REMAP_SFT)&0xffffff))
#define REMAP_SET(n)    ((n<<REMAP_SFT)&REMAP_MSK)

/* Flash Patch Comparator register */
#define FP_COMP(n)  (0xe0002008 + 4*n)
// for FPB Version 1.
// Defines the behavior when the COMP address is matched
#define REPLACE_REMAP               0x00000000
#define REPLACE_BPT_AT_000_COMP_00  0x40000000
#define REPLACE_BPT_AT_000_COMP_00  0x80000000
#define REPLACE_BPT_AT_BOTH         0xc0000000
// Bits[28:2] of the address to compare with addresses from the Code memory
#define COMP_MSK                    0x1ffffffc
#define COMP_GET(r)                 (r&COMP_MSK)
#define COMP_SET(n)                 (n&COMP_MSK)
// Enable bit for this comparator
#define ENABLE_COMP                 0x00000001
// for FPB Version 2 where the Flash Patch is not implemented.
// Breakpoint address.
#define BPADDR_MSK                  0xfffffffe
#define BPADDR_GET(r)               (r&BPADDR_MSK)
#define BPADDR_SET(n)               (n&BPADDR_MSK)
// Enable bit for breakpoint
#define BE                          0x00000001
// for FPB Version 2 where the Flash Patch is implemented.
// Specifies if Flash Patch enabled
#define FE                          0x10000000
// bits[28:2] of the Flash Patch address
#define FPADDR_MSK                  0x1ffffffe
#define FPADDR_GET(r)               (r&FPADDR_MSK)
#define FPADDR_SET(n)               (n&FPADDR_MSK)

#endif
