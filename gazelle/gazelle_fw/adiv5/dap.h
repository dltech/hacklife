 /*
 * Part of USB universal flaser.
 * Functions for interacting with debug access port (DAP).
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <inttypes.h>
#include "regs/dap_reg.h"
#include "regs/memap_reg.h"
#include "swd.h"

uint32_t dapBegin(void);
uint32_t dapReset(void);
uint8_t writeDap(uint8_t reg, uint32_t data);
uint32_t readDap(uint8_t reg, uint8_t *ack);

#define DEFAULT_CSW         (MASTER_TYPE | HPROT1 | CSW_RESERVED1 | ADDR_INC_OFF | SIZE_32BIT)
#define CSW_16BIT           (MASTER_TYPE | HPROT1 | CSW_RESERVED1 | ADDR_INC_OFF | SIZE_16BIT)
#define CSW_8BIT            (MASTER_TYPE | HPROT1 | CSW_RESERVED1 | ADDR_INC_OFF | SIZE_8BIT)
#define DEFAULT_CTRL_STAT   (CSYSPWRUPREQ | CDBGPWRUPREQ)
#define DEFAULT_WCR         (WIREMODE_SYNC | PRESCALER_SET(0))

#define MEMAP_RETRY_COUNT   100

enum {
    ABORT,
    IDCODE,
    CTRL_STAT,
    WCR,
    SELECT,
    RESEND,
    RDBUFF,
    CSW,
    TAR,
    DRW,
    BD0,
    BD1,
    BD2,
    BD3,
    CFG,
    BASE,
    IDR
};
