#ifndef MEMORY_MAPPED_H
#define MEMORY_MAPPED_H
/*
 * Part of USB universal flaser.
 * ARMv7 memory mapped registers access through the SWD interface.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <inttypes.h>

uint32_t memReadSingle(uint32_t addr);
uint8_t memWriteSingle(uint32_t addr, uint32_t reg);
uint8_t memReadSeries(uint32_t addr, uint32_t *regs, int size);
uint8_t memWriteSeries(uint32_t addr, uint32_t *regs, int size);
uint32_t memVerify(uint32_t addr, uint32_t *regs, int size);

#endif
