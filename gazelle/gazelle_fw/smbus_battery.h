/*
 * Part of USB universal flaser.
 * Here is bq20z45 protocol command definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** SBS COMMANDS **/

#define ManufacturerAccess          0x00
#define ManufacturerAccessSize      2

#define RemainingCapacityAlarm      0x01
#define RemainingCapacityAlarmSize  2

#define RemainingTimeAlarm          0x02
#define RemainingTimeAlarmSize      2

#define BatteryMode                 0x03
#define BatteryModeSize             2

#define AtRate                      0x04
#define AtRateSize                  2

#define AtRateTimeToFull            0x05
#define AtRateTimeToFullSize        2

#define AtRateTimeToEmpty           0x06
#define AtRateTimeToEmptySize       2

#define AtRateOK                    0x07
#define AtRateOKSize                2

#define Temperature                 0x08
#define TemperatureSize             2

#define Voltage                     0x09
#define VoltageSize                 2

#define Current                     0x0a
#define CurrentSize                 2

#define AverageCurrent              0x0b
#define AverageCurrentSize          2

#define MaxError                    0x0c
#define MaxErrorSize                1

#define RelativeStateOfCharge       0x0d
#define RelativeStateOfChargeSize   1

#define AbsoluteStateOfCharge       0x0e
#define AbsoluteStateOfChargeSize   1

#define RemainingCapacity           0x0f
#define RemainingCapacitySize       2

#define FullChargeCapacity          0x10
#define FullChargeCapacitySize      2

#define RunTimeToEmpty              0x11
#define RunTimeToEmptySize          2

#define AverageTimeToEmpty          0x12
#define AverageTimeToEmptySize      2

#define AverageTimeToFull           0x13
#define AverageTimeToFullSize       2

#define ChargingCurrent             0x14
#define ChargingCurrentSize         2

#define BatteryStatus               0x16
#define BatteryStatusSize           2

#define CycleCount                  0x17
#define CycleCountSize              2

#define DesignCapacity              0x18
#define DesignCapacitySize          2

#define DesignVoltage               0x19
#define DesignVoltageSize           2

#define SpecificationInfo           0x1a
#define SpecificationInfoSize       2

#define ManufactureDate             0x1b
#define ManufactureDateSize         2

#define SerialNumber                0x1c
#define SerialNumberSize            2

#define ManufacturerName            0x20
#define ManufacturerNameSize        21

#define DeviceName                  0x21
#define DeviceNameSize              21

#define DeviceChemistry             0x22
#define DeviceChemistrySize         5

#define ManufacturerData            0x23
#define ManufacturerDataSize        15

#define Authenticate                0x2f
#define AuthenticateSize            21

#define CellVoltage4                0x3c
#define CellVoltage3                0x3d
#define CellVoltage2                0x3e
#define CellVoltage1                0x3f
#define CellVoltageSize             2

#define AFEData                     0x45
#define AFEDataSize                 2

#define FETControl                  0x46
#define FETControlSize              2

#define StateOfHealth               0x4f
#define StateOfHealthSize           2

#define SafetyStatus                0x51
#define SafetyStatus2               0x69
#define SafetyStatusSize            2

#define PFStatus                    0x53
#define PFStatus2                   0x6b
#define PFStatusSize                2

#define OperationStatus             0x54
#define OperationStatusSize         2

#define ChargingStatus              0x55
#define ChargingStatusSize          2

#define ResetData                   0x57
#define ResetDataSize               2

#define WDResetData                 0x58
#define WDResetDataSize             2

#define PackVoltage                 0x5a
#define PackVoltageSize             2

#define AverageVoltage              0x5d
#define AverageVoltageSize          2

#define TS1Temperature              0x5e
#define TS2Temperature              0x5f
#define TSTemperatureSize           2

#define UnSealKey                   0x60
#define UnSealKeySize               4

#define FullAccessKey               0x61
#define FullAccessKeySize           4

#define PFKey                       0x62
#define PFKeySize                   4

#define AuthenKey3                  0x63
#define AuthenKey2                  0x64
#define AuthenKey1                  0x65
#define AuthenKeySize               4

#define ManufBlock1                 0x6c
#define ManufBlock2                 0x6d
#define ManufBlock3                 0x6e
#define ManufBlock4                 0x6f
#define ManufBlockSize              20

#define ManufacturerInfo            0x70
#define ManufacturerInfoSize        32

#define SenseResistor               0x71
#define SenseResistorSize           2

#define TempRange                   0x72
#define TempRangeSize               2

#define LifetimeData                0x73
#define LifetimeDataSize            33

#define DataFlashSubClassID         0x77
#define DataFlashSubClassIDSize     2

#define DataFlashSubClassPage1      0x78
#define DataFlashSubClassPage2      0x79
#define DataFlashSubClassPage3      0x7a
#define DataFlashSubClassPage4      0x7b
#define DataFlashSubClassPage5      0x7c
#define DataFlashSubClassPage6      0x7d
#define DataFlashSubClassPage7      0x7e
#define DataFlashSubClassPage8      0x7f
#define DataFlashSubClassPageSize   32

