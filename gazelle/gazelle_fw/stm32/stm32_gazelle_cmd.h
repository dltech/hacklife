#ifndef H_STM32_GAZELLE_CMD
#define H_STM32_GAZELLE_CMD
/*
 * Part of USB universal flaser.
 * Command handlers for GUI flasfer utility for STM32 support.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOW_DENSITY_S       21
#define MEDIUM_DENSITY_S    24
#define HIGH_DENSITY_S      22
#define XL_DENSITY_S        20
#define CONNECTIVITY_S      27

int detectStm32(uint8_t *ack);
int eraseStm32(uint8_t *ack);
int writeStm32(uint8_t *ack, uint32_t *data);
int readStm32(uint8_t *ack, uint32_t *data);

#endif
