#ifndef H_STM32_FLASH
#define H_STM32_FLASH
/*
 * Part of USB universal flaser.
 * Functions for flashing stm32 memory.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define FLASH_TIMEOUT   1000

#include <stdint.h>

int erasePage(uint32_t addr);
int massErase(void);
int eraseOptions(void);
int writeFlash(uint32_t addr, uint32_t *data, int size);
int writeOption(uint32_t addr, uint8_t option);

#endif
