#ifndef H_RCC_DBG_REGS
#define H_RCC_DBG_REGS
/*
 * Part of Belkin STM32 HAL, rcc registers of STMF103 MCU,
 * USB debugger version.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "memorymap.h"

/* Clock control register */
#define RCC_CR          0x40021000
// PLL clock ready flag
#define PLLRDY  0x02000000
// PLL enable
#define PLLON   0x01000000
// Clock security system enable
#define CSSON   0x00080000
// External high-speed clock bypass
#define HSEBYP  0x00040000
// External high-speed clock ready flag
#define HSERDY  0x00020000
// HSE clock enable
#define HSEON   0x00010000
// Internal high-speed clock calibration
#define HSICAL_OFFS     8
// Internal high-speed clock trimming
#define HSITRIM_OFFS    3
// Internal high-speed clock ready flag
#define HSIRDY  0x00000002
// Internal high-speed clock enable
#define HSION   0x00000001

/* Clock configuration register */
#define RCC_CFGR        0x40021004
// Microcontroller clock output
#define MCO_NOCLK           0x00000000
#define MCO_SYSCLK          0x04000000
#define MCO_HSI             0x05000000
#define MCO_HSE             0x06000000
#define MCO_PLL_DIV2        0x07000000
// USB prescaler
#define USBPRE              0x00400000
// PLL multiplication factor
#define PLLMUL16            0x003c0000
//#define PLLMUL16            0x00380000
#define PLLMUL15            0x00340000
#define PLLMUL14            0x00300000
#define PLLMUL13            0x002c0000
#define PLLMUL12            0x00280000
#define PLLMUL11            0x00240000
#define PLLMUL10            0x00200000
#define PLLMUL9             0x001c0000
#define PLLMUL8             0x00180000
#define PLLMUL7             0x00140000
#define PLLMUL6             0x00100000
#define PLLMUL5             0x000c0000
#define PLLMUL4             0x00080000
#define PLLMUL3             0x00040000
#define PLLMUL2             0x00000000
// HSE divider for PLL entry
#define PLLXTPRE            0x00020000
// PLL entry clock source
#define PLLSRC              0x00010000
// ADC prescaler
#define ADCPRE_PCLK2_DIV8   0x0000c000
#define ADCPRE_PCLK2_DIV6   0x00008000
#define ADCPRE_PCLK2_DIV4   0x00004000
#define ADCPRE_PCLK2_DIV2   0x00000000
// APB high-speed prescaler (APB2)
#define PPRE2_HCLK_DIV16    0x00003800
#define PPRE2_HCLK_DIV8     0x00003000
#define PPRE2_HCLK_DIV4     0x00002800
#define PPRE2_HCLK_DIV2     0x00002000
#define PPRE2_HCLK_NODIV    0x00000000
// APB low-speed prescaler (APB1)
#define PPRE1_HCLK_DIV16    0x00000700
#define PPRE1_HCLK_DIV8     0x00000600
#define PPRE1_HCLK_DIV4     0x00000500
#define PPRE1_HCLK_DIV2     0x00000400
#define PPRE1_HCLK_NODIV    0x00000000
// AHB prescaler
#define HPRE_SYSCLK_DIV512  0x000000f0
#define HPRE_SYSCLK_DIV256  0x000000e0
#define HPRE_SYSCLK_DIV128  0x000000d0
#define HPRE_SYSCLK_DIV64   0x000000c0
#define HPRE_SYSCLK_DIV16   0x000000b0
#define HPRE_SYSCLK_DIV8    0x000000a0
#define HPRE_SYSCLK_DIV4    0x00000090
#define HPRE_SYSCLK_DIV2    0x00000080
#define HPRE_SYSCLK_NODIV   0x00000000
// System clock switch status
#define SWS_MASK            0x0000000c
#define SWS_HSI             0x00000000
#define SWS_HSE             0x00000004
#define SWS_PLL             0x00000008
// System clock switch
#define SW_MASK             0x00000003
#define SW_HSI              0x00000000
#define SW_HSE              0x00000001
#define SW_PLL              0x00000002

/* Clock interrupt register */
#define RCC_CIR         0x40021008
// Clock security system interrupt clear
#define CSSC        0x00800000
// PLL ready interrupt clear
#define PLLRDYC     0x00100000
// HSE ready interrupt clear
#define HSERDYC     0x00080000
// HSI ready interrupt clear
#define HSIRDYC     0x00040000
// LSE ready interrupt clear
#define LSERDYC     0x00020000
// LSI ready interrupt clear
#define LSIRDYC     0x00010000
// PLL ready interrupt enable
#define PLLRDYIE    0x00001000
// HSE ready interrupt enable
#define HSERDYIE    0x00000800
// HSI ready interrupt enable
#define HSIRDYIE    0x00000400
// LSE ready interrupt enable
#define LSERDYIE    0x00000200
// LSI ready interrupt enable
#define LSIRDYIE    0x00000100
// Clock security system interrupt flag
#define CSSF        0x00000080
// PLL ready interrupt flag
#define PLLRDYF     0x00000010
// HSE ready interrupt flag
#define HSERDYF     0x00000008
// HSI ready interrupt flag
#define HSIRDYF     0x00000004
// LSE ready interrupt flag
#define LSERDYF     0x00000002
// LSI ready interrupt flag
#define LSIRDYF     0x00000001

#endif
