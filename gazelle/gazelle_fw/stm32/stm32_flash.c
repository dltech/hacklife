/*
 * Part of USB universal flaser.
 * Functions for flashing stm32 memory.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../adiv5/memory_mapped.h"
#include "../adiv5/dap.h"
#include "flash_dbg_regs.h"
#include "stm32_flash.h"

static void waitForFlashBusy(void);
static void checkUnlockFlash(void);
static void checkUnlockOptions(void);


uint32_t dcr = 0;
int erasePage(uint32_t addr)
{
    waitForFlashBusy();
    checkUnlockFlash();
    memWriteSingle(FLASH_CR, PER);
    memWriteSingle(FLASH_AR, addr);
    memWriteSingle(FLASH_CR, PER | STRT);
    waitForFlashBusy();
    dcr = memReadSingle(FLASH_SR);
    if( memReadSingle(FLASH_SR) != EOP ) {
        return -1;
    }
    return 0;
}

int massErase()
{
    waitForFlashBusy();
    checkUnlockFlash();
    memWriteSingle(FLASH_CR, MER);
    memWriteSingle(FLASH_CR, MER | STRT);
    waitForFlashBusy();
    if( memReadSingle(FLASH_SR) != EOP ) {
        return -1;
    }
    return 0;
}

int writeFlash(uint32_t addr, uint32_t *data, int size)
{
    waitForFlashBusy();
    checkUnlockFlash();
    for(int i=0 ; i<size*2 ; ++i)
    {
        memWriteSingle(FLASH_CR, PG);
        writeDap(CSW, CSW_16BIT);
        memWriteSingle(addr+(uint32_t)i*2, data[i/2]);
        writeDap(CSW, DEFAULT_CSW);
        waitForFlashBusy();
        if(((i&0x1)==1) && \
        ((memReadSingle(addr+(uint32_t)((i/2)*4))&0xffff0000) != (data[i/2]&0xffff0000))) {
            return -1;
        }
        if(((i&0x1)==0) && \
        ((memReadSingle(addr+(uint32_t)((i/2)*4))&0x0000ffff) != (data[i/2]&0x0000ffff))) {
            return -1;
        }
    }
    return 0;
}

int eraseOptions()
{
    waitForFlashBusy();
    checkUnlockOptions();
    memWriteSingle(FLASH_CR, OPTER);
    memWriteSingle(FLASH_CR, OPTER | STRT);
    waitForFlashBusy();
    if( memReadSingle(FLASH_SR) != EOP ) {
        return -1;
    }
    return 0;
}

int writeOption(uint32_t addr, uint8_t option)
{
    waitForFlashBusy();
    checkUnlockOptions();
    memWriteSingle(FLASH_CR, OPTPG);
    writeDap(CSW, CSW_16BIT);
    memWriteSingle(addr, option);
    writeDap(CSW, DEFAULT_CSW);
    waitForFlashBusy();
    if((uint8_t)memReadSingle(addr) != option) {
        return -1;
    }
    return 0;
}

static void checkUnlockFlash()
{
    if(memReadSingle(FLASH_CR)&LOCK) {
        memWriteSingle(FLASH_KEYR, KEY1);
        memWriteSingle(FLASH_KEYR, KEY2);
    }
}

static void checkUnlockOptions()
{
    if(memReadSingle(FLASH_CR)&OPTWRE) {
        memWriteSingle(FLASH_OPTKEYR, KEY1);
        memWriteSingle(FLASH_OPTKEYR, KEY2);
    }
}

static void waitForFlashBusy()
{
    int tOut = FLASH_TIMEOUT;
    while( (memReadSingle(FLASH_SR) & BSY) && (--tOut>0) );
}
