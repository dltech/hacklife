/*
 * Part of USB universal flaser.
 * Command handlers for GUI flasfer utility for STM32 support.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include <stdlib.h>
#include "stm32_flash.h"
#include "../adiv5/debug.h"
#include "stm32_dbg_clock.h"
#include "../adiv5/dap.h"
#include "../adiv5/memory_mapped.h"
#include "debug_dbg_regs.h"
#include "device_dbg_regs.h"
#include "stm32_gazelle_cmd.h"

uint32_t addr = 0x08000000;

const uint8_t stm32LowDensityStr[LOW_DENSITY_S]    = {'S','T','M','3','2','F','1','x','x',' ','l','o','w',' ','d','e','n','s','i','t','y'};
const uint8_t stm32MedDensityStr[MEDIUM_DENSITY_S] = {'S','T','M','3','2','F','1','x','x',' ','m','e','d','i','u','m',' ','d','e','n','s','i','t','y'};
const uint8_t stm32HighDensityStr[HIGH_DENSITY_S]  = {'S','T','M','3','2','F','1','x','x',' ','h','i','g','h',' ','d','e','n','s','i','t','y'};
const uint8_t stm32XlDensityStr[XL_DENSITY_S]      = {'S','T','M','3','2','F','1','x','x',' ','X','L',' ','d','e','n','s','i','t','y'};
const uint8_t stm32ConnectivityStr[CONNECTIVITY_S] = {'S','T','M','3','2','F','1','x','x',' ','c','o','n','n','e','c','t','i','v','i','t','y',' ','l','i','n','e'};

int detectStm32(uint8_t *ack)
{
    uint32_t idcode = dapBegin();
    uint8_t size = 0;
    if((idcode != ACK_FAULT) && (idcode != ACK_ERR)) {
        size += 9;
        memcpy(ack, "detected ", size);
    } else {
        size += 16;
        memcpy(ack, "connection error", size);
        return size;
    }
    resetAndHalt();
    internalClk48M();
    switch(memReadSingle(DBGMCU_IDCODE) & DEV_ID_MSK)
    {
        case LOW_DENSITY:
            memcpy(ack + size, stm32LowDensityStr, LOW_DENSITY_S);
            size += LOW_DENSITY_S;
            break;
        case MEDIUM_DENSITY:
            memcpy(ack + size, stm32MedDensityStr, MEDIUM_DENSITY_S);
            size += MEDIUM_DENSITY_S;
            break;
        case HIGH_DENSITY:
            memcpy(ack + size, stm32HighDensityStr, HIGH_DENSITY_S);
            size += HIGH_DENSITY_S;
            break;
        case XL_DENSITY:
            memcpy(ack + size, stm32XlDensityStr, XL_DENSITY_S);
            size += XL_DENSITY_S;
            break;
        case CONNECTIVITY:
            memcpy(ack + size, stm32ConnectivityStr, CONNECTIVITY_S);
            size += CONNECTIVITY_S;
            break;
    }
    memcpy(ack+size, ", ", 2);
    size += 2;
    utoa(memReadSingle(FLASH_SIZE)&FLASH_MSK, (char*)(ack+size), 10);
    size = (uint8_t)strlen((char*)ack);
    memcpy(ack+size, "kBytes flash", 12);
    size += 12;
    return size;
}

int eraseStm32(uint8_t *ack)
{
    addr = 0x08000000;
    if( massErase() >= 0 ) {
        memcpy(ack, "erase ok", 9);
        return 9;
    }
    memcpy(ack, "erase err", 10);
    return 10;
}

int writeStm32(uint8_t *ack, uint32_t *data)
{
    if( writeFlash(addr, data, 16) >= 0 ) {
        memcpy(ack, "write ok", 9);
        addr += 64;
        return 9;
    }
    memcpy(ack, "write err", 10);
    addr = 0x08000000;
    return 10;
}

int readStm32(uint8_t *ack, uint32_t *data)
{
    if( memReadSeries(addr, data, 16) != ACK_OK ) {
        memcpy(ack, "read ok", 8);
        addr += 64;
        return 8;
    }
    memcpy(ack, "read err", 9);
    addr = 0x08000000;
    return 9;
}
