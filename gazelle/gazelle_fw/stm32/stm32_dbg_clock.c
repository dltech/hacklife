/*
 * Part of USB universal flaser.
 * Functions for configuring clock through debugger.
 *
 * Copyright 2024 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "rcc_dbg_regs.h"
#include "flash_dbg_regs.h"
#include "../adiv5/memory_mapped.h"
#include "stm32_dbg_clock.h"

void internalClk48M()
{
    int tOut = 1e3;
    // switch on internal oscillator
    memWriteSingle(RCC_CR, HSION);
    while(((memReadSingle(RCC_CR) & HSIRDY) == 0) && (--tOut>0));
    // configuring PLL for 48 MHz from internal RC with max clk on buses
    const uint32_t cfgr = USBPRE | PLLMUL12 | PPRE2_HCLK_NODIV | \
                    PPRE1_HCLK_NODIV | HPRE_SYSCLK_NODIV;
    memWriteSingle(RCC_CFGR, cfgr);
    // flash configure for high frequency.
    memWriteSingle(FLASH_ACR, PRFTBE | LATENCY_72M);
    // switching on the PLL
    tOut = 1e3;
    while( ((memReadSingle(RCC_CR) & PLLRDY) != 0) && (--tOut > 0) );
    memWriteSingle(RCC_CR, PLLON | HSION);
    tOut = 1e3;
    while( ((memReadSingle(RCC_CR) & PLLRDY) == 0) && (--tOut > 0) );
    // включаем sysclk, ждем
    memWriteSingle(RCC_CFGR, cfgr | SW_PLL);
    tOut = 1e3;
    while( ((memReadSingle(RCC_CFGR) & SWS_MASK) != SWS_PLL) && (--tOut > 0) );
}
