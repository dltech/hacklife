################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_core.c \
../Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_init.c \
../Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_int.c \
../Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_mem.c \
../Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_regs.c \
../Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_sil.c 

OBJS += \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_core.o \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_init.o \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_int.o \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_mem.o \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_regs.o \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_sil.o 

C_DEPS += \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_core.d \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_init.d \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_int.d \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_mem.d \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_regs.d \
./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_sil.d 


# Each subdirectory must supply rules for building sources it contributes
Src/USB_FS/STM32_USB_FS_Device_Driver/src/%.o Src/USB_FS/STM32_USB_FS_Device_Driver/src/%.su: ../Src/USB_FS/STM32_USB_FS_Device_Driver/src/%.c Src/USB_FS/STM32_USB_FS_Device_Driver/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -c -I../Inc -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/dltech/src_drugoy/new/modem/Inc" -I"/home/dltech/src_drugoy/new/modem/Inc/RFM69" -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/CMSIS/CM3/CoreSupport" -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/STM32F10x_StdPeriph_Driver/inc" -I"/home/dltech/src_drugoy/new/modem/Inc/USB_FS/STM32_USB_FS_Device_Driver/inc" -I"/home/dltech/src_drugoy/new/modem/Inc/USB_FS" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Src-2f-USB_FS-2f-STM32_USB_FS_Device_Driver-2f-src

clean-Src-2f-USB_FS-2f-STM32_USB_FS_Device_Driver-2f-src:
	-$(RM) ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_core.d ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_core.o ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_core.su ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_init.d ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_init.o ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_init.su ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_int.d ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_int.o ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_int.su ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_mem.d ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_mem.o ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_mem.su ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_regs.d ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_regs.o ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_regs.su ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_sil.d ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_sil.o ./Src/USB_FS/STM32_USB_FS_Device_Driver/src/usb_sil.su

.PHONY: clean-Src-2f-USB_FS-2f-STM32_USB_FS_Device_Driver-2f-src

