################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/USB_FS/hw_config.c \
../Src/USB_FS/stm32_it.c \
../Src/USB_FS/usb_desc.c \
../Src/USB_FS/usb_endp.c \
../Src/USB_FS/usb_istr.c \
../Src/USB_FS/usb_prop.c \
../Src/USB_FS/usb_pwr.c 

OBJS += \
./Src/USB_FS/hw_config.o \
./Src/USB_FS/stm32_it.o \
./Src/USB_FS/usb_desc.o \
./Src/USB_FS/usb_endp.o \
./Src/USB_FS/usb_istr.o \
./Src/USB_FS/usb_prop.o \
./Src/USB_FS/usb_pwr.o 

C_DEPS += \
./Src/USB_FS/hw_config.d \
./Src/USB_FS/stm32_it.d \
./Src/USB_FS/usb_desc.d \
./Src/USB_FS/usb_endp.d \
./Src/USB_FS/usb_istr.d \
./Src/USB_FS/usb_prop.d \
./Src/USB_FS/usb_pwr.d 


# Each subdirectory must supply rules for building sources it contributes
Src/USB_FS/%.o Src/USB_FS/%.su: ../Src/USB_FS/%.c Src/USB_FS/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -c -I../Inc -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/dltech/src_drugoy/new/modem/Inc" -I"/home/dltech/src_drugoy/new/modem/Inc/RFM69" -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/CMSIS/CM3/CoreSupport" -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/STM32F10x_StdPeriph_Driver/inc" -I"/home/dltech/src_drugoy/new/modem/Inc/USB_FS/STM32_USB_FS_Device_Driver/inc" -I"/home/dltech/src_drugoy/new/modem/Inc/USB_FS" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Src-2f-USB_FS

clean-Src-2f-USB_FS:
	-$(RM) ./Src/USB_FS/hw_config.d ./Src/USB_FS/hw_config.o ./Src/USB_FS/hw_config.su ./Src/USB_FS/stm32_it.d ./Src/USB_FS/stm32_it.o ./Src/USB_FS/stm32_it.su ./Src/USB_FS/usb_desc.d ./Src/USB_FS/usb_desc.o ./Src/USB_FS/usb_desc.su ./Src/USB_FS/usb_endp.d ./Src/USB_FS/usb_endp.o ./Src/USB_FS/usb_endp.su ./Src/USB_FS/usb_istr.d ./Src/USB_FS/usb_istr.o ./Src/USB_FS/usb_istr.su ./Src/USB_FS/usb_prop.d ./Src/USB_FS/usb_prop.o ./Src/USB_FS/usb_prop.su ./Src/USB_FS/usb_pwr.d ./Src/USB_FS/usb_pwr.o ./Src/USB_FS/usb_pwr.su

.PHONY: clean-Src-2f-USB_FS

