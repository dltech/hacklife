################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/RFM69/rfm69.c 

OBJS += \
./Src/RFM69/rfm69.o 

C_DEPS += \
./Src/RFM69/rfm69.d 


# Each subdirectory must supply rules for building sources it contributes
Src/RFM69/%.o Src/RFM69/%.su: ../Src/RFM69/%.c Src/RFM69/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DDEBUG -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -c -I../Inc -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x" -I"/home/dltech/src_drugoy/new/modem/Inc" -I"/home/dltech/src_drugoy/new/modem/Inc/RFM69" -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/CMSIS/CM3/CoreSupport" -I"/home/dltech/src_drugoy/new/modem/Inc/STM32F10x_StdPeriph_Lib_V3.5.0/Libraries/STM32F10x_StdPeriph_Driver/inc" -I"/home/dltech/src_drugoy/new/modem/Inc/USB_FS/STM32_USB_FS_Device_Driver/inc" -I"/home/dltech/src_drugoy/new/modem/Inc/USB_FS" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Src-2f-RFM69

clean-Src-2f-RFM69:
	-$(RM) ./Src/RFM69/rfm69.d ./Src/RFM69/rfm69.o ./Src/RFM69/rfm69.su

.PHONY: clean-Src-2f-RFM69

